﻿using System;
using System.Configuration;
using cms_client_tray.Properties;

namespace cms_client_tray
{
    partial class CMSSettings
    {
        private static CMSSettingsImpl s_instance = new CMSSettingsImpl();

        private class CMSSettingsImpl
        {

            private Configuration m_config;
            private AppSettingsSection m_settings;
            private KeyValueConfigurationCollection m_collection;

            public CMSSettingsImpl()
            {
                ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
                configFile.ExeConfigFilename = Resources.File_CMS_ClientConfig;
                m_config = ConfigurationManager.OpenMappedExeConfiguration(configFile,
                    ConfigurationUserLevel.None);
                m_settings = m_config.AppSettings;
                m_collection = m_settings.Settings;
                ConnectionManager.load(m_config);
            }

            public ConnectionStringsSection ConnectionStrings {
                get { return m_config.ConnectionStrings; }
            }

            public bool isAutoStart
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.AutoStart)); }
                set { writeKeyValue(Resources.AutoStart, value); }
            }

            public bool isLockdown
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.Lockdown)); }
                set { writeKeyValue(Resources.Lockdown, value); }
            }

            public string defaultConnection
            {
                get { return readKeyValue(Resources.DefaultConnection); }
                set { writeKeyValue(Resources.DefaultConnection, value); }
            }

            public int serviceTimeout
            {
                get { return Convert.ToInt32(readKeyValue(Resources.ServiceTimeout)); }
                set { writeKeyValue(Resources.ServiceTimeout, value); }
            }

            public int webServiceTimeout
            {
                get { return Convert.ToInt32(readKeyValue(Resources.WebServiceTimeout)); }
                set { writeKeyValue(Resources.WebServiceTimeout, value); }
            }

            public string vpnServiceName
            {
                get { return readKeyValue(Resources.VPNServiceName); }
                set { writeKeyValue(Resources.VPNServiceName, value); }
            }

            public string vpnServiceConfigFile
            {
                get { return readKeyValue(Resources.VPNServiceConfigFile); }
                set { writeKeyValue(Resources.VPNServiceConfigFile, value); }
            }

            public string vpnServiceLogFile
            {
                get { return readKeyValue(Resources.VPNServiceLogFile); }
                set { writeKeyValue(Resources.VPNServiceLogFile, value); }
            }

            public string vncServiceName
            {
                get { return readKeyValue(Resources.VNCServiceName); }
                set { writeKeyValue(Resources.VNCServiceName, value); }
            }

            public int soapPort
            {
                get { return Convert.ToInt32(readKeyValue(Resources.SOAPPort)); }
                set { writeKeyValue(Resources.SOAPPort, value); }
            }

            public int soapDelay
            {
                get { return Convert.ToInt32(readKeyValue(Resources.SOAPDelay)); }
                set { writeKeyValue(Resources.SOAPDelay, value); }
            }

            public int soapTimeout
            {
                get { return Convert.ToInt32(readKeyValue(Resources.SOAPTimeout)); }
                set { writeKeyValue(Resources.SOAPTimeout, value); }
            }

            public int soapInterval
            {
                get { return Convert.ToInt32(readKeyValue(Resources.SOAPInterval)); }
                set { writeKeyValue(Resources.SOAPInterval, value); }
            }

            public string soapUsername
            {
                get { return readKeyValue(Resources.SOAPUsername); }
                set { writeKeyValue(Resources.SOAPUsername, value); }
            }

            public string soapPassword
            {
                get { return readKeyValue(Resources.SOAPPassword); }
                set { writeKeyValue(Resources.SOAPPassword, value); }
            }
            public string soapResources
            {
                get { return readKeyValue(Resources.SOAPResource); }
                set { writeKeyValue(Resources.SOAPResource, value); }
            }
            public bool isDisplayQueryWindow
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.QuerySetting)); }
                set { writeKeyValue(Resources.QuerySetting, value); }                 
            }
            public bool isQueryIfNoLogon
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.QueryIfNoLogon)); }
                set { writeKeyValue(Resources.QueryIfNoLogon, value); }
            }
            public int queryTimeout
            {
                get 
                {
                    int timeout = Convert.ToInt32(readKeyValue(Resources.QueryTimeout)); 
                    const int DEFAULT_TIMEOUT = 1;
                    return timeout == 0 ? DEFAULT_TIMEOUT : timeout; 
                }
                set { writeKeyValue(Resources.QueryTimeout, value); }
            }
            public bool isEnableFileTransfer
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.FileTransferEnabled)); }
                set { writeKeyValue(Resources.FileTransferEnabled, value); }
            }            
            public bool isAllowMultipleConnections
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.ConnectPriority)); }
                set { writeKeyValue(Resources.ConnectPriority, value); }
            }
            public bool isLockOnDisconnect
            {
                get { return Convert.ToBoolean(readKeyValue(Resources.LockSetting)); }
                set { writeKeyValue(Resources.LockSetting, value); }
            }
            public string queryMessage
            {
                get { return readKeyValue(Resources.AcceptRejectMesg); }
                set { writeKeyValue(Resources.AcceptRejectMesg, value); }                
            }

            internal void save()
            {
                m_config.Save(ConfigurationSaveMode.Modified);
            }

            private string readKeyValue(string key)
            {
                KeyValueConfigurationElement elem = m_collection[key];
                return elem == null ? null : elem.Value;
            }

            private void writeKeyValue(string key, object value)
            {
                string strValue = Convert.ToString(value);
                KeyValueConfigurationElement elem = m_collection[key];
                if (elem == null)
                {
                    m_collection.Add(key, strValue);
                } 
                else {
                    elem.Value = strValue;
                }
                m_config.Save(ConfigurationSaveMode.Modified);
            }
        }

        public static void save()
        {
            s_instance.save();
        }
    }
}