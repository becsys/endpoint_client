﻿using System;
using System.Configuration;
using cms_client_tray.Properties;
using cms_transport;

namespace cms_client_tray
{
    partial class CMSSettings
    {
        private static CMSSettingsViaProtocolImpl s_instance = new CMSSettingsViaProtocolImpl();

        private class CMSSettingsViaProtocolImpl
        {

            public CMSSettingsViaProtocolImpl() { }

            public ConnectionStringsSection ConnectionStrings { get; set; }
            public bool isAutoStart { get; set; }
            public bool isLockdown { get; set; }
            public string defaultConnection { get; set; }
            public int serviceTimeout { get; set; }
            public int webServiceTimeout { get; set; }
            public string vpnServiceName { get; set; }
            public string vpnServiceConfigFile { get; set; }
            public string vpnServiceLogFile { get; set; }
            public string vncServiceName { get; set; }
            public int soapPort { get; set; }
            public int soapDelay { get; set; }
            public int soapTimeout { get; set; }
            public int soapInterval { get; set; }
            public string soapUsername { get; set; }
            public string soapPassword { get; set; }
            public string soapResources { get; set; }
            public bool isDisplayQueryWindow { get; set; }
            public bool isQueryIfNoLogon { get; set; }
            public int queryTimeout { get; set; }
            public bool isEnableFileTransfer { get; set; }
            public bool isAllowMultipleConnections { get; set; }
            public bool isLockOnDisconnect { get; set; }
            public string queryMessage { get; set; }
            public string version { get; set; }
            public int connectionTimeout { get; set; }

            public void saveProfiles()
            {
                ClientProtocol.updateProfiles();
            }

            public void saveGlobal()
            {
                ClientProtocol.updateSettings();
            }
        }

        public static void loadFromResponse(CMSRegisterResponse responce)
        {
            CMSSettings.connectionName = responce.connectionName;
            CMSSettings.version = responce.version;
            CMSSettings.connections.Clear();
            foreach (Profile profile in responce.profiles) 
            {
                // todo: fill other fields
                Connection connection = new Connection(profile.name,
                    Convert.ToBoolean(profile.isConfigured), 
                    Convert.ToBoolean(profile.requestEnabled),
                    profile.requestURL,
                    Convert.ToInt32(profile.requestFrequency),
                    profile.managerAddress, 
                    Convert.ToInt32(profile.managerPort), 
                    profile.managerVPNAddress,
                    Convert.ToInt32(profile.managerSOAPPort));
                connections.Add(connection.name, connection);
            }
            cms_transport.Settings settings = responce.settings;
            isAutoStart = Convert.ToBoolean(settings.autostart);
            isLockdown = Convert.ToBoolean(settings.lockdown);
            isDisplayQueryWindow = Convert.ToBoolean(settings.querySetting);
            queryTimeout = Convert.ToInt32(settings.queryTimeout);
            queryMessage = settings.accept_reject_mesg;
            isLockOnDisconnect = Convert.ToBoolean(settings.lockSetting);
            isAllowMultipleConnections = Convert.ToBoolean(settings.connectPriority);
            isEnableFileTransfer = Convert.ToBoolean(settings.fileTransferEnabled);
            isQueryIfNoLogon = Convert.ToBoolean(settings.queryIfNoLogon);
            connectionTimeout = Convert.ToInt32(settings.connectionTimeout);
        }
    }
}