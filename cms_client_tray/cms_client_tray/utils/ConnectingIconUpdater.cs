﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using cms_client_tray.Properties;

namespace cms_client_tray.utils
{
    // special class to update tray icon in connecting status
    class ConnectingIconUpdater
    {
        ConnectForm m_form;
        private const int UPDATE_INTERVAL = 1000;

        private Icon[] m_statusIcons = null;
        private int m_iconIndex = 0;

        private Thread m_updaterThread;
        private int m_isThreadRunning = 0;

        public ConnectingIconUpdater(ConnectForm form)
        {
            m_form = form;

            m_statusIcons = new Icon[4];
            m_statusIcons[0] = Resources.Connecting;
            m_statusIcons[1] = Resources.Connecting_2;
            m_statusIcons[2] = Resources.Connecting_3;
            m_statusIcons[3] = Resources.Connecting_4;

            m_updaterThread = new Thread(new ThreadStart(threadProc));
            m_updaterThread.Start();
        }

        public void startUpdate()
        {            
            Interlocked.Exchange(ref m_isThreadRunning, 1);
        }

        public void stopUpdate()
        {
            Interlocked.Exchange(ref m_isThreadRunning, 0);
            m_iconIndex = 0;
        }

        private void threadProc()
        {
            while (true)
            {
                if (m_isThreadRunning == 1)
                {
                    m_form.BeginInvoke(new MethodInvoker(delegate
                    {
                        m_form.setTrayIcon(m_statusIcons[m_iconIndex++]);
                        if (m_iconIndex == 4) m_iconIndex = 0;
                    }));
                }
                Thread.Sleep(UPDATE_INTERVAL);
            }
        }
    }
}
