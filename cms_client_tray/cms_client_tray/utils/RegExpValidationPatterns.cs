﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cms_client_tray
{
    class RegExpValidationPatterns
    {
        public const string NameValidationPattern = @"^[A-Za-z-_][A-Za-z0-9-_.]{2,10}$";
        public const string DNSAddressValidationPattern = @" # Rev:2013-03-26
            # Match DNS host domain having one or more subdomains.
            # Top level domain subset taken from IANA.ORG. See:
            # http://data.iana.org/TLD/tlds-alpha-by-domain.txt
            ^                  # Anchor to start of string.
            (?!.{256})         # Whole domain must be 255 or less.
            (?:                # Group for one or more sub-domains.
            [a-z0-9]         # Either subdomain length from 2-63.
            [a-z0-9-]{0,61}  # Middle part may have dashes.
            [a-z0-9]         # Starts and ends with alphanum.
            \.               # Dot separates subdomains.
            | [a-z0-9]         # or subdomain length == 1 char.
            \.               # Dot separates subdomains.
            )+                 # One or more sub-domains.
            (?:                # Top level domain alternatives.
            [a-z]{2}         # Either any 2 char country code,
            | AERO|ARPA|ASIA|BIZ|CAT|COM|COOP|EDU|  # or TLD 
            GOV|INFO|INT|JOBS|MIL|MOBI|MUSEUM|    # from list.
            NAME|NET|ORG|POST|PRO|TEL|TRAVEL|XXX  # IANA.ORG
            )                  # End group of TLD alternatives.
            $                  # Anchor to end of string.";

        public const string IPAddressValidationPattern = 
            @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

        public const string URLValidationPattern =
            @"^(?#Protocol)(?:(?:ht|f)tp(?:s?)\:\/\/|~\/|\/)?(?#Username:Password)(?:\w+:\w+@)?(?#Subdomains)(?:(?:[-\w]+\.)+(?#TopLevel Domains)(?:com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel|[a-z]{2}))(?#Port)(?::[\d]{1,5})?(?#Directories)(?:(?:(?:\/(?:[-\w~!$+|.,=]|%[a-f\d]{2})+)+|\/)+|\?|#)?(?#Query)(?:(?:\?(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)(?:&(?:[-\w~!$+|.,*:]|%[a-f\d{2}])+=?(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)*)*(?#Anchor)(?:#(?:[-\w~!$+|.,*:=]|%[a-f\d]{2})*)?$";
    }
}
