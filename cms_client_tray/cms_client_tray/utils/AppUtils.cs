﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using cms_client_tray.Properties;


namespace cms_client_tray.utils
{
    class AppUtils
    {
        private static Mutex mInstanceMutex = null;

        public static bool hasSecondInstance()
        {
            bool cn;
            mInstanceMutex = new System.Threading.Mutex(true, "cms_client_tray", out cn);
            return !mInstanceMutex.WaitOne(0, false);
        }

        public static void ckeckResponceResult(string result)
        {
            if (result != "OK")
            {
                MessageBox.Show("Error: Can't execute command", "BE CMS Endpoint Client");
                System.Environment.Exit(1);
            }
        }

        public static void exitOnConnectError()
        {
            MessageBox.Show("Error: Can't connect to service", "BE CMS Endpoint Client");
            System.Environment.Exit(1);
        }

        public static void updateFormCursor()
        {
            Form f = Form.ActiveForm;
            if (f != null && f.Handle != IntPtr.Zero)   // Send WM_SETCURSOR
                SendMessage(f.Handle, 0x20, f.Handle, (IntPtr)1);
            Application.DoEvents();
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        public static void closeAllForms()
        {
            foreach (Form form in Application.OpenForms)
            {
                form.Close();
            }
        }

        public static void showAbout(Form owner)
        {
            InfoForm info_frm = new InfoForm();
            owner.TopMost = false;
            info_frm.Owner = owner;
            info_frm.ShowDialog();
        }

        public static string getCurrAddressStr()
        {
            return String.IsNullOrEmpty(CMSSettings.connectionName) ? Resources.labelNotAvailable :
                CMSSettings.connections[CMSSettings.connectionName].managerAddress;
        }

        public static string getCurrTCPPortStr()
        {
            return String.IsNullOrEmpty(CMSSettings.connectionName) ? Resources.labelNotAvailable :
                Convert.ToString(CMSSettings.connections[CMSSettings.connectionName].managerPort);
        }

        public static string getCurrVPNAddressStr()
        {
            return String.IsNullOrEmpty(CMSSettings.connectionName) ? Resources.labelNotAvailable :
                CMSSettings.connections[CMSSettings.connectionName].managerVPNAddress;
        }
    }
}
