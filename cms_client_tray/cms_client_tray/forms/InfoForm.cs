﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using cms_client_tray.Properties;
using cms_client_tray.utils;

namespace cms_client_tray
{
    public partial class InfoForm : Form
    {
        public InfoForm()
        {
            InitializeComponent();
        }

        private void InfoForm_Load(object sender, EventArgs e)
        {
            //place form on bottom-right
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);

            //load skin
            this.BackgroundImage = Resources.Info_canvas;
            ClosePictureBox.BackgroundImage = Resources.Info_close_leave;
            MinimizePictureBox.BackgroundImage = Resources.Info_minimize_leave;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;
        }

        private void InfoRichTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ClosePictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.closeAllForms();
        }

        private void MinimizePictureBox_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ClosePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Info_close_leave;
        }

        private void ClosePictureBox_MouseHover(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Info_close_hover;
        }

        private void MinimizePictureBox_MouseLeave(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Info_minimize_leave;
        }

        private void MinimizePictureBox_MouseHover(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Info_minimize_hover;
        }

        private void TreePictureBox_Click(object sender, EventArgs e)
        {
            Process.Start(Resources.LinkBecSys); 
        } 
    }
}
