﻿namespace cms_client_tray
{
    partial class IdentityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IdentityForm));
            this.SCEPRadioButton = new System.Windows.Forms.RadioButton();
            this.LocalRadioButton = new System.Windows.Forms.RadioButton();
            this.ClientKEYState = new System.Windows.Forms.Label();
            this.ClientCERTState = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PassphraseLabel = new System.Windows.Forms.Label();
            this.PassphraseTextBox = new System.Windows.Forms.TextBox();
            this.UrlLabel = new System.Windows.Forms.Label();
            this.UrlTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.IssueCAState = new System.Windows.Forms.Label();
            this.CAOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.CERTOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.KEYOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label6 = new System.Windows.Forms.Label();
            this.TPNameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CancelButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.ApplyButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.CABrowseButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.KeyBrowseButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.CertBrowseButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.MinimizePictureBox = new System.Windows.Forms.PictureBox();
            this.ClosePictureBox = new System.Windows.Forms.PictureBox();
            this.BecSysLinkBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CABrowseButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeyBrowseButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBrowseButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BecSysLinkBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // SCEPRadioButton
            // 
            this.SCEPRadioButton.AutoSize = true;
            this.SCEPRadioButton.BackColor = System.Drawing.Color.White;
            this.SCEPRadioButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SCEPRadioButton.Location = new System.Drawing.Point(369, 172);
            this.SCEPRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.SCEPRadioButton.Name = "SCEPRadioButton";
            this.SCEPRadioButton.Size = new System.Drawing.Size(121, 19);
            this.SCEPRadioButton.TabIndex = 3;
            this.SCEPRadioButton.TabStop = true;
            this.SCEPRadioButton.Text = "SCEP Enrollment";
            this.SCEPRadioButton.UseVisualStyleBackColor = false;
            this.SCEPRadioButton.Click += new System.EventHandler(this.RadioButtons_Click);
            // 
            // LocalRadioButton
            // 
            this.LocalRadioButton.AutoSize = true;
            this.LocalRadioButton.BackColor = System.Drawing.Color.White;
            this.LocalRadioButton.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LocalRadioButton.Location = new System.Drawing.Point(369, 150);
            this.LocalRadioButton.Margin = new System.Windows.Forms.Padding(2);
            this.LocalRadioButton.Name = "LocalRadioButton";
            this.LocalRadioButton.Size = new System.Drawing.Size(118, 19);
            this.LocalRadioButton.TabIndex = 2;
            this.LocalRadioButton.TabStop = true;
            this.LocalRadioButton.Text = "Local Enrollment";
            this.LocalRadioButton.UseVisualStyleBackColor = false;
            this.LocalRadioButton.Click += new System.EventHandler(this.RadioButtons_Click);
            // 
            // ClientKEYState
            // 
            this.ClientKEYState.AutoSize = true;
            this.ClientKEYState.BackColor = System.Drawing.Color.White;
            this.ClientKEYState.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientKEYState.Location = new System.Drawing.Point(77, 310);
            this.ClientKEYState.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ClientKEYState.Name = "ClientKEYState";
            this.ClientKEYState.Size = new System.Drawing.Size(67, 15);
            this.ClientKEYState.TabIndex = 4;
            this.ClientKEYState.Text = "Not loaded";
            // 
            // ClientCERTState
            // 
            this.ClientCERTState.AutoSize = true;
            this.ClientCERTState.BackColor = System.Drawing.Color.White;
            this.ClientCERTState.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientCERTState.Location = new System.Drawing.Point(76, 271);
            this.ClientCERTState.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ClientCERTState.Name = "ClientCERTState";
            this.ClientCERTState.Size = new System.Drawing.Size(67, 15);
            this.ClientCERTState.TabIndex = 3;
            this.ClientCERTState.Text = "Not loaded";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(76, 292);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Client Key:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(76, 253);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Client Cert:";
            // 
            // PassphraseLabel
            // 
            this.PassphraseLabel.AutoSize = true;
            this.PassphraseLabel.BackColor = System.Drawing.Color.White;
            this.PassphraseLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PassphraseLabel.Location = new System.Drawing.Point(362, 283);
            this.PassphraseLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PassphraseLabel.Name = "PassphraseLabel";
            this.PassphraseLabel.Size = new System.Drawing.Size(78, 15);
            this.PassphraseLabel.TabIndex = 3;
            this.PassphraseLabel.Text = "Passphrase:";
            // 
            // PassphraseTextBox
            // 
            this.PassphraseTextBox.Font = new System.Drawing.Font("Arial", 8F);
            this.PassphraseTextBox.Location = new System.Drawing.Point(447, 281);
            this.PassphraseTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.PassphraseTextBox.Name = "PassphraseTextBox";
            this.PassphraseTextBox.Size = new System.Drawing.Size(83, 20);
            this.PassphraseTextBox.TabIndex = 7;
            // 
            // UrlLabel
            // 
            this.UrlLabel.AutoSize = true;
            this.UrlLabel.BackColor = System.Drawing.Color.White;
            this.UrlLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UrlLabel.Location = new System.Drawing.Point(362, 259);
            this.UrlLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.UrlLabel.Name = "UrlLabel";
            this.UrlLabel.Size = new System.Drawing.Size(35, 15);
            this.UrlLabel.TabIndex = 1;
            this.UrlLabel.Text = "URL:";
            // 
            // UrlTextBox
            // 
            this.UrlTextBox.Font = new System.Drawing.Font("Arial", 8F);
            this.UrlTextBox.Location = new System.Drawing.Point(447, 258);
            this.UrlTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.UrlTextBox.Name = "UrlTextBox";
            this.UrlTextBox.Size = new System.Drawing.Size(138, 20);
            this.UrlTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(76, 182);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Issue CA Cert:";
            // 
            // IssueCAState
            // 
            this.IssueCAState.AutoSize = true;
            this.IssueCAState.BackColor = System.Drawing.Color.White;
            this.IssueCAState.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IssueCAState.Location = new System.Drawing.Point(93, 201);
            this.IssueCAState.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IssueCAState.Name = "IssueCAState";
            this.IssueCAState.Size = new System.Drawing.Size(67, 15);
            this.IssueCAState.TabIndex = 2;
            this.IssueCAState.Text = "Not loaded";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(76, 154);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Transpoint Name:";
            // 
            // TPNameTextBox
            // 
            this.TPNameTextBox.Font = new System.Drawing.Font("Arial", 8F);
            this.TPNameTextBox.Location = new System.Drawing.Point(195, 152);
            this.TPNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.TPNameTextBox.Name = "TPNameTextBox";
            this.TPNameTextBox.Size = new System.Drawing.Size(111, 20);
            this.TPNameTextBox.TabIndex = 0;
            this.TPNameTextBox.Leave += new System.EventHandler(this.TPNameTextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(343, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Key Management";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(54, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Local Key Storage";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label9.Location = new System.Drawing.Point(343, 232);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "SCEP Key Storage";
            // 
            // CancelButtonPictureBox
            // 
            this.CancelButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CancelButtonPictureBox.BackgroundImage")));
            this.CancelButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CancelButtonPictureBox.Location = new System.Drawing.Point(474, 337);
            this.CancelButtonPictureBox.Name = "CancelButtonPictureBox";
            this.CancelButtonPictureBox.Size = new System.Drawing.Size(111, 26);
            this.CancelButtonPictureBox.TabIndex = 17;
            this.CancelButtonPictureBox.TabStop = false;
            this.CancelButtonPictureBox.Click += new System.EventHandler(this.CancelButtonPictureBox_Click);
            this.CancelButtonPictureBox.MouseLeave += new System.EventHandler(this.CancelButtonPictureBox_MouseLeave);
            this.CancelButtonPictureBox.MouseHover += new System.EventHandler(this.CancelButtonPictureBox_MouseHover);
            // 
            // ApplyButtonPictureBox
            // 
            this.ApplyButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ApplyButtonPictureBox.BackgroundImage")));
            this.ApplyButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ApplyButtonPictureBox.Location = new System.Drawing.Point(354, 337);
            this.ApplyButtonPictureBox.Name = "ApplyButtonPictureBox";
            this.ApplyButtonPictureBox.Size = new System.Drawing.Size(111, 26);
            this.ApplyButtonPictureBox.TabIndex = 16;
            this.ApplyButtonPictureBox.TabStop = false;
            this.ApplyButtonPictureBox.Click += new System.EventHandler(this.ApplyButtonPictureBox_Click);
            this.ApplyButtonPictureBox.MouseLeave += new System.EventHandler(this.ApplyButtonPictureBox_MouseLeave);
            this.ApplyButtonPictureBox.MouseHover += new System.EventHandler(this.ApplyButtonPictureBox_MouseHover);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(54, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "General Settings";
            // 
            // CABrowseButtonPictureBox
            // 
            this.CABrowseButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CABrowseButtonPictureBox.BackgroundImage")));
            this.CABrowseButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CABrowseButtonPictureBox.Location = new System.Drawing.Point(195, 187);
            this.CABrowseButtonPictureBox.Name = "CABrowseButtonPictureBox";
            this.CABrowseButtonPictureBox.Size = new System.Drawing.Size(111, 26);
            this.CABrowseButtonPictureBox.TabIndex = 19;
            this.CABrowseButtonPictureBox.TabStop = false;
            this.CABrowseButtonPictureBox.Click += new System.EventHandler(this.CABrowseButtonPictureBox_Click);
            this.CABrowseButtonPictureBox.MouseLeave += new System.EventHandler(this.CABrowseButtonPictureBox_MouseLeave);
            this.CABrowseButtonPictureBox.MouseHover += new System.EventHandler(this.CABrowseButtonPictureBox_MouseHover);
            // 
            // KeyBrowseButtonPictureBox
            // 
            this.KeyBrowseButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("KeyBrowseButtonPictureBox.BackgroundImage")));
            this.KeyBrowseButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.KeyBrowseButtonPictureBox.Location = new System.Drawing.Point(195, 295);
            this.KeyBrowseButtonPictureBox.Name = "KeyBrowseButtonPictureBox";
            this.KeyBrowseButtonPictureBox.Size = new System.Drawing.Size(111, 26);
            this.KeyBrowseButtonPictureBox.TabIndex = 20;
            this.KeyBrowseButtonPictureBox.TabStop = false;
            this.KeyBrowseButtonPictureBox.Click += new System.EventHandler(this.KeyBrowseButtonPictureBox_Click);
            this.KeyBrowseButtonPictureBox.MouseLeave += new System.EventHandler(this.KeyBrowseButtonPictureBox_MouseLeave);
            this.KeyBrowseButtonPictureBox.MouseHover += new System.EventHandler(this.KeyBrowseButtonPictureBox_MouseHover);
            // 
            // CertBrowseButtonPictureBox
            // 
            this.CertBrowseButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CertBrowseButtonPictureBox.BackgroundImage")));
            this.CertBrowseButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CertBrowseButtonPictureBox.Location = new System.Drawing.Point(195, 256);
            this.CertBrowseButtonPictureBox.Name = "CertBrowseButtonPictureBox";
            this.CertBrowseButtonPictureBox.Size = new System.Drawing.Size(111, 29);
            this.CertBrowseButtonPictureBox.TabIndex = 21;
            this.CertBrowseButtonPictureBox.TabStop = false;
            this.CertBrowseButtonPictureBox.Click += new System.EventHandler(this.CertBrowseButtonPictureBox_Click);
            this.CertBrowseButtonPictureBox.MouseLeave += new System.EventHandler(this.CertBrowseButtonPictureBox_MouseLeave);
            this.CertBrowseButtonPictureBox.MouseHover += new System.EventHandler(this.CertBrowseButtonPictureBox_MouseHover);
            // 
            // MinimizePictureBox
            // 
            this.MinimizePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizePictureBox.BackgroundImage")));
            this.MinimizePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MinimizePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizePictureBox.Location = new System.Drawing.Point(606, 7);
            this.MinimizePictureBox.Name = "MinimizePictureBox";
            this.MinimizePictureBox.Size = new System.Drawing.Size(10, 10);
            this.MinimizePictureBox.TabIndex = 22;
            this.MinimizePictureBox.TabStop = false;
            this.MinimizePictureBox.Click += new System.EventHandler(this.MinimizePictureBox_Click);
            this.MinimizePictureBox.MouseLeave += new System.EventHandler(this.MinimizePictureBox_MouseLeave);
            this.MinimizePictureBox.MouseHover += new System.EventHandler(this.MinimizePictureBox_MouseHover);
            // 
            // ClosePictureBox
            // 
            this.ClosePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ClosePictureBox.BackgroundImage")));
            this.ClosePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClosePictureBox.Location = new System.Drawing.Point(621, 7);
            this.ClosePictureBox.Name = "ClosePictureBox";
            this.ClosePictureBox.Size = new System.Drawing.Size(10, 10);
            this.ClosePictureBox.TabIndex = 23;
            this.ClosePictureBox.TabStop = false;
            this.ClosePictureBox.Click += new System.EventHandler(this.ClosePictureBox_Click);
            this.ClosePictureBox.MouseLeave += new System.EventHandler(this.ClosePictureBox_MouseLeave);
            this.ClosePictureBox.MouseHover += new System.EventHandler(this.ClosePictureBox_MouseHover);
            // 
            // BecSysLinkBtn
            // 
            this.BecSysLinkBtn.BackColor = System.Drawing.Color.Transparent;
            this.BecSysLinkBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BecSysLinkBtn.Location = new System.Drawing.Point(21, 12);
            this.BecSysLinkBtn.Name = "BecSysLinkBtn";
            this.BecSysLinkBtn.Size = new System.Drawing.Size(118, 46);
            this.BecSysLinkBtn.TabIndex = 24;
            this.BecSysLinkBtn.TabStop = false;
            this.BecSysLinkBtn.Click += new System.EventHandler(this.BecSysLinkBtn_Click);
            // 
            // IdentityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(640, 425);
            this.Controls.Add(this.BecSysLinkBtn);
            this.Controls.Add(this.ClosePictureBox);
            this.Controls.Add(this.MinimizePictureBox);
            this.Controls.Add(this.CertBrowseButtonPictureBox);
            this.Controls.Add(this.KeyBrowseButtonPictureBox);
            this.Controls.Add(this.CABrowseButtonPictureBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CancelButtonPictureBox);
            this.Controls.Add(this.TPNameTextBox);
            this.Controls.Add(this.IssueCAState);
            this.Controls.Add(this.ApplyButtonPictureBox);
            this.Controls.Add(this.PassphraseLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.PassphraseTextBox);
            this.Controls.Add(this.UrlLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.UrlTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LocalRadioButton);
            this.Controls.Add(this.ClientKEYState);
            this.Controls.Add(this.SCEPRadioButton);
            this.Controls.Add(this.ClientCERTState);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IdentityForm";
            this.ShowIcon = false;
            this.Text = "Identity";
            this.Load += new System.EventHandler(this.IdentityForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CancelButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CABrowseButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeyBrowseButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CertBrowseButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BecSysLinkBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton LocalRadioButton;
        private System.Windows.Forms.RadioButton SCEPRadioButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ClientKEYState;
        private System.Windows.Forms.Label ClientCERTState;
        private System.Windows.Forms.Label IssueCAState;
        private System.Windows.Forms.OpenFileDialog CAOpenFileDialog;
        private System.Windows.Forms.OpenFileDialog CERTOpenFileDialog;
        private System.Windows.Forms.OpenFileDialog KEYOpenFileDialog;
        private System.Windows.Forms.Label PassphraseLabel;
        private System.Windows.Forms.TextBox PassphraseTextBox;
        private System.Windows.Forms.Label UrlLabel;
        private System.Windows.Forms.TextBox UrlTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TPNameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox CancelButtonPictureBox;
        private System.Windows.Forms.PictureBox ApplyButtonPictureBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox CABrowseButtonPictureBox;
        private System.Windows.Forms.PictureBox KeyBrowseButtonPictureBox;
        private System.Windows.Forms.PictureBox CertBrowseButtonPictureBox;
        private System.Windows.Forms.PictureBox MinimizePictureBox;
        private System.Windows.Forms.PictureBox ClosePictureBox;
        private System.Windows.Forms.PictureBox BecSysLinkBtn;

    }
}