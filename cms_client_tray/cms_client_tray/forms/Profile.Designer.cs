﻿namespace cms_client_tray
{
    partial class ProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfileForm));
            this.label6 = new System.Windows.Forms.Label();
            this.ProfileManagerVPNAddress = new System.Windows.Forms.TextBox();
            this.ProfileManagerAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ProfileFrequency = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ProfileURL = new System.Windows.Forms.TextBox();
            this.ProfileEstablishAfterRequest = new System.Windows.Forms.CheckBox();
            this.ApplyButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.CancelButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.ClosePictureBox = new System.Windows.Forms.PictureBox();
            this.MinimizePictureBox = new System.Windows.Forms.PictureBox();
            this.becsysLinkBtn = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ProfileManagerTCPport = new System.Windows.Forms.MaskedTextBox();
            this.ProfileConnectionName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.becsysLinkBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.Location = new System.Drawing.Point(76, 143);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Connection Name:";
            // 
            // ProfileManagerVPNAddress
            // 
            this.ProfileManagerVPNAddress.Font = new System.Drawing.Font("Arial", 8F);
            this.ProfileManagerVPNAddress.Location = new System.Drawing.Point(222, 211);
            this.ProfileManagerVPNAddress.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileManagerVPNAddress.Name = "ProfileManagerVPNAddress";
            this.ProfileManagerVPNAddress.Size = new System.Drawing.Size(286, 20);
            this.ProfileManagerVPNAddress.TabIndex = 3;
            this.ProfileManagerVPNAddress.TextChanged += new System.EventHandler(this.ProfileManagerVPNAddress_TextChanged);
            this.ProfileManagerVPNAddress.Leave += new System.EventHandler(this.ProfileManagerVPNAddress_Leave);
            // 
            // ProfileManagerAddress
            // 
            this.ProfileManagerAddress.BackColor = System.Drawing.SystemColors.Window;
            this.ProfileManagerAddress.Font = new System.Drawing.Font("Arial", 8F);
            this.ProfileManagerAddress.Location = new System.Drawing.Point(222, 164);
            this.ProfileManagerAddress.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileManagerAddress.Name = "ProfileManagerAddress";
            this.ProfileManagerAddress.Size = new System.Drawing.Size(286, 20);
            this.ProfileManagerAddress.TabIndex = 1;
            this.ProfileManagerAddress.TextChanged += new System.EventHandler(this.ProfileManagerAddress_TextChanged);
            this.ProfileManagerAddress.Leave += new System.EventHandler(this.ProfileManagerAddress_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.Location = new System.Drawing.Point(76, 213);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Manager VPN Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.Location = new System.Drawing.Point(76, 190);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Manager TCP port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.Location = new System.Drawing.Point(76, 166);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Manager Address:";
            // 
            // ProfileFrequency
            // 
            this.ProfileFrequency.Font = new System.Drawing.Font("Arial", 8F);
            this.ProfileFrequency.Location = new System.Drawing.Point(471, 296);
            this.ProfileFrequency.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileFrequency.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.ProfileFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ProfileFrequency.Name = "ProfileFrequency";
            this.ProfileFrequency.Size = new System.Drawing.Size(37, 20);
            this.ProfileFrequency.TabIndex = 7;
            this.ProfileFrequency.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.Location = new System.Drawing.Point(398, 298);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Freq (min.):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.Location = new System.Drawing.Point(95, 299);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Control URL:";
            // 
            // ProfileURL
            // 
            this.ProfileURL.Font = new System.Drawing.Font("Arial", 8F);
            this.ProfileURL.Location = new System.Drawing.Point(177, 297);
            this.ProfileURL.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileURL.Name = "ProfileURL";
            this.ProfileURL.Size = new System.Drawing.Size(201, 20);
            this.ProfileURL.TabIndex = 6;
            this.ProfileURL.Leave += new System.EventHandler(this.ProfileURL_Leave);
            // 
            // ProfileEstablishAfterRequest
            // 
            this.ProfileEstablishAfterRequest.AutoSize = true;
            this.ProfileEstablishAfterRequest.BackColor = System.Drawing.Color.White;
            this.ProfileEstablishAfterRequest.Font = new System.Drawing.Font("Arial", 9F);
            this.ProfileEstablishAfterRequest.Location = new System.Drawing.Point(79, 274);
            this.ProfileEstablishAfterRequest.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileEstablishAfterRequest.Name = "ProfileEstablishAfterRequest";
            this.ProfileEstablishAfterRequest.Size = new System.Drawing.Size(238, 19);
            this.ProfileEstablishAfterRequest.TabIndex = 5;
            this.ProfileEstablishAfterRequest.Text = "Establish connection only after request";
            this.ProfileEstablishAfterRequest.UseVisualStyleBackColor = false;
            this.ProfileEstablishAfterRequest.CheckedChanged += new System.EventHandler(this.ProfileEstablishAfterRequest_CheckedChanged);
            // 
            // ApplyButtonPictureBox
            // 
            this.ApplyButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ApplyButtonPictureBox.BackgroundImage")));
            this.ApplyButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ApplyButtonPictureBox.Location = new System.Drawing.Point(354, 338);
            this.ApplyButtonPictureBox.Name = "ApplyButtonPictureBox";
            this.ApplyButtonPictureBox.Size = new System.Drawing.Size(111, 24);
            this.ApplyButtonPictureBox.TabIndex = 7;
            this.ApplyButtonPictureBox.TabStop = false;
            this.ApplyButtonPictureBox.Click += new System.EventHandler(this.ApplyButtonPictureBox_Click);
            this.ApplyButtonPictureBox.MouseLeave += new System.EventHandler(this.ApplyButtonPictureBox_MouseLeave);
            this.ApplyButtonPictureBox.MouseHover += new System.EventHandler(this.ApplyButtonPictureBox_MouseHover);
            // 
            // CancelButtonPictureBox
            // 
            this.CancelButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CancelButtonPictureBox.BackgroundImage")));
            this.CancelButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CancelButtonPictureBox.Location = new System.Drawing.Point(474, 338);
            this.CancelButtonPictureBox.Name = "CancelButtonPictureBox";
            this.CancelButtonPictureBox.Size = new System.Drawing.Size(111, 24);
            this.CancelButtonPictureBox.TabIndex = 8;
            this.CancelButtonPictureBox.TabStop = false;
            this.CancelButtonPictureBox.Click += new System.EventHandler(this.CancelButtonPictureBox_Click);
            this.CancelButtonPictureBox.MouseLeave += new System.EventHandler(this.CancelButtonPictureBox_MouseLeave);
            this.CancelButtonPictureBox.MouseHover += new System.EventHandler(this.CancelButtonPictureBox_MouseHover);
            // 
            // ClosePictureBox
            // 
            this.ClosePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ClosePictureBox.BackgroundImage")));
            this.ClosePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClosePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClosePictureBox.Location = new System.Drawing.Point(621, 7);
            this.ClosePictureBox.Name = "ClosePictureBox";
            this.ClosePictureBox.Size = new System.Drawing.Size(10, 10);
            this.ClosePictureBox.TabIndex = 9;
            this.ClosePictureBox.TabStop = false;
            this.ClosePictureBox.Click += new System.EventHandler(this.ClosePictureBox_Click);
            this.ClosePictureBox.MouseLeave += new System.EventHandler(this.ClosePictureBox_MouseLeave);
            this.ClosePictureBox.MouseHover += new System.EventHandler(this.ClosePictureBox_MouseHover);
            // 
            // MinimizePictureBox
            // 
            this.MinimizePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizePictureBox.BackgroundImage")));
            this.MinimizePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MinimizePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizePictureBox.Location = new System.Drawing.Point(606, 7);
            this.MinimizePictureBox.Name = "MinimizePictureBox";
            this.MinimizePictureBox.Size = new System.Drawing.Size(10, 10);
            this.MinimizePictureBox.TabIndex = 10;
            this.MinimizePictureBox.TabStop = false;
            this.MinimizePictureBox.Click += new System.EventHandler(this.MinimizePictureBox_Click);
            this.MinimizePictureBox.MouseLeave += new System.EventHandler(this.MinimizePictureBox_MouseLeave);
            this.MinimizePictureBox.MouseHover += new System.EventHandler(this.MinimizePictureBox_MouseHover);
            // 
            // becsysLinkBtn
            // 
            this.becsysLinkBtn.BackColor = System.Drawing.Color.Transparent;
            this.becsysLinkBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.becsysLinkBtn.Location = new System.Drawing.Point(19, 9);
            this.becsysLinkBtn.Name = "becsysLinkBtn";
            this.becsysLinkBtn.Size = new System.Drawing.Size(118, 50);
            this.becsysLinkBtn.TabIndex = 11;
            this.becsysLinkBtn.TabStop = false;
            this.becsysLinkBtn.Click += new System.EventHandler(this.becsysLinkBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(54, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Connection Settings";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(54, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 15);
            this.label8.TabIndex = 13;
            this.label8.Text = "Client Settings";
            // 
            // ProfileManagerTCPport
            // 
            this.ProfileManagerTCPport.HidePromptOnLeave = true;
            this.ProfileManagerTCPport.Location = new System.Drawing.Point(222, 188);
            this.ProfileManagerTCPport.Mask = "09999";
            this.ProfileManagerTCPport.Name = "ProfileManagerTCPport";
            this.ProfileManagerTCPport.RejectInputOnFirstFailure = true;
            this.ProfileManagerTCPport.Size = new System.Drawing.Size(286, 20);
            this.ProfileManagerTCPport.TabIndex = 14;
            this.ProfileManagerTCPport.Leave += new System.EventHandler(this.ProfileManagerTCPport_Leave);
            // 
            // ProfileConnectionName
            // 
            this.ProfileConnectionName.Font = new System.Drawing.Font("Arial", 8F);
            this.ProfileConnectionName.Location = new System.Drawing.Point(222, 141);
            this.ProfileConnectionName.Margin = new System.Windows.Forms.Padding(2);
            this.ProfileConnectionName.Name = "ProfileConnectionName";
            this.ProfileConnectionName.Size = new System.Drawing.Size(286, 20);
            this.ProfileConnectionName.TabIndex = 0;
            this.ProfileConnectionName.TextChanged += new System.EventHandler(this.ProfileConnectionName_TextChanged);
            this.ProfileConnectionName.Leave += new System.EventHandler(this.ProfileConnectionName_Leave);
            // 
            // ProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(640, 425);
            this.Controls.Add(this.ProfileManagerTCPport);
            this.Controls.Add(this.ProfileConnectionName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.becsysLinkBtn);
            this.Controls.Add(this.MinimizePictureBox);
            this.Controls.Add(this.ClosePictureBox);
            this.Controls.Add(this.CancelButtonPictureBox);
            this.Controls.Add(this.ApplyButtonPictureBox);
            this.Controls.Add(this.ProfileFrequency);
            this.Controls.Add(this.ProfileManagerVPNAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ProfileURL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProfileEstablishAfterRequest);
            this.Controls.Add(this.ProfileManagerAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProfileForm";
            this.ShowIcon = false;
            this.Text = "Profile";
            this.Load += new System.EventHandler(this.ProfileForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProfileFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplyButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.becsysLinkBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ProfileManagerVPNAddress;
        private System.Windows.Forms.TextBox ProfileManagerAddress;
        private System.Windows.Forms.CheckBox ProfileEstablishAfterRequest;
        private System.Windows.Forms.NumericUpDown ProfileFrequency;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ProfileURL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox ApplyButtonPictureBox;
        private System.Windows.Forms.PictureBox CancelButtonPictureBox;
        private System.Windows.Forms.PictureBox ClosePictureBox;
        private System.Windows.Forms.PictureBox MinimizePictureBox;
        private System.Windows.Forms.PictureBox becsysLinkBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox ProfileManagerTCPport;
        private System.Windows.Forms.TextBox ProfileConnectionName;
    }
}