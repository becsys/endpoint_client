﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Configuration;
using Microsoft.Win32;
using System.Diagnostics;
using cms_transport;
using cms_client_tray.Properties;
using cms_client_tray.utils;

namespace cms_client_tray
{
    public partial class SettingsForm : Form, IProtocolListener
    {

        public static string EditProfile = "";

        public void UpdateDataGridView()
        {
            //Clear DataGridView
            this.ProfileGridView.Rows.Clear();

            int z = 0;
            foreach (Connection connection in CMSSettings.connections.Values)
            {
                this.ProfileGridView.Rows.Add();
                this.ProfileGridView.Rows[z].Cells[0].Value = connection.name;
                this.ProfileGridView.Rows[z].Cells[1].Value = connection.managerAddress;
                this.ProfileGridView.Rows[z].Cells[2].Value = connection.managerPort;
                // TODO: implement logic
                this.ProfileGridView.Rows[z].Cells[3].Value =
                    connection.requestEnabled ? Resources.labelByRequest : Resources.labelImmediate;
                z++;
            }
        }

        public SettingsForm()
        {
            InitializeComponent();
            updateDisplayQueryAccess();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ClientProtocol.register(this);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            ClientProtocol.unregister(this);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            checkGlobalSettings();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            //place form on bottom-right
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);


            //fill connection parameters from config file
            ConnectionManagerAddress.Text = AppUtils.getCurrAddressStr();
            ConnectionManagerTCPport.Text = AppUtils.getCurrTCPPortStr();
            ConnectionManagerVPNAddress.Text = AppUtils.getCurrVPNAddressStr();

            //Fill ProfileDataGridView
            UpdateDataGridView();

            this.IdentityDataGridView.Rows.Add();	// добавление строки 

            //Fill IdentityDataGridView
            this.IdentityDataGridView.Rows[0].Cells[0].Value = "Default";
            this.IdentityDataGridView.Rows[0].Cells[1].Value = "Local File";
            this.IdentityDataGridView.Rows[0].Cells[2].Value = "CMS";

            loadGlobalSettings();

            //load log file into LogRichTextBox
            try
            {
                FileStream LogFile = new FileStream(CMSSettings.vpnServiceLogFile, 
                    FileMode.Open, 
                    FileAccess.Read, 
                    FileShare.ReadWrite);
                LogRichTextBox.LoadFile(LogFile, RichTextBoxStreamType.PlainText);
                LogFile.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                //do something else
            }
        }

        private void updateLockdown()
        {
            //Lockdown or connected
            ClientInfoLockdown.Text = CMSSettings.isLockdown ? "Enabled" : "Disabled";

            bool enableInterface = !CMSSettings.isLockdown;
            ChkBoxDisplayQueryWindow.Enabled = enableInterface;
            ChkBoxQueryIfNoLogon.Enabled = enableInterface;
            SpinEditQueryTimeout.Enabled = enableInterface;
            TxtBoxQueryMessage.Enabled = enableInterface;
            ChkBoxEnableFileTransfer.Enabled = enableInterface;
            ChkBoxAllowMultipleConnections.Enabled = enableInterface;
            ChkBoxLockOnDisconnect.Enabled = enableInterface;
            ChkBoxStartEndpointClientWithWindows.Enabled = enableInterface;
            ProfileAddPictureBox.Enabled = enableInterface;
            ProfileEditPictureBox.Enabled = enableInterface;
            ProfileRemovePictureBox.Enabled = enableInterface;
            IdentityAddPictureBox.Enabled = enableInterface;
            IdentityEditPictureBox.Enabled = enableInterface;
            IdentityRemovePictureBox.Enabled = enableInterface;
            LogClearButtonPictureBox.Enabled = enableInterface;
        }

        public void onStatusReceived(CMSStatusResponse status)
        {
            if (status.result != "OK")
            {
                MessageBox.Show("Error: Can't execute command", "BE CMS Endpoint Client");
                System.Environment.Exit(1);
            }
            ConnectionState.Text = status.connectionStatus;

            Network network = status.network;
            if (network != null) { 
                ClientInfoSystemName.Text = network.systemName;
                ClientInfoCustomName.Text = network.customName;
                ClientInfoOSversion.Text = network.osVersion;
                ClientInfoSerialNumber.Text = network.serialNumber;
                ConnectionBytesReceived.Text = network.receivedBytes;
                ConnectionBytesSent.Text = network.sendtBytes;
                ConnectionClientVPNAddress.Text = network.ip;
            }
            Vpn vpn = status.vpn;
            if (vpn != null)
            {
                ConnectionDuration.Text = vpn.lifetime;
            }
        }

        private void loadGlobalSettings()
        {
            ChkBoxDisplayQueryWindow.Checked = CMSSettings.isDisplayQueryWindow;
            ChkBoxQueryIfNoLogon.Checked = CMSSettings.isQueryIfNoLogon;
            SpinEditQueryTimeout.Value = CMSSettings.queryTimeout;
            TxtBoxQueryMessage.Text = CMSSettings.queryMessage;
            ChkBoxEnableFileTransfer.Checked = CMSSettings.isEnableFileTransfer;
            ChkBoxAllowMultipleConnections.Checked = CMSSettings.isAllowMultipleConnections;
            ChkBoxLockOnDisconnect.Checked = CMSSettings.isLockOnDisconnect;
            ChkBoxStartEndpointClientWithWindows.Checked = CMSSettings.isAutoStart;
            spinBoxConnectionTimeout.Value = CMSSettings.connectionTimeout;

            updateLockdown();
        }

        private void LogClearButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to clear log?", 
                Resources.ConnectionManager, 
                MessageBoxButtons.YesNo);

           if (dialogResult == DialogResult.Yes)
           {
               LogRichTextBox.Clear();
               try
               {
                   System.IO.File.WriteAllText(CMSSettings.vpnServiceLogFile, string.Empty);
               }
               catch (Exception ex)
               {
                   Debug.WriteLine(ex.Message);
                   //do something else
               }
           }
        }
                
        private void ClosePictureBox_MouseHover(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Settings_close_hover;
        }

        private void ClosePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Settings_close_leave;
        }

        private void MinimizePictureBox_MouseHover(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Settings_minimize_hover;
        }

        private void MinimizePictureBox_MouseLeave(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Settings_minimize_leave;
        }

        private void MinimizePictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClosePictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.closeAllForms();
        }

        private void LinkPictureBox_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.becsys.ru/cms"); 
        }

        private void ProfileAddPictureBox_Click(object sender, EventArgs e)
        {
            ProfileForm profile_frm = new ProfileForm();
            profile_frm.Owner = this; //Передаём вновь созданной форме её владельца.
            profile_frm.ShowDialog();
        }

        private void ProfileEditPictureBox_Click(object sender, EventArgs e)
        {
            if (ProfileGridView.Rows[ProfileGridView.CurrentRow.Index].Cells[0].Value != null)
            {
                EditProfile = ProfileGridView.Rows[ProfileGridView.CurrentRow.Index].Cells[0].Value.ToString();
                ProfileForm profile_frm = new ProfileForm();
                profile_frm.Owner = this; //Передаём вновь созданной форме её владельца.
                profile_frm.ShowDialog();
            }
        }

        private void ProfileRemovePictureBox_Click(object sender, EventArgs e)
        {
            if (ProfileGridView.Rows[ProfileGridView.CurrentRow.Index].Cells[0].Value != null)
            {
                string SelectedProfile = ProfileGridView.Rows[ProfileGridView.CurrentRow.Index].Cells[0].Value.ToString();

                DialogResult dialogResult = MessageBox.Show("Are you sure to delete this connection profile?",
                    Resources.ConnectionManager, 
                    MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    CMSSettings.removeConnection(SelectedProfile);
                    UpdateDataGridView();
                }
            }
        }

        private void ProfileAddPictureBox_MouseHover(object sender, EventArgs e)
        {
            ProfileAddPictureBox.BackgroundImage = Resources.Profile_button_add_hover;
        }

        private void ProfileAddPictureBox_MouseLeave(object sender, EventArgs e)
        {
            ProfileAddPictureBox.BackgroundImage = Resources.Profile_button_add_leave;
        }

        private void ProfileEditPictureBox_MouseHover(object sender, EventArgs e)
        {
            ProfileEditPictureBox.BackgroundImage = Resources.Profile_button_edit_hover;
        }

        private void ProfileEditPictureBox_MouseLeave(object sender, EventArgs e)
        {
            ProfileEditPictureBox.BackgroundImage = Resources.Profile_button_edit_leave;
        }

        private void ProfileRemovePictureBox_MouseHover(object sender, EventArgs e)
        {
            ProfileRemovePictureBox.BackgroundImage = Resources.Profile_button_remove_hover;
        }

        private void ProfileRemovePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ProfileRemovePictureBox.BackgroundImage = Resources.Profile_button_remove_leave;
        }

        private void IdentityAddPictureBox_Click(object sender, EventArgs e)
        {
            IdentityForm identity_frm = new IdentityForm();
            identity_frm.Owner = this; //Передаём вновь созданной форме её владельца.
            identity_frm.ShowDialog();
        }

        private void LogClearButtonPictureBox_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to clear log?",
                Resources.ConnectionManager, 
                MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                LogRichTextBox.Clear();
                try
                {
                    System.IO.File.WriteAllText(CMSSettings.vpnServiceLogFile, string.Empty);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    //do something else
                }
            }
        }

        private void LogClearButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            LogClearButtonPictureBox.BackgroundImage = Resources.Log_button_clear_hover;
        }

        private void LogClearButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            LogClearButtonPictureBox.BackgroundImage = Resources.Log_button_clear_leave;
        }

        private void IdentityAddPictureBox_MouseHover(object sender, EventArgs e)
        {
            IdentityAddPictureBox.BackgroundImage = Resources.Identity_button_add_hover;
        }

        private void IdentityAddPictureBox_MouseLeave(object sender, EventArgs e)
        {
            IdentityAddPictureBox.BackgroundImage = Resources.Identity_button_add_leave;
        }

        private void IdentityEditPictureBox_MouseHover(object sender, EventArgs e)
        {
            IdentityEditPictureBox.BackgroundImage = Resources.Identity_button_edit_hover;
        }

        private void IdentityEditPictureBox_MouseLeave(object sender, EventArgs e)
        {
            IdentityEditPictureBox.BackgroundImage = Resources.Identity_button_edit_leave;
        }

        private void IdentityRemovePictureBox_MouseHover(object sender, EventArgs e)
        {
            IdentityRemovePictureBox.BackgroundImage = Resources.Identity_button_remove_hover;
        }

        private void IdentityRemovePictureBox_MouseLeave(object sender, EventArgs e)
        {
            IdentityRemovePictureBox.BackgroundImage = Resources.Identity_button_remove_leave;
        }

        private void TreePictureBox_Click(object sender, EventArgs e)
        {
            Process.Start(Resources.LinkBecSys); 
        }

        private void InfoPictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.showAbout(this);
        }

        private void InfoPictureBox_MouseHover(object sender, EventArgs e)
        {
            InfoPictureBox.BackgroundImage = Resources.Connect_info_hover;
        }

        private void InfoPictureBox_MouseLeave(object sender, EventArgs e)
        {
            InfoPictureBox.BackgroundImage = Resources.Connect_info_leave;
        }

        private void LinkPictureBox_MouseLeave(object sender, EventArgs e)
        {
            LinkPictureBox.BackgroundImage = Resources.Link_be_leave;
        }

        private void LinkPictureBox_MouseHover(object sender, EventArgs e)
        {
            LinkPictureBox.BackgroundImage = Resources.Link_be_hover;
        }

        private void chkBoxDisplayQueryWindow_CheckedChanged(object sender, EventArgs e)
        {
            updateDisplayQueryAccess();            
        }

        private void updateDisplayQueryAccess()
        {
            bool isEnabled = ChkBoxDisplayQueryWindow.Checked;
            ChkBoxQueryIfNoLogon.Enabled = isEnabled;
            SpinEditQueryTimeout.Enabled = isEnabled;
            TxtBoxQueryMessage.Enabled = isEnabled;
            LblQueryTimeout.Enabled = isEnabled;
            LblQueryMessage.Enabled = isEnabled;
        }

        private void saveGlobalSettings()
        {
            CMSSettings.isDisplayQueryWindow = ChkBoxDisplayQueryWindow.Checked;
            CMSSettings.isQueryIfNoLogon = ChkBoxQueryIfNoLogon.Checked;
            CMSSettings.queryTimeout = (int)SpinEditQueryTimeout.Value;
            CMSSettings.queryMessage = TxtBoxQueryMessage.Text;
            CMSSettings.isEnableFileTransfer = ChkBoxEnableFileTransfer.Checked;
            CMSSettings.isAllowMultipleConnections = ChkBoxAllowMultipleConnections.Checked;
            CMSSettings.isLockOnDisconnect = ChkBoxLockOnDisconnect.Checked;
            CMSSettings.isAutoStart = ChkBoxStartEndpointClientWithWindows.Checked;
            CMSSettings.connectionTimeout = (int)spinBoxConnectionTimeout.Value;

            CMSSettings.saveGlobal();            
        }

        private void SettingsTabControl_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            checkGlobalSettings();
        }

        private void checkGlobalSettings()
        {
            if (SettingsTabControl.SelectedTab == Global && isGlobalSettingsChanged())
            {
                if (MessageBox.Show(Resources.ConfirmSettingsDialog,
                    Resources.GlobalSettings,
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    saveGlobalSettings();
                }
                else
                {
                    loadGlobalSettings();
                }
            }
        }

        private bool isGlobalSettingsChanged()
        {
            return CMSSettings.isDisplayQueryWindow != ChkBoxDisplayQueryWindow.Checked ||
                CMSSettings.isQueryIfNoLogon != ChkBoxQueryIfNoLogon.Checked ||
                CMSSettings.queryTimeout != (int)SpinEditQueryTimeout.Value ||
                CMSSettings.queryMessage != TxtBoxQueryMessage.Text ||
                CMSSettings.isEnableFileTransfer != ChkBoxEnableFileTransfer.Checked ||
                CMSSettings.isAllowMultipleConnections != ChkBoxAllowMultipleConnections.Checked ||
                CMSSettings.isLockOnDisconnect != ChkBoxLockOnDisconnect.Checked ||
                CMSSettings.isAutoStart != ChkBoxStartEndpointClientWithWindows.Checked ||
                CMSSettings.connectionTimeout != (int)spinBoxConnectionTimeout.Value;
        }

        public Control getControl()
        {
            return this;
        }

        private void ChkBoxStartEndpointClientWithWindows_CheckedChanged(object sender, EventArgs e)
        {
            bool connectionTimeoutActive = !ChkBoxStartEndpointClientWithWindows.Checked;
            lblConnectionTimeout.Enabled = connectionTimeoutActive;
            spinBoxConnectionTimeout.Enabled = connectionTimeoutActive;
        }
    }
}