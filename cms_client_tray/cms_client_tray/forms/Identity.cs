﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Diagnostics;
using cms_client_tray.Properties;
using cms_client_tray.utils;

namespace cms_client_tray
{
    public partial class IdentityForm : Form
    {
        public IdentityForm()
        {
            InitializeComponent();
        }

        //////////////////////////////////////////////
        //Validations
        //

        private bool TrustPointNameValidation()
        {
            return Regex.IsMatch(TPNameTextBox.Text,
                    RegExpValidationPatterns.NameValidationPattern,
                    RegexOptions.IgnoreCase);
        }

        private void IdentityForm_Load(object sender, EventArgs e)
        {
            //place form on bottom-right
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);

            //
            LocalRadioButton.Checked = true;
            updateLocalRadioButton();
        }    

        private void RadioButtons_Click(object sender, EventArgs e)
        {
            updateLocalRadioButton();
        }

        private void updateLocalRadioButton()
        {
            bool localRadioBtnChecked = LocalRadioButton.Checked;
            UrlTextBox.Enabled = !localRadioBtnChecked;
            PassphraseTextBox.Enabled = !localRadioBtnChecked;
            CertBrowseButtonPictureBox.Visible = localRadioBtnChecked;
            CertBrowseButtonPictureBox.Enabled = localRadioBtnChecked;
            KeyBrowseButtonPictureBox.Visible = localRadioBtnChecked;
            KeyBrowseButtonPictureBox.Enabled = localRadioBtnChecked;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            //close Identity form
            this.Close();
        }

        private void TPNameTextBox_Leave(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(TPNameTextBox.Text, 
                    RegExpValidationPatterns.NameValidationPattern, 
                    RegexOptions.IgnoreCase))    
            {
                // NOT a valid profile name...
                if (String.IsNullOrEmpty(TPNameTextBox.Text))
                {
                    this.ActiveControl = TPNameTextBox;
                }
            }
        }

        private void CABrowseButtonPictureBox_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a CA file
            CAOpenFileDialog.Filter = Resources.CAFileFilter;
            CAOpenFileDialog.Title = Resources.SelectCAFileTitle;
            CAOpenFileDialog.ShowDialog();

            if (!String.IsNullOrEmpty(CAOpenFileDialog.FileName))
            {
                IssueCAState.Text = Resources.Selected;
            }
        }

        private void CertBrowseButtonPictureBox_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a CA file
            CERTOpenFileDialog.Filter = Resources.ClientCertFileFilter;
            CERTOpenFileDialog.Title = Resources.CertOpenFileDialogTitle;
            CERTOpenFileDialog.ShowDialog();

            if (!String.IsNullOrEmpty(CERTOpenFileDialog.FileName))
            {
                ClientCERTState.Text = Resources.Selected;
            }
        }

        private void KeyBrowseButtonPictureBox_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a CA file
            KEYOpenFileDialog.Filter = Resources.CAFileFilter;
            KEYOpenFileDialog.Title = Resources.CAOpenKeyFileDialogTitle;
            KEYOpenFileDialog.ShowDialog();
            if (!String.IsNullOrEmpty(KEYOpenFileDialog.FileName))
            {
                ClientKEYState.Text = Resources.Selected;
            }
        }

        private void ApplyButtonPictureBox_Click(object sender, EventArgs e)
        {
            if (TrustPointNameValidation() && LocalRadioButton.Checked)
            {
                if (MessageBox.Show("Are you sure to add new trust point?", 
                    Resources.ConnectionManager, 
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (String.IsNullOrEmpty(CAOpenFileDialog.FileName) &&
                        String.IsNullOrEmpty(CERTOpenFileDialog.FileName) &&
                        String.IsNullOrEmpty(KEYOpenFileDialog.FileName))
                    {
                        //import CA, CERT and KEY into local storage
                        System.IO.File.Copy(CAOpenFileDialog.FileName, 
                            Path.Combine("C:\\Program Files\\OpenVPN\\config\\", 
                            TPNameTextBox.Text + "_CA" + ".crt"), true);
                        System.IO.File.Copy(CERTOpenFileDialog.FileName, 
                            Path.Combine("C:\\Program Files\\OpenVPN\\config\\", 
                            TPNameTextBox.Text + "_CERT" + ".crt"), true);
                        System.IO.File.Copy(KEYOpenFileDialog.FileName, 
                            Path.Combine("C:\\Program Files\\OpenVPN\\config\\", 
                            TPNameTextBox.Text + "_KEY" + ".key"), true);

                        //close Identity form
                        this.Close();
                    }
                }
                
            }
        }

        private void ApplyButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            ApplyButtonPictureBox.BackgroundImage = Resources.Identity_button_apply_hover;
        }

        private void ApplyButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            ApplyButtonPictureBox.BackgroundImage = Resources.Identity_button_apply_leave;
        }

        private void CancelButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            CancelButtonPictureBox.BackgroundImage = Resources.Identity_button_cancel_hover;
        }

        private void CancelButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CancelButtonPictureBox.BackgroundImage = Resources.Identity_button_cancel_leave;
        }

        private void CABrowseButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            CABrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_hover;
        }

        private void CABrowseButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CABrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_leave;
        }

        private void CertBrowseButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            CertBrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_hover;
        }

        private void CertBrowseButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CertBrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_leave;
        }

        private void MinimizePictureBox_MouseHover(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Identity_minimize_hover;
        }

        private void MinimizePictureBox_MouseLeave(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Identity_minimize_leave;
        }

        private void ClosePictureBox_MouseHover(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Identity_close_hover;
        }

        private void ClosePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Identity_close_leave;
        }

        private void CancelButtonPictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizePictureBox_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClosePictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.closeAllForms();
        }

        private void KeyBrowseButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            KeyBrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_hover;
        }

        private void KeyBrowseButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            KeyBrowseButtonPictureBox.BackgroundImage = Resources.Identity_button_browse_leave;
        }

        private void BecSysLinkBtn_Click(object sender, EventArgs e)
        {
            Process.Start(Resources.LinkBecSys);
        }
    }
}
