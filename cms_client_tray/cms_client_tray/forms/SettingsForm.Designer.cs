﻿namespace cms_client_tray
{
    partial class SettingsForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SettingsTabControl = new System.Windows.Forms.TabControl();
            this.Status = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ClientInfoLockdown = new System.Windows.Forms.Label();
            this.ConnectionManagerTCPport = new System.Windows.Forms.Label();
            this.Lockdown = new System.Windows.Forms.Label();
            this.ClientInfoCustomName = new System.Windows.Forms.Label();
            this.VPNTCPport = new System.Windows.Forms.Label();
            this.LogicalName = new System.Windows.Forms.Label();
            this.ConnectionBytesReceived = new System.Windows.Forms.Label();
            this.ClientInfoSystemName = new System.Windows.Forms.Label();
            this.ConnectionClientVPNAddress = new System.Windows.Forms.Label();
            this.SystemName = new System.Windows.Forms.Label();
            this.BytesReceived = new System.Windows.Forms.Label();
            this.ClientInfoOSversion = new System.Windows.Forms.Label();
            this.ClientVPNAddress = new System.Windows.Forms.Label();
            this.OSversion = new System.Windows.Forms.Label();
            this.ConnectionBytesSent = new System.Windows.Forms.Label();
            this.ClientInfoSerialNumber = new System.Windows.Forms.Label();
            this.ConnectionManagerVPNAddress = new System.Windows.Forms.Label();
            this.SerialNumber = new System.Windows.Forms.Label();
            this.BytesSent = new System.Windows.Forms.Label();
            this.ConnectionManagerAddress = new System.Windows.Forms.Label();
            this.ConnectionDuration = new System.Windows.Forms.Label();
            this.ManagerVPNAdddress = new System.Windows.Forms.Label();
            this.Duration = new System.Windows.Forms.Label();
            this.StatusManagerAddress = new System.Windows.Forms.Label();
            this.ConnectionState = new System.Windows.Forms.Label();
            this.State = new System.Windows.Forms.Label();
            this.Global = new System.Windows.Forms.TabPage();
            this.TxtBoxQueryMessage = new System.Windows.Forms.RichTextBox();
            this.SpinEditQueryTimeout = new System.Windows.Forms.NumericUpDown();
            this.LblQueryTimeout = new System.Windows.Forms.Label();
            this.LblQueryMessage = new System.Windows.Forms.Label();
            this.ChkBoxStartEndpointClientWithWindows = new System.Windows.Forms.CheckBox();
            this.ChkBoxLockOnDisconnect = new System.Windows.Forms.CheckBox();
            this.ChkBoxAllowMultipleConnections = new System.Windows.Forms.CheckBox();
            this.ChkBoxEnableFileTransfer = new System.Windows.Forms.CheckBox();
            this.ChkBoxQueryIfNoLogon = new System.Windows.Forms.CheckBox();
            this.ChkBoxDisplayQueryWindow = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Profiles = new System.Windows.Forms.TabPage();
            this.ProfileRemovePictureBox = new System.Windows.Forms.PictureBox();
            this.ProfileEditPictureBox = new System.Windows.Forms.PictureBox();
            this.ProfileAddPictureBox = new System.Windows.Forms.PictureBox();
            this.ProfileGridView = new System.Windows.Forms.DataGridView();
            this.ConnectionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManagerAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManagerTCPPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.connectionMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Identity = new System.Windows.Forms.TabPage();
            this.IdentityRemovePictureBox = new System.Windows.Forms.PictureBox();
            this.IdentityEditPictureBox = new System.Windows.Forms.PictureBox();
            this.IdentityAddPictureBox = new System.Windows.Forms.PictureBox();
            this.IdentityDataGridView = new System.Windows.Forms.DataGridView();
            this.TrustPointName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KeyManagement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IssueCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Log = new System.Windows.Forms.TabPage();
            this.LogClearButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.LogRichTextBox = new System.Windows.Forms.RichTextBox();
            this.MinimizePictureBox = new System.Windows.Forms.PictureBox();
            this.ClosePictureBox = new System.Windows.Forms.PictureBox();
            this.LinkPictureBox = new System.Windows.Forms.PictureBox();
            this.InfoPictureBox = new System.Windows.Forms.PictureBox();
            this.TreePictureBox = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.spinBoxConnectionTimeout = new System.Windows.Forms.NumericUpDown();
            this.lblConnectionTimeout = new System.Windows.Forms.Label();
            this.SettingsTabControl.SuspendLayout();
            this.Status.SuspendLayout();
            this.Global.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditQueryTimeout)).BeginInit();
            this.Profiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileRemovePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileEditPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileAddPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileGridView)).BeginInit();
            this.Identity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityRemovePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityEditPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityAddPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityDataGridView)).BeginInit();
            this.Log.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogClearButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinBoxConnectionTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // SettingsTabControl
            // 
            this.SettingsTabControl.Controls.Add(this.Status);
            this.SettingsTabControl.Controls.Add(this.Global);
            this.SettingsTabControl.Controls.Add(this.Profiles);
            this.SettingsTabControl.Controls.Add(this.Identity);
            this.SettingsTabControl.Controls.Add(this.Log);
            this.SettingsTabControl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SettingsTabControl.Location = new System.Drawing.Point(40, 107);
            this.SettingsTabControl.Name = "SettingsTabControl";
            this.SettingsTabControl.SelectedIndex = 0;
            this.SettingsTabControl.Size = new System.Drawing.Size(565, 273);
            this.SettingsTabControl.TabIndex = 0;
            this.SettingsTabControl.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.SettingsTabControl_Deselecting);
            // 
            // Status
            // 
            this.Status.BackColor = System.Drawing.Color.White;
            this.Status.Controls.Add(this.label2);
            this.Status.Controls.Add(this.label1);
            this.Status.Controls.Add(this.ClientInfoLockdown);
            this.Status.Controls.Add(this.ConnectionManagerTCPport);
            this.Status.Controls.Add(this.Lockdown);
            this.Status.Controls.Add(this.ClientInfoCustomName);
            this.Status.Controls.Add(this.VPNTCPport);
            this.Status.Controls.Add(this.LogicalName);
            this.Status.Controls.Add(this.ConnectionBytesReceived);
            this.Status.Controls.Add(this.ClientInfoSystemName);
            this.Status.Controls.Add(this.ConnectionClientVPNAddress);
            this.Status.Controls.Add(this.SystemName);
            this.Status.Controls.Add(this.BytesReceived);
            this.Status.Controls.Add(this.ClientInfoOSversion);
            this.Status.Controls.Add(this.ClientVPNAddress);
            this.Status.Controls.Add(this.OSversion);
            this.Status.Controls.Add(this.ConnectionBytesSent);
            this.Status.Controls.Add(this.ClientInfoSerialNumber);
            this.Status.Controls.Add(this.ConnectionManagerVPNAddress);
            this.Status.Controls.Add(this.SerialNumber);
            this.Status.Controls.Add(this.BytesSent);
            this.Status.Controls.Add(this.ConnectionManagerAddress);
            this.Status.Controls.Add(this.ConnectionDuration);
            this.Status.Controls.Add(this.ManagerVPNAdddress);
            this.Status.Controls.Add(this.Duration);
            this.Status.Controls.Add(this.StatusManagerAddress);
            this.Status.Controls.Add(this.ConnectionState);
            this.Status.Controls.Add(this.State);
            this.Status.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Status.Location = new System.Drawing.Point(4, 24);
            this.Status.Name = "Status";
            this.Status.Padding = new System.Windows.Forms.Padding(3);
            this.Status.Size = new System.Drawing.Size(557, 245);
            this.Status.TabIndex = 0;
            this.Status.Text = "Status";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(22, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "Client Information";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(22, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Connection Status";
            // 
            // ClientInfoLockdown
            // 
            this.ClientInfoLockdown.AutoSize = true;
            this.ClientInfoLockdown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientInfoLockdown.Location = new System.Drawing.Point(155, 177);
            this.ClientInfoLockdown.Name = "ClientInfoLockdown";
            this.ClientInfoLockdown.Size = new System.Drawing.Size(26, 15);
            this.ClientInfoLockdown.TabIndex = 9;
            this.ClientInfoLockdown.Text = "N/A";
            // 
            // ConnectionManagerTCPport
            // 
            this.ConnectionManagerTCPport.AutoSize = true;
            this.ConnectionManagerTCPport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionManagerTCPport.Location = new System.Drawing.Point(418, 65);
            this.ConnectionManagerTCPport.Name = "ConnectionManagerTCPport";
            this.ConnectionManagerTCPport.Size = new System.Drawing.Size(26, 15);
            this.ConnectionManagerTCPport.TabIndex = 7;
            this.ConnectionManagerTCPport.Text = "N/A";
            // 
            // Lockdown
            // 
            this.Lockdown.AutoSize = true;
            this.Lockdown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Lockdown.Location = new System.Drawing.Point(44, 177);
            this.Lockdown.Name = "Lockdown";
            this.Lockdown.Size = new System.Drawing.Size(66, 15);
            this.Lockdown.TabIndex = 8;
            this.Lockdown.Text = "Lockdown:";
            // 
            // ClientInfoCustomName
            // 
            this.ClientInfoCustomName.AutoSize = true;
            this.ClientInfoCustomName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientInfoCustomName.Location = new System.Drawing.Point(418, 177);
            this.ClientInfoCustomName.Name = "ClientInfoCustomName";
            this.ClientInfoCustomName.Size = new System.Drawing.Size(26, 15);
            this.ClientInfoCustomName.TabIndex = 7;
            this.ClientInfoCustomName.Text = "N/A";
            // 
            // VPNTCPport
            // 
            this.VPNTCPport.AutoSize = true;
            this.VPNTCPport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VPNTCPport.Location = new System.Drawing.Point(263, 65);
            this.VPNTCPport.Name = "VPNTCPport";
            this.VPNTCPport.Size = new System.Drawing.Size(109, 15);
            this.VPNTCPport.TabIndex = 6;
            this.VPNTCPport.Text = "Manager TCP port:";
            // 
            // LogicalName
            // 
            this.LogicalName.AutoSize = true;
            this.LogicalName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogicalName.Location = new System.Drawing.Point(263, 177);
            this.LogicalName.Name = "LogicalName";
            this.LogicalName.Size = new System.Drawing.Size(87, 15);
            this.LogicalName.TabIndex = 6;
            this.LogicalName.Text = "Logical Name:";
            // 
            // ConnectionBytesReceived
            // 
            this.ConnectionBytesReceived.AutoSize = true;
            this.ConnectionBytesReceived.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionBytesReceived.Location = new System.Drawing.Point(155, 103);
            this.ConnectionBytesReceived.Name = "ConnectionBytesReceived";
            this.ConnectionBytesReceived.Size = new System.Drawing.Size(26, 15);
            this.ConnectionBytesReceived.TabIndex = 9;
            this.ConnectionBytesReceived.Text = "N/A";
            // 
            // ClientInfoSystemName
            // 
            this.ClientInfoSystemName.AutoSize = true;
            this.ClientInfoSystemName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientInfoSystemName.Location = new System.Drawing.Point(418, 158);
            this.ClientInfoSystemName.Name = "ClientInfoSystemName";
            this.ClientInfoSystemName.Size = new System.Drawing.Size(26, 15);
            this.ClientInfoSystemName.TabIndex = 5;
            this.ClientInfoSystemName.Text = "N/A";
            // 
            // ConnectionClientVPNAddress
            // 
            this.ConnectionClientVPNAddress.AutoSize = true;
            this.ConnectionClientVPNAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionClientVPNAddress.Location = new System.Drawing.Point(418, 103);
            this.ConnectionClientVPNAddress.Name = "ConnectionClientVPNAddress";
            this.ConnectionClientVPNAddress.Size = new System.Drawing.Size(26, 15);
            this.ConnectionClientVPNAddress.TabIndex = 5;
            this.ConnectionClientVPNAddress.Text = "N/A";
            // 
            // SystemName
            // 
            this.SystemName.AutoSize = true;
            this.SystemName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SystemName.Location = new System.Drawing.Point(263, 158);
            this.SystemName.Name = "SystemName";
            this.SystemName.Size = new System.Drawing.Size(88, 15);
            this.SystemName.TabIndex = 4;
            this.SystemName.Text = "System Name:";
            // 
            // BytesReceived
            // 
            this.BytesReceived.AutoSize = true;
            this.BytesReceived.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BytesReceived.Location = new System.Drawing.Point(44, 103);
            this.BytesReceived.Name = "BytesReceived";
            this.BytesReceived.Size = new System.Drawing.Size(89, 15);
            this.BytesReceived.TabIndex = 8;
            this.BytesReceived.Text = "Bytes received:";
            // 
            // ClientInfoOSversion
            // 
            this.ClientInfoOSversion.AutoSize = true;
            this.ClientInfoOSversion.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientInfoOSversion.Location = new System.Drawing.Point(155, 196);
            this.ClientInfoOSversion.Name = "ClientInfoOSversion";
            this.ClientInfoOSversion.Size = new System.Drawing.Size(26, 15);
            this.ClientInfoOSversion.TabIndex = 3;
            this.ClientInfoOSversion.Text = "N/A";
            // 
            // ClientVPNAddress
            // 
            this.ClientVPNAddress.AutoSize = true;
            this.ClientVPNAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientVPNAddress.Location = new System.Drawing.Point(263, 103);
            this.ClientVPNAddress.Name = "ClientVPNAddress";
            this.ClientVPNAddress.Size = new System.Drawing.Size(117, 15);
            this.ClientVPNAddress.TabIndex = 4;
            this.ClientVPNAddress.Text = "Client VPN Address:";
            // 
            // OSversion
            // 
            this.OSversion.AutoSize = true;
            this.OSversion.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OSversion.Location = new System.Drawing.Point(44, 196);
            this.OSversion.Name = "OSversion";
            this.OSversion.Size = new System.Drawing.Size(70, 15);
            this.OSversion.TabIndex = 2;
            this.OSversion.Text = "OS version:";
            // 
            // ConnectionBytesSent
            // 
            this.ConnectionBytesSent.AutoSize = true;
            this.ConnectionBytesSent.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionBytesSent.Location = new System.Drawing.Point(155, 84);
            this.ConnectionBytesSent.Name = "ConnectionBytesSent";
            this.ConnectionBytesSent.Size = new System.Drawing.Size(26, 15);
            this.ConnectionBytesSent.TabIndex = 7;
            this.ConnectionBytesSent.Text = "N/A";
            // 
            // ClientInfoSerialNumber
            // 
            this.ClientInfoSerialNumber.AutoSize = true;
            this.ClientInfoSerialNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ClientInfoSerialNumber.Location = new System.Drawing.Point(155, 158);
            this.ClientInfoSerialNumber.Name = "ClientInfoSerialNumber";
            this.ClientInfoSerialNumber.Size = new System.Drawing.Size(26, 15);
            this.ClientInfoSerialNumber.TabIndex = 1;
            this.ClientInfoSerialNumber.Text = "N/A";
            // 
            // ConnectionManagerVPNAddress
            // 
            this.ConnectionManagerVPNAddress.AutoSize = true;
            this.ConnectionManagerVPNAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionManagerVPNAddress.Location = new System.Drawing.Point(418, 84);
            this.ConnectionManagerVPNAddress.Name = "ConnectionManagerVPNAddress";
            this.ConnectionManagerVPNAddress.Size = new System.Drawing.Size(26, 15);
            this.ConnectionManagerVPNAddress.TabIndex = 3;
            this.ConnectionManagerVPNAddress.Text = "N/A";
            // 
            // SerialNumber
            // 
            this.SerialNumber.AutoSize = true;
            this.SerialNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SerialNumber.Location = new System.Drawing.Point(44, 158);
            this.SerialNumber.Name = "SerialNumber";
            this.SerialNumber.Size = new System.Drawing.Size(90, 15);
            this.SerialNumber.TabIndex = 0;
            this.SerialNumber.Text = "Serial Number:";
            // 
            // BytesSent
            // 
            this.BytesSent.AutoSize = true;
            this.BytesSent.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BytesSent.Location = new System.Drawing.Point(44, 84);
            this.BytesSent.Name = "BytesSent";
            this.BytesSent.Size = new System.Drawing.Size(67, 15);
            this.BytesSent.TabIndex = 6;
            this.BytesSent.Text = "Bytes sent:";
            // 
            // ConnectionManagerAddress
            // 
            this.ConnectionManagerAddress.AutoSize = true;
            this.ConnectionManagerAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionManagerAddress.Location = new System.Drawing.Point(418, 46);
            this.ConnectionManagerAddress.Name = "ConnectionManagerAddress";
            this.ConnectionManagerAddress.Size = new System.Drawing.Size(26, 15);
            this.ConnectionManagerAddress.TabIndex = 2;
            this.ConnectionManagerAddress.Text = "N/A";
            // 
            // ConnectionDuration
            // 
            this.ConnectionDuration.AutoSize = true;
            this.ConnectionDuration.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionDuration.Location = new System.Drawing.Point(155, 65);
            this.ConnectionDuration.Name = "ConnectionDuration";
            this.ConnectionDuration.Size = new System.Drawing.Size(26, 15);
            this.ConnectionDuration.TabIndex = 5;
            this.ConnectionDuration.Text = "N/A";
            // 
            // ManagerVPNAdddress
            // 
            this.ManagerVPNAdddress.AutoSize = true;
            this.ManagerVPNAdddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ManagerVPNAdddress.Location = new System.Drawing.Point(263, 84);
            this.ManagerVPNAdddress.Name = "ManagerVPNAdddress";
            this.ManagerVPNAdddress.Size = new System.Drawing.Size(133, 15);
            this.ManagerVPNAdddress.TabIndex = 1;
            this.ManagerVPNAdddress.Text = "Manager VPN Address:";
            // 
            // Duration
            // 
            this.Duration.AutoSize = true;
            this.Duration.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Duration.Location = new System.Drawing.Point(44, 65);
            this.Duration.Name = "Duration";
            this.Duration.Size = new System.Drawing.Size(57, 15);
            this.Duration.TabIndex = 4;
            this.Duration.Text = "Duration:";
            // 
            // StatusManagerAddress
            // 
            this.StatusManagerAddress.AutoSize = true;
            this.StatusManagerAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StatusManagerAddress.Location = new System.Drawing.Point(263, 46);
            this.StatusManagerAddress.Name = "StatusManagerAddress";
            this.StatusManagerAddress.Size = new System.Drawing.Size(106, 15);
            this.StatusManagerAddress.TabIndex = 0;
            this.StatusManagerAddress.Text = "Manager Address:";
            // 
            // ConnectionState
            // 
            this.ConnectionState.AutoSize = true;
            this.ConnectionState.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectionState.Location = new System.Drawing.Point(155, 46);
            this.ConnectionState.Name = "ConnectionState";
            this.ConnectionState.Size = new System.Drawing.Size(83, 15);
            this.ConnectionState.TabIndex = 3;
            this.ConnectionState.Text = "Disconnected";
            // 
            // State
            // 
            this.State.AutoSize = true;
            this.State.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.State.Location = new System.Drawing.Point(44, 46);
            this.State.Name = "State";
            this.State.Size = new System.Drawing.Size(38, 15);
            this.State.TabIndex = 2;
            this.State.Text = "State:";
            // 
            // Global
            // 
            this.Global.BackColor = System.Drawing.Color.White;
            this.Global.Controls.Add(this.spinBoxConnectionTimeout);
            this.Global.Controls.Add(this.lblConnectionTimeout);
            this.Global.Controls.Add(this.TxtBoxQueryMessage);
            this.Global.Controls.Add(this.SpinEditQueryTimeout);
            this.Global.Controls.Add(this.LblQueryTimeout);
            this.Global.Controls.Add(this.LblQueryMessage);
            this.Global.Controls.Add(this.ChkBoxStartEndpointClientWithWindows);
            this.Global.Controls.Add(this.ChkBoxLockOnDisconnect);
            this.Global.Controls.Add(this.ChkBoxAllowMultipleConnections);
            this.Global.Controls.Add(this.ChkBoxEnableFileTransfer);
            this.Global.Controls.Add(this.ChkBoxQueryIfNoLogon);
            this.Global.Controls.Add(this.ChkBoxDisplayQueryWindow);
            this.Global.Controls.Add(this.label3);
            this.Global.Controls.Add(this.label6);
            this.Global.Controls.Add(this.label7);
            this.Global.Location = new System.Drawing.Point(4, 24);
            this.Global.Name = "Global";
            this.Global.Size = new System.Drawing.Size(557, 245);
            this.Global.TabIndex = 4;
            this.Global.Text = "Global";
            // 
            // TxtBoxQueryMessage
            // 
            this.TxtBoxQueryMessage.BackColor = System.Drawing.Color.White;
            this.TxtBoxQueryMessage.Font = new System.Drawing.Font("Arial", 9F);
            this.TxtBoxQueryMessage.Location = new System.Drawing.Point(40, 144);
            this.TxtBoxQueryMessage.Name = "TxtBoxQueryMessage";
            this.TxtBoxQueryMessage.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.TxtBoxQueryMessage.Size = new System.Drawing.Size(198, 66);
            this.TxtBoxQueryMessage.TabIndex = 36;
            this.TxtBoxQueryMessage.Text = "";
            // 
            // SpinEditQueryTimeout
            // 
            this.SpinEditQueryTimeout.Font = new System.Drawing.Font("Arial", 8F);
            this.SpinEditQueryTimeout.Location = new System.Drawing.Point(201, 91);
            this.SpinEditQueryTimeout.Margin = new System.Windows.Forms.Padding(2);
            this.SpinEditQueryTimeout.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.SpinEditQueryTimeout.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SpinEditQueryTimeout.Name = "SpinEditQueryTimeout";
            this.SpinEditQueryTimeout.Size = new System.Drawing.Size(37, 20);
            this.SpinEditQueryTimeout.TabIndex = 35;
            this.SpinEditQueryTimeout.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LblQueryTimeout
            // 
            this.LblQueryTimeout.AutoSize = true;
            this.LblQueryTimeout.BackColor = System.Drawing.Color.White;
            this.LblQueryTimeout.Font = new System.Drawing.Font("Arial", 9F);
            this.LblQueryTimeout.Location = new System.Drawing.Point(37, 92);
            this.LblQueryTimeout.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblQueryTimeout.Name = "LblQueryTimeout";
            this.LblQueryTimeout.Size = new System.Drawing.Size(117, 15);
            this.LblQueryTimeout.TabIndex = 33;
            this.LblQueryTimeout.Text = "Query timeout (sec.)";
            // 
            // LblQueryMessage
            // 
            this.LblQueryMessage.AutoSize = true;
            this.LblQueryMessage.BackColor = System.Drawing.Color.White;
            this.LblQueryMessage.Font = new System.Drawing.Font("Arial", 9F);
            this.LblQueryMessage.Location = new System.Drawing.Point(37, 119);
            this.LblQueryMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblQueryMessage.Name = "LblQueryMessage";
            this.LblQueryMessage.Size = new System.Drawing.Size(95, 15);
            this.LblQueryMessage.TabIndex = 32;
            this.LblQueryMessage.Text = "Query message";
            // 
            // ChkBoxStartEndpointClientWithWindows
            // 
            this.ChkBoxStartEndpointClientWithWindows.AutoSize = true;
            this.ChkBoxStartEndpointClientWithWindows.BackColor = System.Drawing.Color.White;
            this.ChkBoxStartEndpointClientWithWindows.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxStartEndpointClientWithWindows.Location = new System.Drawing.Point(288, 144);
            this.ChkBoxStartEndpointClientWithWindows.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxStartEndpointClientWithWindows.Name = "ChkBoxStartEndpointClientWithWindows";
            this.ChkBoxStartEndpointClientWithWindows.Size = new System.Drawing.Size(217, 19);
            this.ChkBoxStartEndpointClientWithWindows.TabIndex = 31;
            this.ChkBoxStartEndpointClientWithWindows.Text = "Start Endpoint Client with Windows";
            this.ChkBoxStartEndpointClientWithWindows.UseVisualStyleBackColor = false;
            this.ChkBoxStartEndpointClientWithWindows.CheckedChanged += new System.EventHandler(this.ChkBoxStartEndpointClientWithWindows_CheckedChanged);
            // 
            // ChkBoxLockOnDisconnect
            // 
            this.ChkBoxLockOnDisconnect.AutoSize = true;
            this.ChkBoxLockOnDisconnect.BackColor = System.Drawing.Color.White;
            this.ChkBoxLockOnDisconnect.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxLockOnDisconnect.Location = new System.Drawing.Point(288, 88);
            this.ChkBoxLockOnDisconnect.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxLockOnDisconnect.Name = "ChkBoxLockOnDisconnect";
            this.ChkBoxLockOnDisconnect.Size = new System.Drawing.Size(132, 19);
            this.ChkBoxLockOnDisconnect.TabIndex = 30;
            this.ChkBoxLockOnDisconnect.Text = "Lock on disconnect";
            this.ChkBoxLockOnDisconnect.UseVisualStyleBackColor = false;
            // 
            // ChkBoxAllowMultipleConnections
            // 
            this.ChkBoxAllowMultipleConnections.AutoSize = true;
            this.ChkBoxAllowMultipleConnections.BackColor = System.Drawing.Color.White;
            this.ChkBoxAllowMultipleConnections.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxAllowMultipleConnections.Location = new System.Drawing.Point(288, 65);
            this.ChkBoxAllowMultipleConnections.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxAllowMultipleConnections.Name = "ChkBoxAllowMultipleConnections";
            this.ChkBoxAllowMultipleConnections.Size = new System.Drawing.Size(172, 19);
            this.ChkBoxAllowMultipleConnections.TabIndex = 29;
            this.ChkBoxAllowMultipleConnections.Text = "Allow multiple connections";
            this.ChkBoxAllowMultipleConnections.UseVisualStyleBackColor = false;
            // 
            // ChkBoxEnableFileTransfer
            // 
            this.ChkBoxEnableFileTransfer.AutoSize = true;
            this.ChkBoxEnableFileTransfer.BackColor = System.Drawing.Color.White;
            this.ChkBoxEnableFileTransfer.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxEnableFileTransfer.Location = new System.Drawing.Point(288, 42);
            this.ChkBoxEnableFileTransfer.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxEnableFileTransfer.Name = "ChkBoxEnableFileTransfer";
            this.ChkBoxEnableFileTransfer.Size = new System.Drawing.Size(129, 19);
            this.ChkBoxEnableFileTransfer.TabIndex = 28;
            this.ChkBoxEnableFileTransfer.Text = "Enable file transfer";
            this.ChkBoxEnableFileTransfer.UseVisualStyleBackColor = false;
            // 
            // ChkBoxQueryIfNoLogon
            // 
            this.ChkBoxQueryIfNoLogon.AutoSize = true;
            this.ChkBoxQueryIfNoLogon.BackColor = System.Drawing.Color.White;
            this.ChkBoxQueryIfNoLogon.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxQueryIfNoLogon.Location = new System.Drawing.Point(40, 65);
            this.ChkBoxQueryIfNoLogon.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxQueryIfNoLogon.Name = "ChkBoxQueryIfNoLogon";
            this.ChkBoxQueryIfNoLogon.Size = new System.Drawing.Size(118, 19);
            this.ChkBoxQueryIfNoLogon.TabIndex = 27;
            this.ChkBoxQueryIfNoLogon.Text = "Query if no logon";
            this.ChkBoxQueryIfNoLogon.UseVisualStyleBackColor = false;
            // 
            // ChkBoxDisplayQueryWindow
            // 
            this.ChkBoxDisplayQueryWindow.AutoSize = true;
            this.ChkBoxDisplayQueryWindow.BackColor = System.Drawing.Color.White;
            this.ChkBoxDisplayQueryWindow.Font = new System.Drawing.Font("Arial", 9F);
            this.ChkBoxDisplayQueryWindow.Location = new System.Drawing.Point(21, 42);
            this.ChkBoxDisplayQueryWindow.Margin = new System.Windows.Forms.Padding(2);
            this.ChkBoxDisplayQueryWindow.Name = "ChkBoxDisplayQueryWindow";
            this.ChkBoxDisplayQueryWindow.Size = new System.Drawing.Size(145, 19);
            this.ChkBoxDisplayQueryWindow.TabIndex = 26;
            this.ChkBoxDisplayQueryWindow.Text = "Display query window";
            this.ChkBoxDisplayQueryWindow.UseVisualStyleBackColor = false;
            this.ChkBoxDisplayQueryWindow.CheckedChanged += new System.EventHandler(this.chkBoxDisplayQueryWindow_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(285, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 15);
            this.label3.TabIndex = 25;
            this.label3.Text = "Client Settings";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(285, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "Session Settings";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(18, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 15);
            this.label7.TabIndex = 23;
            this.label7.Text = "Remote Control Settings";
            // 
            // Profiles
            // 
            this.Profiles.BackColor = System.Drawing.Color.White;
            this.Profiles.Controls.Add(this.ProfileRemovePictureBox);
            this.Profiles.Controls.Add(this.ProfileEditPictureBox);
            this.Profiles.Controls.Add(this.ProfileAddPictureBox);
            this.Profiles.Controls.Add(this.ProfileGridView);
            this.Profiles.Font = new System.Drawing.Font("Arial", 9F);
            this.Profiles.Location = new System.Drawing.Point(4, 24);
            this.Profiles.Name = "Profiles";
            this.Profiles.Padding = new System.Windows.Forms.Padding(3);
            this.Profiles.Size = new System.Drawing.Size(557, 245);
            this.Profiles.TabIndex = 1;
            this.Profiles.Text = "Profiles";
            // 
            // ProfileRemovePictureBox
            // 
            this.ProfileRemovePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProfileRemovePictureBox.BackgroundImage")));
            this.ProfileRemovePictureBox.Location = new System.Drawing.Point(424, 205);
            this.ProfileRemovePictureBox.Name = "ProfileRemovePictureBox";
            this.ProfileRemovePictureBox.Size = new System.Drawing.Size(111, 24);
            this.ProfileRemovePictureBox.TabIndex = 6;
            this.ProfileRemovePictureBox.TabStop = false;
            this.ProfileRemovePictureBox.Click += new System.EventHandler(this.ProfileRemovePictureBox_Click);
            this.ProfileRemovePictureBox.MouseLeave += new System.EventHandler(this.ProfileRemovePictureBox_MouseLeave);
            this.ProfileRemovePictureBox.MouseHover += new System.EventHandler(this.ProfileRemovePictureBox_MouseHover);
            // 
            // ProfileEditPictureBox
            // 
            this.ProfileEditPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProfileEditPictureBox.BackgroundImage")));
            this.ProfileEditPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ProfileEditPictureBox.Location = new System.Drawing.Point(303, 205);
            this.ProfileEditPictureBox.Name = "ProfileEditPictureBox";
            this.ProfileEditPictureBox.Size = new System.Drawing.Size(111, 24);
            this.ProfileEditPictureBox.TabIndex = 5;
            this.ProfileEditPictureBox.TabStop = false;
            this.ProfileEditPictureBox.Click += new System.EventHandler(this.ProfileEditPictureBox_Click);
            this.ProfileEditPictureBox.MouseLeave += new System.EventHandler(this.ProfileEditPictureBox_MouseLeave);
            this.ProfileEditPictureBox.MouseHover += new System.EventHandler(this.ProfileEditPictureBox_MouseHover);
            // 
            // ProfileAddPictureBox
            // 
            this.ProfileAddPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProfileAddPictureBox.BackgroundImage")));
            this.ProfileAddPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ProfileAddPictureBox.Location = new System.Drawing.Point(181, 205);
            this.ProfileAddPictureBox.Name = "ProfileAddPictureBox";
            this.ProfileAddPictureBox.Size = new System.Drawing.Size(111, 24);
            this.ProfileAddPictureBox.TabIndex = 4;
            this.ProfileAddPictureBox.TabStop = false;
            this.ProfileAddPictureBox.Click += new System.EventHandler(this.ProfileAddPictureBox_Click);
            this.ProfileAddPictureBox.MouseLeave += new System.EventHandler(this.ProfileAddPictureBox_MouseLeave);
            this.ProfileAddPictureBox.MouseHover += new System.EventHandler(this.ProfileAddPictureBox_MouseHover);
            // 
            // ProfileGridView
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.ProfileGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ProfileGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProfileGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.ProfileGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProfileGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ConnectionName,
            this.ManagerAddress,
            this.ManagerTCPPort,
            this.connectionMethod});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ProfileGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.ProfileGridView.GridColor = System.Drawing.Color.White;
            this.ProfileGridView.Location = new System.Drawing.Point(27, 20);
            this.ProfileGridView.Name = "ProfileGridView";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ProfileGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.ProfileGridView.RowHeadersVisible = false;
            this.ProfileGridView.RowTemplate.Height = 24;
            this.ProfileGridView.Size = new System.Drawing.Size(508, 169);
            this.ProfileGridView.TabIndex = 0;
            // 
            // ConnectionName
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.ConnectionName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ConnectionName.HeaderText = "Profile Name";
            this.ConnectionName.Name = "ConnectionName";
            this.ConnectionName.ReadOnly = true;
            this.ConnectionName.Width = 155;
            // 
            // ManagerAddress
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.ManagerAddress.DefaultCellStyle = dataGridViewCellStyle4;
            this.ManagerAddress.HeaderText = "Manager Address";
            this.ManagerAddress.Name = "ManagerAddress";
            this.ManagerAddress.ReadOnly = true;
            this.ManagerAddress.Width = 130;
            // 
            // ManagerTCPPort
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.ManagerTCPPort.DefaultCellStyle = dataGridViewCellStyle5;
            this.ManagerTCPPort.FillWeight = 50F;
            this.ManagerTCPPort.HeaderText = "Manager Port";
            this.ManagerTCPPort.Name = "ManagerTCPPort";
            this.ManagerTCPPort.ReadOnly = true;
            this.ManagerTCPPort.Width = 120;
            // 
            // connectionMethod
            // 
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9F);
            this.connectionMethod.DefaultCellStyle = dataGridViewCellStyle6;
            this.connectionMethod.HeaderText = "Connection Method";
            this.connectionMethod.Name = "connectionMethod";
            this.connectionMethod.ReadOnly = true;
            // 
            // Identity
            // 
            this.Identity.BackColor = System.Drawing.Color.White;
            this.Identity.Controls.Add(this.IdentityRemovePictureBox);
            this.Identity.Controls.Add(this.IdentityEditPictureBox);
            this.Identity.Controls.Add(this.IdentityAddPictureBox);
            this.Identity.Controls.Add(this.IdentityDataGridView);
            this.Identity.Location = new System.Drawing.Point(4, 24);
            this.Identity.Name = "Identity";
            this.Identity.Padding = new System.Windows.Forms.Padding(3);
            this.Identity.Size = new System.Drawing.Size(557, 245);
            this.Identity.TabIndex = 2;
            this.Identity.Text = "Identity";
            // 
            // IdentityRemovePictureBox
            // 
            this.IdentityRemovePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IdentityRemovePictureBox.BackgroundImage")));
            this.IdentityRemovePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.IdentityRemovePictureBox.Location = new System.Drawing.Point(424, 205);
            this.IdentityRemovePictureBox.Name = "IdentityRemovePictureBox";
            this.IdentityRemovePictureBox.Size = new System.Drawing.Size(111, 24);
            this.IdentityRemovePictureBox.TabIndex = 6;
            this.IdentityRemovePictureBox.TabStop = false;
            this.IdentityRemovePictureBox.MouseLeave += new System.EventHandler(this.IdentityRemovePictureBox_MouseLeave);
            this.IdentityRemovePictureBox.MouseHover += new System.EventHandler(this.IdentityRemovePictureBox_MouseHover);
            // 
            // IdentityEditPictureBox
            // 
            this.IdentityEditPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IdentityEditPictureBox.BackgroundImage")));
            this.IdentityEditPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.IdentityEditPictureBox.Location = new System.Drawing.Point(303, 205);
            this.IdentityEditPictureBox.Name = "IdentityEditPictureBox";
            this.IdentityEditPictureBox.Size = new System.Drawing.Size(111, 24);
            this.IdentityEditPictureBox.TabIndex = 5;
            this.IdentityEditPictureBox.TabStop = false;
            this.IdentityEditPictureBox.MouseLeave += new System.EventHandler(this.IdentityEditPictureBox_MouseLeave);
            this.IdentityEditPictureBox.MouseHover += new System.EventHandler(this.IdentityEditPictureBox_MouseHover);
            // 
            // IdentityAddPictureBox
            // 
            this.IdentityAddPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IdentityAddPictureBox.BackgroundImage")));
            this.IdentityAddPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.IdentityAddPictureBox.Location = new System.Drawing.Point(181, 205);
            this.IdentityAddPictureBox.Name = "IdentityAddPictureBox";
            this.IdentityAddPictureBox.Size = new System.Drawing.Size(111, 24);
            this.IdentityAddPictureBox.TabIndex = 4;
            this.IdentityAddPictureBox.TabStop = false;
            this.IdentityAddPictureBox.Click += new System.EventHandler(this.IdentityAddPictureBox_Click);
            this.IdentityAddPictureBox.MouseLeave += new System.EventHandler(this.IdentityAddPictureBox_MouseLeave);
            this.IdentityAddPictureBox.MouseHover += new System.EventHandler(this.IdentityAddPictureBox_MouseHover);
            // 
            // IdentityDataGridView
            // 
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.IdentityDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.IdentityDataGridView.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.IdentityDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.IdentityDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IdentityDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TrustPointName,
            this.KeyManagement,
            this.IssueCA});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.IdentityDataGridView.DefaultCellStyle = dataGridViewCellStyle11;
            this.IdentityDataGridView.GridColor = System.Drawing.Color.White;
            this.IdentityDataGridView.Location = new System.Drawing.Point(27, 20);
            this.IdentityDataGridView.Name = "IdentityDataGridView";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.IdentityDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.IdentityDataGridView.RowHeadersVisible = false;
            this.IdentityDataGridView.RowTemplate.Height = 24;
            this.IdentityDataGridView.Size = new System.Drawing.Size(508, 169);
            this.IdentityDataGridView.TabIndex = 0;
            // 
            // TrustPointName
            // 
            this.TrustPointName.HeaderText = "Trustpoint Name";
            this.TrustPointName.Name = "TrustPointName";
            this.TrustPointName.Width = 195;
            // 
            // KeyManagement
            // 
            this.KeyManagement.HeaderText = "Key Management";
            this.KeyManagement.Name = "KeyManagement";
            this.KeyManagement.Width = 160;
            // 
            // IssueCA
            // 
            this.IssueCA.HeaderText = "Issue CA Name";
            this.IssueCA.Name = "IssueCA";
            this.IssueCA.Width = 150;
            // 
            // Log
            // 
            this.Log.BackColor = System.Drawing.Color.White;
            this.Log.Controls.Add(this.LogClearButtonPictureBox);
            this.Log.Controls.Add(this.LogRichTextBox);
            this.Log.Location = new System.Drawing.Point(4, 24);
            this.Log.Name = "Log";
            this.Log.Padding = new System.Windows.Forms.Padding(3);
            this.Log.Size = new System.Drawing.Size(557, 245);
            this.Log.TabIndex = 3;
            this.Log.Text = "Log";
            // 
            // LogClearButtonPictureBox
            // 
            this.LogClearButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LogClearButtonPictureBox.BackgroundImage")));
            this.LogClearButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LogClearButtonPictureBox.Location = new System.Drawing.Point(424, 205);
            this.LogClearButtonPictureBox.Name = "LogClearButtonPictureBox";
            this.LogClearButtonPictureBox.Size = new System.Drawing.Size(111, 24);
            this.LogClearButtonPictureBox.TabIndex = 2;
            this.LogClearButtonPictureBox.TabStop = false;
            this.LogClearButtonPictureBox.Click += new System.EventHandler(this.LogClearButtonPictureBox_Click);
            this.LogClearButtonPictureBox.MouseLeave += new System.EventHandler(this.LogClearButtonPictureBox_MouseLeave);
            this.LogClearButtonPictureBox.MouseHover += new System.EventHandler(this.LogClearButtonPictureBox_MouseHover);
            // 
            // LogRichTextBox
            // 
            this.LogRichTextBox.BackColor = System.Drawing.Color.White;
            this.LogRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogRichTextBox.Location = new System.Drawing.Point(27, 20);
            this.LogRichTextBox.Name = "LogRichTextBox";
            this.LogRichTextBox.ReadOnly = true;
            this.LogRichTextBox.Size = new System.Drawing.Size(508, 169);
            this.LogRichTextBox.TabIndex = 0;
            this.LogRichTextBox.Text = "";
            // 
            // MinimizePictureBox
            // 
            this.MinimizePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizePictureBox.BackgroundImage")));
            this.MinimizePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MinimizePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizePictureBox.Location = new System.Drawing.Point(606, 7);
            this.MinimizePictureBox.Name = "MinimizePictureBox";
            this.MinimizePictureBox.Size = new System.Drawing.Size(10, 10);
            this.MinimizePictureBox.TabIndex = 13;
            this.MinimizePictureBox.TabStop = false;
            this.MinimizePictureBox.Click += new System.EventHandler(this.MinimizePictureBox_Click);
            this.MinimizePictureBox.MouseLeave += new System.EventHandler(this.MinimizePictureBox_MouseLeave);
            this.MinimizePictureBox.MouseHover += new System.EventHandler(this.MinimizePictureBox_MouseHover);
            // 
            // ClosePictureBox
            // 
            this.ClosePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ClosePictureBox.BackgroundImage")));
            this.ClosePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClosePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClosePictureBox.Location = new System.Drawing.Point(621, 7);
            this.ClosePictureBox.Name = "ClosePictureBox";
            this.ClosePictureBox.Size = new System.Drawing.Size(10, 10);
            this.ClosePictureBox.TabIndex = 12;
            this.ClosePictureBox.TabStop = false;
            this.ClosePictureBox.Click += new System.EventHandler(this.ClosePictureBox_Click);
            this.ClosePictureBox.MouseLeave += new System.EventHandler(this.ClosePictureBox_MouseLeave);
            this.ClosePictureBox.MouseHover += new System.EventHandler(this.ClosePictureBox_MouseHover);
            // 
            // LinkPictureBox
            // 
            this.LinkPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LinkPictureBox.BackgroundImage")));
            this.LinkPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LinkPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LinkPictureBox.Location = new System.Drawing.Point(522, 405);
            this.LinkPictureBox.Name = "LinkPictureBox";
            this.LinkPictureBox.Size = new System.Drawing.Size(100, 12);
            this.LinkPictureBox.TabIndex = 17;
            this.LinkPictureBox.TabStop = false;
            this.LinkPictureBox.Click += new System.EventHandler(this.LinkPictureBox_Click);
            this.LinkPictureBox.MouseLeave += new System.EventHandler(this.LinkPictureBox_MouseLeave);
            this.LinkPictureBox.MouseHover += new System.EventHandler(this.LinkPictureBox_MouseHover);
            // 
            // InfoPictureBox
            // 
            this.InfoPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InfoPictureBox.BackgroundImage")));
            this.InfoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.InfoPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.InfoPictureBox.Location = new System.Drawing.Point(19, 405);
            this.InfoPictureBox.Name = "InfoPictureBox";
            this.InfoPictureBox.Size = new System.Drawing.Size(12, 12);
            this.InfoPictureBox.TabIndex = 16;
            this.InfoPictureBox.TabStop = false;
            this.InfoPictureBox.Click += new System.EventHandler(this.InfoPictureBox_Click);
            this.InfoPictureBox.MouseLeave += new System.EventHandler(this.InfoPictureBox_MouseLeave);
            this.InfoPictureBox.MouseHover += new System.EventHandler(this.InfoPictureBox_MouseHover);
            // 
            // TreePictureBox
            // 
            this.TreePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TreePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TreePictureBox.Location = new System.Drawing.Point(21, 8);
            this.TreePictureBox.Name = "TreePictureBox";
            this.TreePictureBox.Size = new System.Drawing.Size(114, 50);
            this.TreePictureBox.TabIndex = 19;
            this.TreePictureBox.TabStop = false;
            this.TreePictureBox.Click += new System.EventHandler(this.TreePictureBox_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Location = new System.Drawing.Point(35, 98);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(382, 11);
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(281, 102);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(328, 28);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // spinBoxConnectionTimeout
            // 
            this.spinBoxConnectionTimeout.Font = new System.Drawing.Font("Arial", 8F);
            this.spinBoxConnectionTimeout.Location = new System.Drawing.Point(467, 164);
            this.spinBoxConnectionTimeout.Margin = new System.Windows.Forms.Padding(2);
            this.spinBoxConnectionTimeout.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.spinBoxConnectionTimeout.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.spinBoxConnectionTimeout.Name = "spinBoxConnectionTimeout";
            this.spinBoxConnectionTimeout.Size = new System.Drawing.Size(37, 20);
            this.spinBoxConnectionTimeout.TabIndex = 38;
            this.spinBoxConnectionTimeout.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // lblConnectionTimeout
            // 
            this.lblConnectionTimeout.AutoSize = true;
            this.lblConnectionTimeout.BackColor = System.Drawing.Color.White;
            this.lblConnectionTimeout.Font = new System.Drawing.Font("Arial", 9F);
            this.lblConnectionTimeout.Location = new System.Drawing.Point(303, 165);
            this.lblConnectionTimeout.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblConnectionTimeout.Name = "lblConnectionTimeout";
            this.lblConnectionTimeout.Size = new System.Drawing.Size(148, 15);
            this.lblConnectionTimeout.TabIndex = 37;
            this.lblConnectionTimeout.Text = "Connection timeout (sec.)";
            // 
            // SettingsForm
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(640, 425);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SettingsTabControl);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.TreePictureBox);
            this.Controls.Add(this.LinkPictureBox);
            this.Controls.Add(this.InfoPictureBox);
            this.Controls.Add(this.MinimizePictureBox);
            this.Controls.Add(this.ClosePictureBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.Text = "BE CMS Endpoint Client Connection Details";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.SettingsTabControl.ResumeLayout(false);
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.Global.ResumeLayout(false);
            this.Global.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEditQueryTimeout)).EndInit();
            this.Profiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProfileRemovePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileEditPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileAddPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProfileGridView)).EndInit();
            this.Identity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IdentityRemovePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityEditPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityAddPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdentityDataGridView)).EndInit();
            this.Log.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogClearButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinBoxConnectionTimeout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl SettingsTabControl;
        private System.Windows.Forms.TabPage Status;
        private System.Windows.Forms.TabPage Profiles;
        private System.Windows.Forms.TabPage Identity;
        private System.Windows.Forms.Label State;
        private System.Windows.Forms.Label ConnectionState;
        private System.Windows.Forms.Label Duration;
        private System.Windows.Forms.Label ConnectionDuration;
        private System.Windows.Forms.Label BytesSent;
        private System.Windows.Forms.Label ConnectionBytesSent;
        private System.Windows.Forms.Label BytesReceived;
        private System.Windows.Forms.Label ConnectionBytesReceived;
        private System.Windows.Forms.Label ManagerVPNAdddress;
        private System.Windows.Forms.Label StatusManagerAddress;
        private System.Windows.Forms.Label ConnectionManagerAddress;
        private System.Windows.Forms.Label ConnectionManagerVPNAddress;
        private System.Windows.Forms.Label ClientVPNAddress;
        private System.Windows.Forms.Label ConnectionClientVPNAddress;
        private System.Windows.Forms.Label ConnectionManagerTCPport;
        private System.Windows.Forms.Label VPNTCPport;
        private System.Windows.Forms.DataGridView ProfileGridView;
        private System.Windows.Forms.TabPage Log;
        private System.Windows.Forms.RichTextBox LogRichTextBox;
        private System.Windows.Forms.DataGridView IdentityDataGridView;
        private System.Windows.Forms.Label ClientInfoLockdown;
        private System.Windows.Forms.Label Lockdown;
        private System.Windows.Forms.Label ClientInfoCustomName;
        private System.Windows.Forms.Label LogicalName;
        private System.Windows.Forms.Label ClientInfoSystemName;
        private System.Windows.Forms.Label SystemName;
        private System.Windows.Forms.Label ClientInfoOSversion;
        private System.Windows.Forms.Label OSversion;
        private System.Windows.Forms.Label ClientInfoSerialNumber;
        private System.Windows.Forms.Label SerialNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox MinimizePictureBox;
        private System.Windows.Forms.PictureBox ClosePictureBox;
        private System.Windows.Forms.PictureBox LinkPictureBox;
        private System.Windows.Forms.PictureBox InfoPictureBox;
        private System.Windows.Forms.PictureBox ProfileAddPictureBox;
        private System.Windows.Forms.PictureBox ProfileEditPictureBox;
        private System.Windows.Forms.PictureBox ProfileRemovePictureBox;
        private System.Windows.Forms.PictureBox IdentityAddPictureBox;
        private System.Windows.Forms.PictureBox IdentityEditPictureBox;
        private System.Windows.Forms.PictureBox IdentityRemovePictureBox;
        private System.Windows.Forms.PictureBox LogClearButtonPictureBox;
        private System.Windows.Forms.PictureBox TreePictureBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrustPointName;
        private System.Windows.Forms.DataGridViewTextBoxColumn KeyManagement;
        private System.Windows.Forms.DataGridViewTextBoxColumn IssueCA;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage Global;
        private System.Windows.Forms.NumericUpDown SpinEditQueryTimeout;
        private System.Windows.Forms.Label LblQueryTimeout;
        private System.Windows.Forms.Label LblQueryMessage;
        private System.Windows.Forms.CheckBox ChkBoxStartEndpointClientWithWindows;
        private System.Windows.Forms.CheckBox ChkBoxLockOnDisconnect;
        private System.Windows.Forms.CheckBox ChkBoxAllowMultipleConnections;
        private System.Windows.Forms.CheckBox ChkBoxEnableFileTransfer;
        private System.Windows.Forms.CheckBox ChkBoxQueryIfNoLogon;
        private System.Windows.Forms.CheckBox ChkBoxDisplayQueryWindow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox TxtBoxQueryMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConnectionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManagerAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManagerTCPPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn connectionMethod;
        private System.Windows.Forms.NumericUpDown spinBoxConnectionTimeout;
        private System.Windows.Forms.Label lblConnectionTimeout;

    }
}

