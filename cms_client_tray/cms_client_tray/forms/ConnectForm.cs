﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Diagnostics;
using cms_transport;
using cms_client_tray.Properties;
using cms_client_tray.utils;


namespace cms_client_tray
{
    public class ConnectForm : Form, IProtocolListener
    {
        private IContainer components;
        private PictureBox ClosePictureBox;
        private PictureBox MinimizePictureBox;
        private PictureBox ConnectButtonPictureBox;
        private PictureBox InfoPictureBox;
        private PictureBox SettingsPictureBox;
        private PictureBox LinkPictureBox;
        private PictureBox LockPictureBox;
        private PictureBox TreePictureBox;
        private Label TimerLabel;
        private Label ConnectStatus;
        private NotifyIcon trayIcon;
        private System.Windows.Forms.ComboBox ConnListComboBox;
        private Timer connectBtnUpdateTimer;

        private static bool connect_form_hidden;
        private static bool connect_button_locked = false;
        
        private static ConnectForm m_instance = null;
        private ConnectingIconUpdater m_iconUpdater = null;

        public ConnectForm()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.None;
            this.MouseDown += new MouseEventHandler(ConnectForm_MouseDown);

            // Create a simple tray menu with only one item.
            ContextMenu trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Open BE CMS Endpoint Client", OnOpen);
            trayMenu.MenuItems.Add("About", OnAbout);
            trayMenu.MenuItems.Add("Quit", OnExit);

            // Create a tray icon. In this example we use a
            // standard system icon for simplicity, but you
            // can of course use your own custom icon too.
            trayIcon = new NotifyIcon();
            trayIcon.Text = "BE CMS Endpoint Client \nStatus: Not Connected";
            trayIcon.Icon = Resources.Disconnected;

            // Add menu to tray icon and show it.
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;

            // Handle the DoubleClick event to activate the form.
            trayIcon.Click += new System.EventHandler(this.trayIcon_DoubleClick);

            m_iconUpdater = new ConnectingIconUpdater(this);
        }

        [STAThread]
        public static void Main()
        {
            //check if only one instanse running...
            if (!AppUtils.hasSecondInstance())
            {
                m_instance = new ConnectForm();
                Application.Run(m_instance);
            }
        }

        public static ConnectForm getForm()
        {
            return m_instance;
        }

        void ConnectForm_MouseDown(object sender, MouseEventArgs e)
        {
            base.Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }  

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            if (!connect_form_hidden)
            {
                this.Hide();
                connect_form_hidden = true;
                e.Cancel = true;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Visible = false; // Hide form window.
            ShowInTaskbar = false; // Remove from taskbar.
            ClientProtocol.register(this);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            ClientProtocol.unregister(this);
        }

        private void OnAbout(object sender, EventArgs e)
        {
            // show InfoForm
            this.TopMost = false;
            InfoForm info_frm = new InfoForm();
            info_frm.ShowDialog();
        }

        private void OnOpen(object sender, EventArgs e)
        {
            // this.WindowState = FormWindowState.Normal;
            this.TopMost = true;
            this.Show();
            connect_form_hidden = false;
        }

        private void OnExit(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void trayIcon_DoubleClick(object Sender, EventArgs e)
        {

            if (connect_form_hidden) // this.WindowState == FormWindowState.Minimized)
            {
                // this.WindowState = FormWindowState.Normal;
                this.TopMost = true;
                this.Show();
                connect_form_hidden = false;
            }
            else
            {
                // this.WindowState = FormWindowState.Minimized;
                this.Hide();
                connect_form_hidden = true;
            }
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                // Release the icon resource.
                trayIcon.Dispose();
            }

            base.Dispose(isDisposing);
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectForm));
            this.ConnListComboBox = new System.Windows.Forms.ComboBox();
            this.ConnectStatus = new System.Windows.Forms.Label();
            this.ClosePictureBox = new System.Windows.Forms.PictureBox();
            this.MinimizePictureBox = new System.Windows.Forms.PictureBox();
            this.ConnectButtonPictureBox = new System.Windows.Forms.PictureBox();
            this.InfoPictureBox = new System.Windows.Forms.PictureBox();
            this.SettingsPictureBox = new System.Windows.Forms.PictureBox();
            this.LinkPictureBox = new System.Windows.Forms.PictureBox();
            this.LockPictureBox = new System.Windows.Forms.PictureBox();
            this.TreePictureBox = new System.Windows.Forms.PictureBox();
            this.TimerLabel = new System.Windows.Forms.Label();
            this.connectBtnUpdateTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectButtonPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LockPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ConnListComboBox
            // 
            this.ConnListComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ConnListComboBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.ConnListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConnListComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConnListComboBox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnListComboBox.ForeColor = System.Drawing.Color.Black;
            this.ConnListComboBox.FormattingEnabled = true;
            this.ConnListComboBox.ItemHeight = 16;
            this.ConnListComboBox.Location = new System.Drawing.Point(36, 75);
            this.ConnListComboBox.Name = "ConnListComboBox";
            this.ConnListComboBox.Size = new System.Drawing.Size(262, 24);
            this.ConnListComboBox.TabIndex = 0;
            // 
            // ConnectStatus
            // 
            this.ConnectStatus.AutoSize = true;
            this.ConnectStatus.BackColor = System.Drawing.Color.Transparent;
            this.ConnectStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConnectStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ConnectStatus.Location = new System.Drawing.Point(33, 112);
            this.ConnectStatus.Name = "ConnectStatus";
            this.ConnectStatus.Size = new System.Drawing.Size(53, 14);
            this.ConnectStatus.TabIndex = 7;
            this.ConnectStatus.Text = "Unknown";
            // 
            // ClosePictureBox
            // 
            this.ClosePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ClosePictureBox.BackgroundImage")));
            this.ClosePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClosePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClosePictureBox.Location = new System.Drawing.Point(432, 6);
            this.ClosePictureBox.Name = "ClosePictureBox";
            this.ClosePictureBox.Size = new System.Drawing.Size(10, 10);
            this.ClosePictureBox.TabIndex = 10;
            this.ClosePictureBox.TabStop = false;
            this.ClosePictureBox.Click += new System.EventHandler(this.ClosePictureBox_Click);
            this.ClosePictureBox.MouseLeave += new System.EventHandler(this.ClosePictureBox_MouseLeave);
            this.ClosePictureBox.MouseHover += new System.EventHandler(this.ClosePictureBox_MouseHover);
            // 
            // MinimizePictureBox
            // 
            this.MinimizePictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MinimizePictureBox.BackgroundImage")));
            this.MinimizePictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MinimizePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizePictureBox.Location = new System.Drawing.Point(417, 6);
            this.MinimizePictureBox.Name = "MinimizePictureBox";
            this.MinimizePictureBox.Size = new System.Drawing.Size(10, 10);
            this.MinimizePictureBox.TabIndex = 11;
            this.MinimizePictureBox.TabStop = false;
            this.MinimizePictureBox.Click += new System.EventHandler(this.MinimizePictureBox_Click);
            this.MinimizePictureBox.MouseLeave += new System.EventHandler(this.MinimizePictureBox_MouseLeave);
            this.MinimizePictureBox.MouseHover += new System.EventHandler(this.MinimizePictureBox_MouseHover);
            // 
            // ConnectButtonPictureBox
            // 
            this.ConnectButtonPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ConnectButtonPictureBox.BackgroundImage")));
            this.ConnectButtonPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ConnectButtonPictureBox.Location = new System.Drawing.Point(304, 75);
            this.ConnectButtonPictureBox.Name = "ConnectButtonPictureBox";
            this.ConnectButtonPictureBox.Size = new System.Drawing.Size(111, 24);
            this.ConnectButtonPictureBox.TabIndex = 12;
            this.ConnectButtonPictureBox.TabStop = false;
            this.ConnectButtonPictureBox.Click += new System.EventHandler(this.ConnectButtonPictureBox_Click);
            this.ConnectButtonPictureBox.MouseLeave += new System.EventHandler(this.ConnectButtonPictureBox_MouseLeave);
            this.ConnectButtonPictureBox.MouseHover += new System.EventHandler(this.ConnectButtonPictureBox_MouseHover);
            // 
            // InfoPictureBox
            // 
            this.InfoPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InfoPictureBox.BackgroundImage")));
            this.InfoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.InfoPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.InfoPictureBox.Location = new System.Drawing.Point(20, 138);
            this.InfoPictureBox.Name = "InfoPictureBox";
            this.InfoPictureBox.Size = new System.Drawing.Size(12, 12);
            this.InfoPictureBox.TabIndex = 13;
            this.InfoPictureBox.TabStop = false;
            this.InfoPictureBox.Click += new System.EventHandler(this.InfoPictureBox_Click);
            this.InfoPictureBox.MouseLeave += new System.EventHandler(this.InfoPictureBox_MouseLeave);
            this.InfoPictureBox.MouseHover += new System.EventHandler(this.InfoPictureBox_MouseHover);
            // 
            // SettingsPictureBox
            // 
            this.SettingsPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SettingsPictureBox.BackgroundImage")));
            this.SettingsPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SettingsPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SettingsPictureBox.Location = new System.Drawing.Point(38, 138);
            this.SettingsPictureBox.Name = "SettingsPictureBox";
            this.SettingsPictureBox.Size = new System.Drawing.Size(12, 12);
            this.SettingsPictureBox.TabIndex = 14;
            this.SettingsPictureBox.TabStop = false;
            this.SettingsPictureBox.Click += new System.EventHandler(this.SettingsPictureBox_Click);
            this.SettingsPictureBox.MouseLeave += new System.EventHandler(this.SettingsPictureBox_MouseLeave);
            this.SettingsPictureBox.MouseHover += new System.EventHandler(this.SettingsPictureBox_MouseHover);
            // 
            // LinkPictureBox
            // 
            this.LinkPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LinkPictureBox.BackgroundImage")));
            this.LinkPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LinkPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LinkPictureBox.Location = new System.Drawing.Point(332, 138);
            this.LinkPictureBox.Name = "LinkPictureBox";
            this.LinkPictureBox.Size = new System.Drawing.Size(100, 12);
            this.LinkPictureBox.TabIndex = 15;
            this.LinkPictureBox.TabStop = false;
            this.LinkPictureBox.Click += new System.EventHandler(this.LinkPictureBox_Click);
            this.LinkPictureBox.MouseLeave += new System.EventHandler(this.LinkPictureBox_MouseLeave);
            this.LinkPictureBox.MouseHover += new System.EventHandler(this.LinkPictureBox_MouseHover);
            // 
            // LockPictureBox
            // 
            this.LockPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LockPictureBox.BackgroundImage")));
            this.LockPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LockPictureBox.Location = new System.Drawing.Point(119, 114);
            this.LockPictureBox.Name = "LockPictureBox";
            this.LockPictureBox.Size = new System.Drawing.Size(16, 11);
            this.LockPictureBox.TabIndex = 16;
            this.LockPictureBox.TabStop = false;
            // 
            // TreePictureBox
            // 
            this.TreePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TreePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TreePictureBox.Location = new System.Drawing.Point(12, 6);
            this.TreePictureBox.Name = "TreePictureBox";
            this.TreePictureBox.Size = new System.Drawing.Size(129, 50);
            this.TreePictureBox.TabIndex = 17;
            this.TreePictureBox.TabStop = false;
            this.TreePictureBox.Click += new System.EventHandler(this.TreePictureBox_Click);
            // 
            // TimerLabel
            // 
            this.TimerLabel.AutoSize = true;
            this.TimerLabel.BackColor = System.Drawing.Color.Transparent;
            this.TimerLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.TimerLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TimerLabel.Location = new System.Drawing.Point(369, 112);
            this.TimerLabel.Name = "TimerLabel";
            this.TimerLabel.Size = new System.Drawing.Size(49, 14);
            this.TimerLabel.TabIndex = 18;
            this.TimerLabel.Text = "00:00:00";
            // 
            // connectBtnUpdateTimer
            // 
            this.connectBtnUpdateTimer.Interval = 5000;
            this.connectBtnUpdateTimer.Tick += new System.EventHandler(this.connectBtnUpdateTimer_Tick);
            // 
            // ConnectForm
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(446, 160);
            this.Controls.Add(this.TimerLabel);
            this.Controls.Add(this.TreePictureBox);
            this.Controls.Add(this.LockPictureBox);
            this.Controls.Add(this.LinkPictureBox);
            this.Controls.Add(this.SettingsPictureBox);
            this.Controls.Add(this.InfoPictureBox);
            this.Controls.Add(this.ConnectButtonPictureBox);
            this.Controls.Add(this.MinimizePictureBox);
            this.Controls.Add(this.ClosePictureBox);
            this.Controls.Add(this.ConnectStatus);
            this.Controls.Add(this.ConnListComboBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectForm";
            this.ShowIcon = false;
            this.Text = "BE CMS Endpoint Client";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ConnectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ClosePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectButtonPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InfoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingsPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LockPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreePictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void loadConnections()
        {
            ConnListComboBox.Items.Clear();
            foreach (string key in CMSSettings.connections.Keys)
            {
                ConnListComboBox.Items.Add(key);
                if (CMSSettings.connections[key].name == CMSSettings.connectionName)
                {
                    ConnListComboBox.SelectedIndex = ConnListComboBox.Items.Count - 1;
                }
            }
        }
        
        private void ConnectForm_Load(object sender, System.EventArgs e)
        {
            //Load skin
            this.BackgroundImage = Resources.Connect_canvas;
            ClosePictureBox.BackgroundImage = Resources.Connect_close_leave;
            MinimizePictureBox.BackgroundImage = Resources.Connect_minimize_leave;
            ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_connect_leave;
            InfoPictureBox.BackgroundImage = Resources.Connect_info_leave;
            SettingsPictureBox.BackgroundImage = Resources.Connect_settings_leave;
            LinkPictureBox.BackgroundImage = Resources.Link_be_leave;

            //place form on bottom-right
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);

            // hide connection form
            this.Hide();
            connect_form_hidden = true;

            ClientProtocol.register();
        }

        public void onRegister(CMSRegisterResponse response)
        {
            loadConnections();
            if (!String.IsNullOrEmpty(response.connectionStatus) && response.connectionStatus == "CONNECTED")
            {
                ConnListComboBox.SelectedItem = response.connectionName;
                trayIcon.Text = "BE CMS Endpoint Client \nStatus: Connected";
                ConnectStatus.Text = "Connected";
                trayIcon.Icon = Resources.Connected;
                m_iconUpdater.stopUpdate();
                ConnListComboBox.Enabled = false;
                CMSSettings.isConnected = true;
            }
            else
            {
                // set connection_state to false
                Console.WriteLine("HERE"); 
                CMSSettings.isConnected = false;
            }
        }
        
        private void InfoButton_Click(object sender, System.EventArgs e)
        {
            AppUtils.showAbout(this);
        }

        private void setConnectStatus(string state, 
                                      string ConnectStatus, 
                                      bool ComboBoxEnabled, 
                                      bool ConnectButtonEnabled, 
                                      bool TimerLabelVisible, 
                                      string TimerDuration)
        {
            if (state == "Connect")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_connect_leave;
                LockPictureBox.BackgroundImage = Resources.Lock_unlocked;
                LockPictureBox.Left = 110;
                LockPictureBox.Visible = true;
            }
            else if (state == "Disconnect")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_disconnect_leave;
                LockPictureBox.BackgroundImage = Resources.Lock_locked;
                LockPictureBox.Left = 88;
                LockPictureBox.Visible = true;
            }
            else if (state == "Cancel")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_cancel_leave;
                LockPictureBox.Visible = false;
            }

            this.ConnectButtonPictureBox.Enabled = ConnectButtonEnabled;
            this.ConnectStatus.Text = ConnectStatus;
            this.ConnListComboBox.Enabled = ComboBoxEnabled;
            this.TimerLabel.Visible = TimerLabelVisible;
            this.TimerLabel.Text = TimerDuration;
            this.TimerLabel.Refresh();
        }

        public void onStatusReceived(CMSStatusResponse status)
        {
            if (status.connectionStatus == "NETWORKDOWN")
            {
                status.connectionStatus = "CONNECTING";
            }

            if (status.connectionStatus == "CONNECTED")
            {
                ConnListComboBox.SelectedItem = status.connectionStatus;
                trayIcon.Text = "BE CMS Endpoint Client \nStatus: Connected \nSN: " + status.network.serialNumber;
                trayIcon.Icon = Resources.Connected;
                m_iconUpdater.stopUpdate();
                CMSSettings.isConnected = true;

                setConnectStatus("Disconnect",
                    "Connected",
                    false,
                    true,
                    true,
                    status.vpn.lifetime);
            }
            else if (status.connectionStatus == "CONNECTING")
            {
                ConnListComboBox.SelectedItem = status.connectionStatus;
                trayIcon.Text = "BE CMS Endpoint Client \nStatus: Connecting...";
                m_iconUpdater.startUpdate();
                ConnListComboBox.Enabled = false;
                CMSSettings.isConnected = true;

                setConnectStatus("Cancel",
                    "Connecting...",
                    false,
                    true,
                    false,
                    "");
            }
            else if (status.connectionStatus == "WAITING_REQUEST")
            {
                ConnListComboBox.SelectedItem = status.connectionStatus;
                trayIcon.Text = "BE CMS Endpoint Client \nStatus: Waiting for request...";
                trayIcon.Icon = Resources.Waiting;
                m_iconUpdater.stopUpdate();
                ConnListComboBox.Enabled = false;
                CMSSettings.isConnected = true;

                setConnectStatus("Cancel",
                    "Waiting for request...",
                    false,
                    true,
                    false,
                    "");
            }
            else
            {
                trayIcon.Text = "BE CMS Endpoint Client \nStatus: Not Connected";
                trayIcon.Icon = Resources.Disconnected;
                CMSSettings.isConnected = false;
                m_iconUpdater.stopUpdate();
                setConnectStatus("Connect",
                    "Not Connected",
                    true,
                    true,
                    false,
                    "");
            }
        }

        private void ConnectButtonPictureBox_Click(object sender, EventArgs e)
        {
            if (connect_button_locked == false)
            {
                //Lock connect button
                connectBtnUpdateTimer.Enabled = true;
                connect_button_locked = true;
                ConnectButtonPictureBox.UseWaitCursor = true;
                AppUtils.updateFormCursor();

                if (!CMSSettings.isConnected)
                {
                    trayIcon.Text = "BE CMS Endpoint Client \nStatus: Connecting...";
                    ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_cancel_hover;
                    ConnectStatus.Text = "Connecting...";
                    m_iconUpdater.startUpdate();
                    ClientProtocol.connect(ConnListComboBox.Text, "CONNECTING");
                }
                else
                {
                    trayIcon.Text = "BE CMS Endpoint Client \nStatus: Not Connected";
                    ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_connect_leave;
                    ConnectStatus.Text = "Not Connected";
                    trayIcon.Icon = Resources.Disconnected;
                    m_iconUpdater.stopUpdate();
                    ClientProtocol.disconnect();
                }
            }  
        }

        public void onConnected(CMSConnectResponse response)
        {
            AppUtils.ckeckResponceResult(response.result);

            ConnListComboBox.Enabled = false;
            CMSSettings.isConnected = true;
        }

        public void onDisconnected(CMSDisconnectResponse response)
        {
            AppUtils.ckeckResponceResult(response.result);

            ConnListComboBox.Enabled = true;
            CMSSettings.isConnected = false;
        }

        private void InfoPictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.showAbout(this);
        }

        private void SettingsPictureBox_Click(object sender, EventArgs e)
        {
            // show SettingsForm
            this.TopMost = false;
            SettingsForm settings_frm = new SettingsForm();
            settings_frm.Owner = this; //Передаём вновь созданной форме её владельца.
            settings_frm.ShowDialog();
        }

        private void InfoPictureBox_MouseHover(object sender, EventArgs e)
        {
            InfoPictureBox.BackgroundImage = Resources.Connect_info_hover;
        }

        private void InfoPictureBox_MouseLeave(object sender, EventArgs e)
        {
            InfoPictureBox.BackgroundImage = Resources.Connect_info_leave;
        }

        private void SettingsPictureBox_MouseHover(object sender, EventArgs e)
        {
            SettingsPictureBox.BackgroundImage = Resources.Connect_settings_hover;
        }

        private void SettingsPictureBox_MouseLeave(object sender, EventArgs e)
        {
            SettingsPictureBox.BackgroundImage = Resources.Connect_settings_leave;
        }

        private void ClosePictureBox_MouseHover(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Connect_close_hover;
        }

        private void ClosePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Connect_close_leave;
        }

        private void MinimizePictureBox_MouseLeave(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Connect_minimize_leave;
        }

        private void MinimizePictureBox_MouseHover(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Connect_minimize_hover;
        }

        private void MinimizePictureBox_Click(object sender, EventArgs e)
        {
            ClosePictureBox_Click(sender, e);
        }

        private void ClosePictureBox_Click(object sender, EventArgs e)
        {
            if (!connect_form_hidden)
            {
                this.Hide();
                connect_form_hidden = true;
            }
        }

        private void ConnectButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            if (ConnectStatus.Text == "Not Connected")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_connect_leave;
            }
            else if (ConnectStatus.Text == "Waiting for request..." || ConnectStatus.Text == "Connecting...")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_cancel_leave;
            }
            else if (ConnectStatus.Text == "Connected")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_disconnect_leave;
            }
        }

        private void ConnectButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            if (ConnectStatus.Text == "Not Connected")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_connect_hover;
            }
            else if (ConnectStatus.Text == "Waiting for request..." || ConnectStatus.Text == "Connecting...")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_cancel_hover;
            }
            else if (ConnectStatus.Text == "Connected")
            {
                ConnectButtonPictureBox.BackgroundImage = Resources.Connect_button_disconnect_hover;
            }
        }

        private void LinkPictureBox_MouseLeave(object sender, EventArgs e)
        {
            LinkPictureBox.BackgroundImage = Resources.Link_be_leave;
        }

        private void LinkPictureBox_MouseHover(object sender, EventArgs e)
        {
            LinkPictureBox.BackgroundImage = Resources.Link_be_hover;
        }

        private void LinkPictureBox_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.becsys.ru/cms"); 
        }

        private void TreePictureBox_Click(object sender, EventArgs e)
        {
            Process.Start(Resources.LinkBecSys); 
        }

        private void connectBtnUpdateTimer_Tick(object sender, EventArgs e)
        {
            connectBtnUpdateTimer.Enabled = false;
            connect_button_locked = false;
            ConnectButtonPictureBox.UseWaitCursor = false;
            AppUtils.updateFormCursor();
        }

        public Control getControl()
        {
            return this;
        }

        public void setTrayIcon(Icon icon)
        {
            trayIcon.Icon = icon;
        }
    }
}