﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Diagnostics;
using cms_client_tray.Properties;
using cms_client_tray.utils; 

namespace cms_client_tray
{
    public partial class ProfileForm : Form
    {
        // current edited profile name
        private string m_currentProfileName = "";

        public ProfileForm()
        {
            InitializeComponent();
        }

        private void ProfileForm_Load(object sender, EventArgs e)
        {
            //place form on bottom-right
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width, workingArea.Bottom - Size.Height);

            m_currentProfileName = SettingsForm.EditProfile;
            if (String.IsNullOrEmpty(m_currentProfileName))
            {
                //Add new profile
                ProfileURL.Enabled = false;
                ProfileFrequency.Enabled = false;
            }
            else
            {                
                SettingsForm.EditProfile = "";
                ProfileConnectionName.Enabled = false;
                Connection currConnection = CMSSettings.connections[m_currentProfileName];
                ProfileConnectionName.Text = currConnection.name;
                ProfileManagerAddress.Text = currConnection.managerAddress;
                ProfileManagerTCPport.Text = Convert.ToString(currConnection.managerPort);
                ProfileManagerVPNAddress.Text = currConnection.managerVPNAddress;
                ProfileURL.Text = currConnection.requestURL;

                //RequestEnabled
                if (currConnection.requestEnabled)
                {
                    ProfileEstablishAfterRequest.Checked = true;
                    ProfileURL.Enabled = true;
                }

                //RequestFreq
                int requestFreq = currConnection.requestFrequency;
                if (requestFreq < 1)
                {
                    requestFreq = 1;
                }
                
                ProfileFrequency.Value = requestFreq;
            }

            //Lockdown or connected
            if (CMSSettings.isLockdown)
            {
                //Lock controls on Profile form
                ProfileManagerAddress.Enabled = false;
                ProfileManagerTCPport.Enabled = false;
                ProfileManagerVPNAddress.Enabled = false;
                ProfileEstablishAfterRequest.Enabled = false;
                ProfileURL.Enabled = false;
                ProfileFrequency.Enabled = false;
                ApplyButtonPictureBox.Enabled = false;
                ApplyButtonPictureBox.Visible = false;
            }

        }

        private void ProfileEstablishAfterRequest_CheckedChanged(object sender, EventArgs e)
        {
            bool profileUrlEnabled = ProfileEstablishAfterRequest.Checked; 
            ProfileURL.Enabled = profileUrlEnabled;
            ProfileFrequency.Enabled = profileUrlEnabled;
        }
        
        private void ProfileManagerAddress_Leave(object sender, EventArgs e)
        {
            checkAddressTextBox(ProfileManagerAddress);
        }

        private void ProfileManagerVPNAddress_Leave(object sender, EventArgs e)
        {
            checkAddressTextBox(ProfileManagerVPNAddress);
        }

        private void ProfileConnectionName_Leave(object sender, EventArgs e)
        {
            checkConnNameTextBox();
        }

        private void checkConnNameTextBox()
        {
            markControl(!String.IsNullOrEmpty(ProfileConnectionName.Text) &&
                            !isValidConnectionName(),
                        ProfileConnectionName);
        }

        private void markControl(bool errorCondition, TextBoxBase control)
        {
            if (errorCondition)
            {
                control.ForeColor = Color.Red;
                this.ActiveControl = control;
            }
            else
            {
                control.ForeColor = Color.Black;
            }
        }

        private void ProfileURL_Leave(object sender, EventArgs e)
        {
            markControl(!isValidRequestURL(), ProfileURL);
        }

        private void ApplyButtonPictureBox_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(m_currentProfileName))
            {
                addNewConnection();
            }
            else
            {
                editConnection();
            }
        }

        private void addNewConnection()
        {
            if (!isValidConnectionName() || 
                !ManagerAddressValidation() || 
                !isValidManagerVPNAddress() ||
                !isValidManagerTCPport() || 
                !isValidRequestURL())
            {
                MessageBox.Show(Resources.SetupFieldsMsg);
                return;
            }

            if (CMSSettings.connections.ContainsKey(ProfileConnectionName.Text))
            {
                MessageBox.Show(Resources.ConnectionExistsMsg);
                return;
            }

            if (MessageBox.Show(Resources.AddNewConnectionConfirm,
                Resources.ConnectionManager, 
                MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                return;
            }

            bool isRequestEnabled = ProfileEstablishAfterRequest.Checked;
            string requestUrl = isRequestEnabled ? ProfileURL.Text : "";
            int reqFrequency = isRequestEnabled ? Convert.ToInt32(ProfileFrequency.Value) : 0;
            Connection connection = new Connection(ProfileConnectionName.Text,
                    true,
                    isRequestEnabled,
                    requestUrl,
                    reqFrequency,
                    ProfileManagerAddress.Text,
                    Convert.ToInt32(ProfileManagerTCPport.Text),
                    ProfileManagerVPNAddress.Text,
                    CMSSettings.soapPort);

            CMSSettings.addConnection(connection);

            // Update DataGridView on Settings form
            SettingsForm settings_frm = (SettingsForm)this.Owner;
            settings_frm.UpdateDataGridView();

            //close Settings form
            this.Close();            
        }

        private void editConnection()
        {
            if (!isValidConnectionName() ||
                !ManagerAddressValidation() ||
                !isValidManagerTCPport() ||
                !isValidManagerVPNAddress())
            {
                MessageBox.Show(Resources.SetupFieldsMsg);
                return;
            }
            DialogResult dialogResult = MessageBox.Show(
                    CMSSettings.isConnected ?
                        Resources.ApplyNewSettingsDlg : Resources.ConfirmSettingsDialog,
                    Resources.ConnectionManager,
                    MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection updateConnection = CMSSettings.connections[m_currentProfileName];

                //fill local structure
                updateConnection.managerAddress = ProfileManagerAddress.Text;
                updateConnection.managerPort = Convert.ToInt32(ProfileManagerTCPport.Text);
                updateConnection.managerVPNAddress = ProfileManagerVPNAddress.Text;
                updateConnection.requestURL = ProfileURL.Text;
                updateConnection.requestFrequency = Convert.ToInt32(ProfileFrequency.Value);
                updateConnection.requestEnabled = ProfileEstablishAfterRequest.Checked;
                updateConnection.isConfigured = true;

                //update Connections from local structure
                CMSSettings.updateConnection(updateConnection);

                // Update DataGridView on Settings form
                SettingsForm settings_frm = (SettingsForm)this.Owner;
                settings_frm.UpdateDataGridView();

                //close Settings form
                this.Close();
            }
        }

        private void CancelButtonPictureBox_Click(object sender, EventArgs e)
        {
            //close Profile form
            this.Close();
        }

        private void ApplyButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            ApplyButtonPictureBox.BackgroundImage = Resources.Profile_button_apply_hover;
        }

        private void ApplyButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            ApplyButtonPictureBox.BackgroundImage = Resources.Profile_button_apply_leave;
        }

        private void CancelButtonPictureBox_MouseHover(object sender, EventArgs e)
        {
            CancelButtonPictureBox.BackgroundImage = Resources.Profile_button_cancel_hover;
        }

        private void CancelButtonPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CancelButtonPictureBox.BackgroundImage = Resources.Profile_button_cancel_leave;
        }

        private void MinimizePictureBox_MouseHover(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Settings_profile_minimize_hover;
        }

        private void MinimizePictureBox_MouseLeave(object sender, EventArgs e)
        {
            MinimizePictureBox.BackgroundImage = Resources.Settings_profile_minimize_leave;
        }

        private void ClosePictureBox_MouseHover(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Settings_profile_close_hover;
        }

        private void ClosePictureBox_MouseLeave(object sender, EventArgs e)
        {
            ClosePictureBox.BackgroundImage = Resources.Settings_profile_close_leave;
        }

        private void ClosePictureBox_Click(object sender, EventArgs e)
        {
            AppUtils.closeAllForms();
        }

        private void MinimizePictureBox_Click(object sender, EventArgs e)
        {
            //close Profile form
            this.Close();
        }

        private void becsysLinkBtn_Click(object sender, EventArgs e)
        {
            Process.Start(Resources.LinkBecSys); 
        }

        private bool isValidConnectionName()
        {
            return Regex.IsMatch(ProfileConnectionName.Text,
                    RegExpValidationPatterns.NameValidationPattern,
                    RegexOptions.IgnoreCase);
        }

        private bool ManagerAddressValidation()
        {
            return isValidDNS_orIP_Address(ProfileManagerAddress.Text);
        }

        private bool isValidManagerVPNAddress()
        {
            return isValidDNS_orIP_Address(ProfileManagerVPNAddress.Text);
        }

        private bool isValidRequestURL()
        {
            return Regex.IsMatch(ProfileURL.Text,
                RegExpValidationPatterns.URLValidationPattern,
                RegexOptions.IgnoreCase)
                ||
                String.IsNullOrEmpty(ProfileURL.Text) &&
                    !ProfileEstablishAfterRequest.Checked;
        }

        private bool isValidDNS_orIP_Address(string text)
        {
            return Regex.IsMatch(text,
                RegExpValidationPatterns.DNSAddressValidationPattern,
                RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace)
                ||
                Regex.IsMatch(text,
                RegExpValidationPatterns.IPAddressValidationPattern,
                RegexOptions.IgnoreCase);
        }

        private void ProfileManagerAddress_TextChanged(object sender, EventArgs e)
        {
            checkAddressTextBox(ProfileManagerAddress);
        }

        private void ProfileManagerVPNAddress_TextChanged(object sender, EventArgs e)
        {
            checkAddressTextBox(ProfileManagerVPNAddress);
        }

        private void checkAddressTextBox(TextBox control)
        {
            markControl(!String.IsNullOrEmpty(control.Text) &&
                            !isValidDNS_orIP_Address(control.Text),
                        control);
        }

        private void ProfileConnectionName_TextChanged(object sender, EventArgs e)
        {
            checkConnNameTextBox();
        }

        private void ProfileManagerTCPport_Leave(object sender, EventArgs e)
        {
            if (!isValidManagerTCPport())
            {
                this.ActiveControl = ProfileManagerTCPport;
            }
            markControl(!isValidManagerTCPport(),
                        ProfileManagerTCPport);
        }

        private bool isValidManagerTCPport()
        {
            //ManagerTCPport
            int tcp_port;
            bool result = Int32.TryParse(ProfileManagerTCPport.Text, out tcp_port);
            return result && tcp_port < 65536 && tcp_port > 0;
        }

        private bool isProfileChanged()
        {
            if (String.IsNullOrEmpty(m_currentProfileName)) {
                return false;
            }

            Connection updateConnection = CMSSettings.connections[m_currentProfileName];
            return updateConnection.managerAddress != ProfileManagerAddress.Text || 
                updateConnection.managerPort != Convert.ToInt32(ProfileManagerTCPport.Text) ||
                updateConnection.managerVPNAddress != ProfileManagerVPNAddress.Text ||
                updateConnection.requestURL != ProfileURL.Text ||
                updateConnection.requestFrequency != Convert.ToInt32(ProfileFrequency.Value) ||
                updateConnection.requestEnabled != ProfileEstablishAfterRequest.Checked;
        }
    }
}
