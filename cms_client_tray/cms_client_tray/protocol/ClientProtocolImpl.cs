﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using cms_transport;
using cms_client_tray.utils;

namespace cms_client_tray
{
    // system logic protocol interface & implmentation
    partial class ClientProtocol
    {
        private static ClientProtocolImpl m_protocol = new ClientProtocolImpl();

        class ClientProtocolImpl
        {
            private ClientPipe m_pipe = null;
            private JavaScriptSerializer m_serializer = null;
            private ISet<IProtocolListener> m_listeners = new HashSet<IProtocolListener>();
            private bool m_isRegistered = false;

            public ClientProtocolImpl()
            {
                m_pipe = new ClientPipe();
                m_serializer = new JavaScriptSerializer(new CustomResolver());
            }

            private void registerCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        string request = m_serializer.Serialize(new CMSRegisterRequest());
                        string responceStr = m_pipe.sendReceive(request);
                        CMSRegisterResponse response = m_serializer.Deserialize<CMSRegisterResponse>(responceStr);
                        CMSSettings.loadFromResponse(response);
                        ConnectForm.getForm().BeginInvoke(new MethodInvoker(delegate
                        {
                            ConnectForm.getForm().onRegister(response);
                        }));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
                getStatus();
            }

            public void register()
            {
                if (!m_isRegistered)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(registerCallback));
                    m_isRegistered = true;
                }
            }

            private void connectCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        CMSConnectRequest request = (CMSConnectRequest)threadContext;
                        string responce = m_pipe.sendReceive(m_serializer.Serialize(request));
                        CMSConnectResponse response = m_serializer.Deserialize<CMSConnectResponse>(responce);
                        ConnectForm.getForm().BeginInvoke(new MethodInvoker(delegate
                        {
                            ConnectForm.getForm().onConnected(response);
                        }));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
            }

            public void connect(string name, string status)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(connectCallback),
                    new CMSConnectRequest(name, status));
            }

            private void disconnectCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        string request = m_serializer.Serialize(new CMSDisconnectRequest());
                        string responce = m_pipe.sendReceive(request);
                        CMSDisconnectResponse response =
                            m_serializer.Deserialize<CMSDisconnectResponse>(responce);
                        ConnectForm.getForm().BeginInvoke(new MethodInvoker(delegate
                        {
                            ConnectForm.getForm().onDisconnected(response);
                        }));
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
            }

            public void disconnect()
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(disconnectCallback));
            }

            private void statusCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        string request = m_serializer.Serialize(new CMSStatusRequest());
                        string responce = m_pipe.sendReceive(request);
                        CMSStatusResponse response = m_serializer.Deserialize<CMSStatusResponse>(responce);
                        AppUtils.ckeckResponceResult(response.result);

                        foreach (IProtocolListener listener in m_listeners)
                        {
                            listener.getControl().BeginInvoke(new MethodInvoker(delegate
                            {
                                listener.onStatusReceived(response);
                            }));
                        }

                        // todo: check flags and ask settings $ profiles
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
                Thread.Sleep(1000);
                getStatus();
            }

            private void getStatus()
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(statusCallback));
            }

            private void updateProfilesCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        Profile[] profiles = (Profile[])threadContext;
                        CMSSetProfileRequest request = new CMSSetProfileRequest(profiles);
                        string requestStr = m_serializer.Serialize(request);
                        string responseStr = m_pipe.sendReceive(requestStr);
                        CMSSetProfileResponse response = m_serializer.Deserialize<CMSSetProfileResponse>(responseStr);
                        if (response.result != "OK")
                        {
                            MessageBox.Show("Error: Can't apply new settings", "BE CMS Endpoint Client");
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
            }

            public void updateProfiles(Profile[] profiles)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(updateProfilesCallback), profiles);
            }

            private void updateSettingsCallback(Object threadContext)
            {
                lock (m_pipe)
                {
                    try
                    {
                        cms_transport.Settings settings = (cms_transport.Settings)threadContext;
                        CMSSetGlobalSettingsRequest request = new CMSSetGlobalSettingsRequest(settings);
                        string requestStr = m_serializer.Serialize(request);
                        string responseStr = m_pipe.sendReceive(requestStr);
                        CMSSetGlobalSettingsResponse response = 
                            m_serializer.Deserialize<CMSSetGlobalSettingsResponse>(responseStr);
                        if (response.result != "OK")
                        {
                            MessageBox.Show("Error: Can't apply new settings", "BE CMS Endpoint Client");
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        AppUtils.exitOnConnectError();
                    }
                }
            }

            public void updateSettings(cms_transport.Settings settings)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(updateSettingsCallback), settings);
            }
                        
            // subscribe IProtocolListener to network events
            public void register(IProtocolListener listener)
            {
                m_listeners.Add(listener);
            }

            public void unregister(IProtocolListener listener)
            {
                m_listeners.Remove(listener);
            }
        }

        public static void updateProfiles()
        {
            Profile[] profiles = new Profile[CMSSettings.connections.Count];
            int index = 0;
            foreach (Connection connection in CMSSettings.connections.Values)
            {
                profiles[index++] = new Profile(connection.name,
                    Convert.ToString(connection.isConfigured),
                    Convert.ToString(connection.requestEnabled),
                    connection.requestURL,
                    Convert.ToString(connection.requestFrequency),
                    connection.managerAddress,
                    Convert.ToString(connection.managerPort),
                    connection.managerVPNAddress,
                    Convert.ToString(connection.managerSOAPPort));
            }

            m_protocol.updateProfiles(profiles);
        }

        public static void updateSettings()
        {
            cms_transport.Settings settings = new Settings();
            settings.querySetting = Convert.ToString(CMSSettings.isDisplayQueryWindow);
            settings.queryTimeout = Convert.ToString(CMSSettings.queryTimeout);
            settings.accept_reject_mesg = CMSSettings.queryMessage;
            settings.lockSetting = Convert.ToString(CMSSettings.isLockOnDisconnect);
            settings.connectPriority = Convert.ToString(CMSSettings.isAllowMultipleConnections);
            settings.fileTransferEnabled = Convert.ToString(CMSSettings.isEnableFileTransfer);
            settings.queryIfNoLogon = Convert.ToString(CMSSettings.isQueryIfNoLogon);
            settings.connectionTimeout = Convert.ToString(CMSSettings.connectionTimeout);
            settings.autostart = Convert.ToString(CMSSettings.isAutoStart);
            m_protocol.updateSettings(settings);
        }
    }
}