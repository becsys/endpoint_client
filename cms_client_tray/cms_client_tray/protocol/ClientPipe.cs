﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;
using System.Security.Principal;
using cms_transport;
using cms_client_tray.utils;
using System.Diagnostics;

namespace cms_client_tray
{
    class ClientPipe
    {
        //connection configuration
        private const string PIPE_NAME = "be_cms_client";
        private const int BUFFER_SIZE = 4096;

        private NamedPipeClientStream m_pipeClient = null;

        public ClientPipe()
        {
            try
            {
                m_pipeClient = new NamedPipeClientStream(".", 
                    PIPE_NAME, 
                    PipeDirection.InOut, 
                    PipeOptions.None, 
                    TokenImpersonationLevel.Impersonation);
                m_pipeClient.Connect(CMSSettings.serviceTimeout);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                AppUtils.exitOnConnectError();
            }
        }

        public string sendReceive(string msg)
        {
            try 
            { 
                byte[] data = System.Text.Encoding.UTF8.GetBytes(msg);
                m_pipeClient.Write(data, 0, data.Length);
                data = new byte[BUFFER_SIZE];
                m_pipeClient.Read(data, 0, BUFFER_SIZE);
                return System.Text.Encoding.UTF8.GetString(data).TrimEnd('\0');
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                AppUtils.exitOnConnectError();
                return null;
            }
        }
    }
}
