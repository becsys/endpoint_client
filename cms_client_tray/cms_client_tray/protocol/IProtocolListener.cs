﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cms_transport;
using System.Windows.Forms;

namespace cms_client_tray
{
    interface IProtocolListener
    {
        void onStatusReceived(CMSStatusResponse status);

        Control getControl();
    }
}
