﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cms_transport;

namespace cms_client_tray
{
    // user logic protocol interface
    partial class ClientProtocol
    {
        public static void register()
        {
            m_protocol.register();
        }

        public static void connect(string name, string status)
        {
            m_protocol.connect(name, status);
        }

        public static void disconnect()
        {
            m_protocol.disconnect();
        }

        public static void register(IProtocolListener listener)
        {
            m_protocol.register(listener);
        }

        public static void unregister(IProtocolListener listener)
        {
            m_protocol.unregister(listener);
        }
    }
}
