﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceProcess;

namespace cms_client_service
{
    namespace CMSData
    {
        class Protocol
        {
            CMSService service;

            public Protocol(CMSService iservice)
            {
                service = iservice;
            }

            public void start()
            {
                Transport transport = new Transport();
                transport.subscribe("REGISTER", Register);
                transport.subscribe("UNREGISTER", Unregister);
                transport.subscribe("CONNECT", Connect);
                transport.subscribe("DISCONNECT", Disconnect);
                transport.subscribe("GET_VPN_STATUS", Get_vpn_status);
                transport.subscribe("GET_CLIENT_STATUS", Get_client_status);
                transport.subscribe("UNKNOWN", Unknown);
                transport.start();
            }

            //Метод Register - позволяет клиенту зарегистрироваться на сервисе и получить информацию о текущем статусе. Получив данные, клиент правильно визуализирует изменения
            public void Register(ref Message client_msg)
            {
                CMSLogger.log(service.ServiceName + "::REGISTER" + Environment.NewLine);
                CMSLogger.debug("Message: REGISTER");
                
                service.CONNECTION_NAME = client_msg.StrAttribute1;

                CMSLogger.debug("service CONNECTION_NAME: " + service.CONNECTION_NAME);
                client_msg.Command = "NONE";
                //client_msg.StrAttribute1 = service.CONNECTION_NAME;
                //client_msg.StrAttribute2 = service.CONNECTION_STATUS;
                client_msg.Result = "OK";
            }

            //Метод Unregister - завершение работы клиента
            public void Unregister(ref Message client_msg)
            {
                CMSLogger.debug("Protocol: UNREGISTER");
                CMSLogger.log(service.ServiceName + "::UNREGISTER" + Environment.NewLine);

                client_msg.Command = "NONE";
                client_msg.Result = "OK";
            }

            //Метод Connect - используется для передачи сервису параметров (PROFILE_NAME, REQUEST CONNECTED) при нажатии кнопки CONNECT
            //o Проверка RequestEnabled флага (Waiting for Request), если YES, то: выставляем статус WAITING_REQUEST и режим BY_REQUEST
            //o Проверка RequestEnabled флага (Waiting for Request), если NO, то: выставляем режим MANUAL
            //o Остановка сервисов OpenVPN/UltraVNC (в текущей версии только OpenVPN)
            //o Очистка глобальных переменных статус, режим и имя профиля
            public void Connect(ref Message client_msg)
            {
                CMSLogger.log(Program.programName + "::CONNECT" + Environment.NewLine);
                CMSLogger.debug("Message: CONNECT");
                CMSLogger.debug("CONNECTION_NAME: " + client_msg.StrAttribute1);

                //получим имя профиля по которому идет подключение
                service.CONNECTION_NAME = client_msg.StrAttribute1;

                //в зависимости от настроек профиля установим режим работы (ручной/удаленный)
                string tmp_status;
                if (service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestEnabled") == "false")
                {
                    service.CONNECTION_CONFIG = "MANUAL";
                    tmp_status = "CONNECTING";
                }
                else
                {
                    service.CONNECTION_CONFIG = "BY_REQUEST";
                    tmp_status = "WAITING_REQUEST";
                }
                client_msg.Command = "NONE";
                client_msg.StrAttribute1 = service.CONNECTION_NAME;
                client_msg.StrAttribute2 = tmp_status;
                client_msg.Result = "OK";
            }

            //Метод Disconnect - при нажатии кнопки DISCONNECT
            public void Disconnect(ref Message client_msg)
            {
                CMSLogger.log(Program.programName + "::DISCONNECT" + Environment.NewLine);
                CMSLogger.debug("Message: DISCONNECT");

                lock (service.lockThis)
                {
                    service.CONNECTION_STATUS = "DISCONNECTING";
                }
                    

                client_msg.Command = "NONE";
                client_msg.Result = "OK";
            }

            //Метод GetVPNStatus - запрос состояния впн-соединения
            //o отправка имени профиля и статуса, если статус CONNECTED то отсылается время активности соединения
            public void Get_vpn_status(ref Message client_msg)
            {
                CMSLogger.debug("Message: GET_VPN_STATUS");

                string tmp_status;

                if ((service.CONNECTION_CONFIG != "") &&
                    (service.CONNECTION_STATUS == ""))
                {
                    tmp_status = "CONNECTING";
                }
                else{
                    tmp_status = service.CONNECTION_STATUS;
                }
                client_msg.StrAttribute1 = service.CONNECTION_NAME;
                client_msg.StrAttribute2 = tmp_status;                

                if(service.CONNECTION_STATUS == "CONNECTED")
                {
                    TimeSpan ts = service.getConnectionTimerTime();
                    client_msg.StrAttribute3 = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
                    CMSLogger.debug("VPN timer: " + client_msg.StrAttribute3);
                }

                client_msg.Command = "NONE";
                client_msg.Result = "OK";
            }

            //Метод GetClientStatus - запрос детальной информации (в момент захода на вкладку Status). Клиент получает информацию: system name, custom name, os version, sn, 
            //connection status. Проверка состояния TAP-интерфейса:
            //o в случае UP – передаем tunnel ip, send/received bytes, время (час, мин)
            //o в случае DOWN – все переменные в N/A
            public void Get_client_status(ref Message client_msg)
            {
                CMSLogger.debug("Message: GET_CLIENT_STATUS");

                client_msg.Command = "NONE";
                client_msg.StrAttribute1 = service.information_module.CLIENT_SYSTEM_NAME;
                client_msg.StrAttribute2 = service.information_module.CLIENT_CUSTOM_NAME;
                client_msg.StrAttribute3 = service.information_module.CLIENT_OS_VERSION;
                client_msg.StrAttribute4 = service.information_module.CLIENT_SERIAL_NUMBER_WITH_BE;
                client_msg.StrAttribute5 = service.CONNECTION_STATUS;
                
                if (service.network_module.status() == "Up")
                {
                    CMSLogger.debug("TAP-interface: Up");

                    TimeSpan ts = service.getConnectionTimerTime();
                    client_msg.StrAttribute6 = String.Format("{0}", service.network_module.receivedInBytes); 
                    client_msg.StrAttribute7 = String.Format("{0}", service.network_module.sentInBytes);
                    client_msg.StrAttribute8 = service.network_module.ipAddress;
                    client_msg.StrAttribute9 = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
                }
                else
                {
                    CMSLogger.debug("TAP-interface: Down");
                    client_msg.StrAttribute6 = "N/A";
                    client_msg.StrAttribute7 = "N/A";
                    client_msg.StrAttribute8 = "N/A";
                    client_msg.StrAttribute9 = "N/A";
                }
                
                client_msg.Result = "OK";
            }

            //Метод Unknown - реакция на неизвестные комманды
            public void Unknown(ref Message msg)
            {
                CMSLogger.debug("Message: UNKNOWN");

                msg.Command = "None";
                msg.Result = "OK";
            }
        }
    }
}






