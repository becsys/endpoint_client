﻿using System;
using System.Runtime.InteropServices;

namespace cms_client_service
{
    namespace CMSData
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
        public struct Message
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string Command;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string Result;

            //string attributes
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string StrAttribute3;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute4;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute5;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute6;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute7;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute8;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute9;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string StrAttribute10;


            // Member function for serialization.
            public byte[] Serialize()
            {
                // Get the byte size of a MESSAGE structure if it is to be
                // marshaled to unmanaged memory.
                Int32 iSizeOMyDataStruct = Marshal.SizeOf(typeof(Message));
                // Allocate a byte array to contain the bytes of the unmanaged version
                // of the MESSAGE structure.
                byte[] byteArrayMyDataStruct = new byte[iSizeOMyDataStruct];
                // Allocate a GCHandle to pin the byteArrayMESSAGE array
                // in memory in order to obtain its pointer.
                GCHandle gch = GCHandle.Alloc(byteArrayMyDataStruct, GCHandleType.Pinned);
                // Obtain a pointer to the byteArrayMESSAGE array in memory.
                IntPtr pbyteArrayMyDataStruct = gch.AddrOfPinnedObject();
                // Copy all bytes from the managed MESSAGE structure into
                // the byte array.
                Marshal.StructureToPtr(this, pbyteArrayMyDataStruct, false);
                // Unpin the byteArrayMESSAGE array in memory.
                gch.Free();
                // Return the byte array.
                // It contains the serialized bytes of the MESSAGE structure.
                return byteArrayMyDataStruct;
            }

            // Member function for deserialization.
            public void Deserialize(ref byte[] byteSerializedData)
            {
                // Allocate a GCHandle of the Pinned Type
                // for the byteSerializedData byte array.
                // This is possible for a byte array
                // because it is a blittable type.
                GCHandle gch = GCHandle.Alloc(byteSerializedData, GCHandleType.Pinned);
                // Get a pointer to the byteSerializedData array.
                IntPtr pbyteSerializedData = gch.AddrOfPinnedObject();
                // Convert the array data of byteSerializedData
                // directly into a MESSAGE structure.
                // The interop marshaler will use the MarshalAsAttribute
                // of the fields of the MESSAGE structure to
                // perform field data conversions.
                this = (Message)Marshal.PtrToStructure(pbyteSerializedData, typeof(Message));
                // Free the GCHandle.
                gch.Free();
            }

            public static byte[] SerializeMessage(Message my_data_struct)
            {
                // Serialize the MESSAGE structure into an array
                // of bytes.
                byte[] byArraySerializedData = my_data_struct.Serialize();

                return byArraySerializedData;
            }

            public static Message DeserializeMessage(byte[] byArraySerializedData)
            {
                // Deserialize the contents of byArraySerializedData into
                // a MESSAGE structure.
                Message result = new Message();
                result.Deserialize(ref byArraySerializedData);
                return result;
            }

        }
    }
}
