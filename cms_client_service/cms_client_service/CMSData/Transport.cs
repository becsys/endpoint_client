﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;

namespace cms_client_service
{
    namespace CMSData
    {
        class Transport
        {
            private static string PIPE_NAME = "be_cms_client";
            private static int BUFFER_SIZE = 1024;

            protected NamedPipeServerStream m_ui_pipe;
            public delegate void Callfunc(ref Message msg);
            public SortedList<string, Callfunc> func_list = new SortedList<string, Callfunc>();
            public PipeSecurity sec;

            public Transport()
            {
                try
                {
                    setPipeSecurity();
                    initPipe();
                }
                catch
                {
                    if (Program.DEBUG)
                        CMSLogger.debug("Transport: initPipe() ERROR");
                }

            }

			//Создадим правила безопасности бля пользования пайпом
            public void setPipeSecurity()
            {
                System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.BuiltinUsersSid, null);
                PipeAccessRule rule = new PipeAccessRule(sid, PipeAccessRights.ReadWrite, System.Security.AccessControl.AccessControlType.Allow);
                sec = new PipeSecurity();
                sec.AddAccessRule(rule);
            }

            public void initPipe()
            {
                if (Program.DEBUG)
                    CMSLogger.debug("Transport: initPipe()");

                //проверим и закроем pipe
                if (m_ui_pipe != null)
                {
                    if (Program.DEBUG)
                        CMSLogger.debug("pipe Disconnect()");

                    //kill original sever
                    m_ui_pipe.Disconnect();
                    m_ui_pipe.Close();
                }

                // создадим новый pipe
                m_ui_pipe = new NamedPipeServerStream(PIPE_NAME, PipeDirection.InOut, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 0, 0, sec);
            }

            //Регистрируем пользовательскую функцию и событие
            public void subscribe(string myevent, Callfunc myfunc)
            {
                func_list.Add(myevent, myfunc);
            }

            //вызовем связанную с событием key зарегистрированную функцию, в качестве параметра функции передасться msg
            public void process(string key, ref Message msg)
            {
                if (func_list.ContainsKey(key))
                {
                    func_list[key](ref msg);
                    SendMessage(m_ui_pipe, msg);                   
                }
                else
                {
                    //для обработки неизвестного ключа попробуем использовать зарезервированную команду "UNKNOWN"
                    if (func_list.ContainsKey("UNKNOWN"))
                    {
                        SendMessage(m_ui_pipe, msg);
                    }
                }
            }

            //Асинхронно запустим пайп на ожидание данных. Обработка будет вестись в отдельном потоке.
            public void start()
            {
                if (Program.DEBUG)
                    CMSLogger.debug("Transport: start() New Pipe Created");

				//Асинхронный эквисалент WaitForConnection
                m_ui_pipe.BeginWaitForConnection(new AsyncCallback(WaitForConnectionCallBack), m_ui_pipe);
            }

            public static Message ReceiveMessage(NamedPipeServerStream pipeClient, Message server_msg)
            {
                int received_butes = 0;

                byte[] messageFromServer = new byte[BUFFER_SIZE];
                received_butes = pipeClient.Read(messageFromServer, 0, BUFFER_SIZE);

                //deserialize message
                server_msg = Message.DeserializeMessage(messageFromServer);

                return server_msg;
            }

            public static void SendMessage(NamedPipeServerStream pipeClient, Message server_msg)
            {
                //serialize message
                byte[] SerializedMessage = Message.SerializeMessage(server_msg);

                //send message to client
                pipeClient.Write(SerializedMessage, 0, SerializedMessage.Length);
            }

            //обработка данных пришедшых в пайп
            private void WaitForConnectionCallBack(IAsyncResult iar)
            {
                if (Program.DEBUG)
                    CMSLogger.debug("Transport: WaitForConnectionCallBack()");

                NamedPipeServerStream m_ui_pipe = (NamedPipeServerStream)iar.AsyncState;
                m_ui_pipe.EndWaitForConnection(iar);

                try
                {
                    Message client_msg = new Message();
                    //receive client message
                    client_msg = ReceiveMessage(m_ui_pipe, client_msg);
                    process(client_msg.Command, ref client_msg);
                }
                catch
                {
                    if (Program.DEBUG)
                        CMSLogger.debug("WaitConnectionCallBack() Process data ERROR");
                }

                try
                {
                    initPipe();
                }
                catch
                {
                    if (Program.DEBUG)
                        CMSLogger.debug("WaitConnectionCallBack() initPipe() ERROR");
                }

				start();
                if (Program.DEBUG)
                    CMSLogger.debug("WaitConnectionCallBack() Exit");
            }
        }
    }
}
