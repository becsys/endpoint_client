﻿using System;
using System.ServiceProcess;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO.Pipes;
using Microsoft.Win32;
using System.Management;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace cms_client_service
{
    public class CMSServiceState
    {
        public string state;
        public CMSService service;

        public static SortedList<string, string> WebServiceConfig;
        private static int request_xml_interval = 0;
        private static int publish_xml_interval = 0;
        public SortedList<string, string> vnc_info;
        public delegate void Callfunc();
        public SortedList<string, Callfunc> func_list = new SortedList<string, Callfunc>();
        public bool publish_first_run = false;
        public bool startConnecting = false;
        Int32 unixTimestampNow;
        Int32 unixTimestampStart;

        public CMSServiceState(CMSService iservice)
        {
            service = iservice;

            func_list.Add("idle", idle);
            func_list.Add("start manual", startManual);
            func_list.Add("start manual with timeout", startManualWithTimeout);
            func_list.Add("init remote", initRemote);
            func_list.Add("init remote with timeout", initRemoteWithTimeout);
            func_list.Add("start remote", startRemote);
            func_list.Add("start remote with timeout", startRemoteWithTimeout);
            func_list.Add("connecting manual", connectingManual);
            func_list.Add("connecting manual with timeout", connectingManualWithTimeout);
            func_list.Add("waiting remote", waitingRemote);
            func_list.Add("waiting remote with timeout", waitingRemoteWithTimeout);
            func_list.Add("connecting remote", connectingRemote);
            func_list.Add("connecting remote with timeout", connectingRemoteWithTimeout);
            func_list.Add("process remote", processRemote);
            func_list.Add("process manual", processManual);
            func_list.Add("disconnecting", disconnecting);
            func_list.Add("shutdown", shutdown);
        }

        public void process()
        {
            func_list[state]();
        }

        //получение xml данных с определенным интервалом
        public bool get_xml_data(ref string connect)
        {
            int RequestFreq = service.receiveWeb_module.getRequestFrequency();
            request_xml_interval += service.m_delay.Seconds;

            if (request_xml_interval >= (RequestFreq))
            {
                CMSLogger.debug("Execute: get_xml_data. Date: " + DateTime.Now.ToString("h:mm:ss tt"));

                request_xml_interval = 0;
                try
                {
                    WebServiceConfig = service.receiveWeb_module.getWebServiceConfig(service.information_module.CLIENT_SERIAL_NUMBER);
                    connect = WebServiceConfig["Connect"];
                    return true;
                }
                catch
                {
                    CMSLogger.log(Program.programName + "::WEBREQUEST_ERROR" + Environment.NewLine);
                    CMSLogger.debug("Execute: get_xml_data. WEBREQUEST_ERROR");
                    return false;
                }
            }
            return false;
        }

        //отправка xml данных с определенным интервалом
        public void send_xml_data()
        {
            int delay = service.publishWeb_module.getDelay();
            int interval = service.publishWeb_module.getInterval();

            publish_xml_interval += service.m_delay.Seconds;

            if ((publish_xml_interval >= interval) || ((publish_xml_interval == delay) && publish_first_run))
            {
                CMSLogger.debug("Execute: send_xml_data. Date: " + DateTime.Now.ToString("h:mm:ss tt"));

                publish_xml_interval = 0;
                try
                {
                    string vpn_ip_address;

                    if (service.network_module.status() == "Up")
                    {
                        vpn_ip_address = service.network_module.ipAddress;
                    }
                    else
                    {
                        vpn_ip_address = "N/A";
                    }
                    try
                    {
                        CMSLogger.debug("PublishWeb");
                        //задача 1.10 - добавление доп. информации в Oracle
                        SortedList<string, string> info = new SortedList<string, string>();
                        info.Add("Serial Number", service.information_module.CLIENT_SERIAL_NUMBER);
                        info.Add("SoftwareVersion", Program.SOFTWARE_NUMBER);
                        info.Add("AutoStart", service.cfg.getApplicationSettingAsBool("AutoStart"));
                        info.Add("Lockdown", service.cfg.getApplicationSettingAsBool("Lockdown"));
                        info.Add("OSversion", service.information_module.CLIENT_OS_VERSION);
                        info.Add("SystemName", service.information_module.CLIENT_SYSTEM_NAME);
                        info.Add("CustomName", service.information_module.CLIENT_CUSTOM_NAME);
                        info.Add("ManagerAddress", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerAddress"));
                        info.Add("ManagerVPNAddress", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerVPNAddress"));
                        info.Add("ManagerPort", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerSOAPPort"));
                        info.Add("RequestURL", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestURL"));
                        info.Add("RequestInterval", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestFrequency"));
                        info.Add("ClientVPNAddress", vpn_ip_address);
                        info.Add("ClientPhysicalAddress", service.network_module.getPhysicalAddress());
                        info.Add("Display_Query_Window", service.vnc_module.vnc_info["QuerySetting"]);
                        info.Add("Query_if_no_logon", service.vnc_module.vnc_info["QueryIfNoLogon"]);
                        info.Add("Query timeout", service.vnc_module.vnc_info["QueryTimeout"]);
                        info.Add("Enable_file_transfer", service.vnc_module.vnc_info["accept_reject_mesg"]);
                        info.Add("Allow_multiple_connections", service.vnc_module.vnc_info["ConnectPriority"]);
                        info.Add("Lock_on_disconnect", service.vnc_module.vnc_info["LockSetting"]);
                        info.Add("FileTransferEnabled", service.vnc_module.vnc_info["FileTransferEnabled"]);
                        info.Add("RequestEnabled", service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestEnabled"));

                        service.publishWeb_module.postXMLData(info);
                    }
                    catch (Exception e)
                    {
                        CMSLogger.debug("PublishWeb: set data ERROR" + e.ToString());
                    }

                }
                catch
                {
                    //
                }
            }
        }

        // SERVICE STATES

        public void idle()
        {
            //TODO: проверять наличие изменений в конфигах VPN и VNC и откатывать их
        }

        public void startManual()
        {
                //update client.ovpn config file
                service.vpn_module.update_config(service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerAddress"),
                                                 service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerPort"));

                //set SOAP_URL
                service.publishWeb_module.SOAP_URL = "http://" + 
                                                     service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerVPNAddress") + 
                                                     ":" +
                                                     service.cfg.getApplicationSetting("SOAPPort") + 
                                                     service.cfg.getApplicationSetting("SOAPResource");

                //set publish_first_run
                publish_first_run = true;

                //Перезапустим службы
                if (service.network_module.isEnabled())
                {
                    service.vpn_module.restart();
                }
                else
                {
                    CMSLogger.debug("TAP-интерфейс отключен");
                    return;
                }

                updateVncConfig();
                service.vnc_module.restart();
                service.startConnectionTimer();

                string last_reason = service.CONNECTION_REASON;
                service.CONNECTION_STATUS = "CONNECTING";
                service.CONNECTION_REASON = last_reason;
            }

        private void updateVncConfig()
        {
            service.vnc_module.vnc_info["ManagerVPNAddress"] = service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerVPNAddress");
            service.vnc_module.vnc_info["QuerySetting"] = service.cfg.getApplicationSetting("QuerySetting");
            service.vnc_module.vnc_info["QueryTimeout"] = service.cfg.getApplicationSetting("QueryTimeout");
            service.vnc_module.vnc_info["accept_reject_mesg"] = service.cfg.getApplicationSetting("accept_reject_mesg");
            service.vnc_module.vnc_info["LockSetting"] = service.cfg.getApplicationSetting("LockSetting");
            service.vnc_module.vnc_info["ConnectPriority"] = service.cfg.getApplicationSetting("ConnectPriority");
            service.vnc_module.vnc_info["FileTransferEnabled"] = service.cfg.getApplicationSetting("FileTransferEnabled");
            service.vnc_module.vnc_info["QueryIfNoLogon"] = service.cfg.getApplicationSetting("QueryIfNoLogon");
            service.vnc_module.updateConfig();
            System.IO.FileInfo file = new System.IO.FileInfo(service.vnc_module.VNC_CONFIG_FILE);
            service.lastWriteTime = file.LastWriteTime;
        }

        public void startManualWithTimeout()
        {
            CMSLogger.debug("Process: startManualWithTimeout()");
            unixTimestampNow = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            int timeout = Convert.ToInt32(service.cfg.getApplicationSetting("ConnectionTimeout"));
            if (!startConnecting)
            {
                unixTimestampStart = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                startConnecting = true;
            }
            if (unixTimestampNow - unixTimestampStart >= timeout)
            {
                disconnect();
                service.CONNECTION_STATUS = "";
                service.CONNECTION_REASON = "TIMEOUT";
                startConnecting = false;
                return;
            }

            if (!service.network_module.isEnabled())
            {
                CMSLogger.debug("TAP-интерфейс отключен");
                return;
            }

            startManual();

            string last_reason = service.CONNECTION_REASON;
            service.CONNECTION_STATUS = "CONNECTING_WITH_TIMEOUT";
            service.CONNECTION_REASON = last_reason;
        }
        
        public void initRemote()
        {
            service.receiveWeb_module.REQUEST_URL = service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestURL");
            service.receiveWeb_module.setRequestFrequency(service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestFrequency"));

            service.CONNECTION_STATUS = "WAITING_REQUEST";
        }

        public void initRemoteWithTimeout()
        {
            initRemote();
            service.CONNECTION_STATUS = "WAITING_REQUEST_WITH_TIMEOUT";
        }

        public void startRemote()
        {
            startManual();
        }
        
        public void startRemoteWithTimeout()
        {
            startManualWithTimeout();
        }

        public void connectingManualWithTimeout()
        {
            CMSLogger.debug("Process: connectingManualWithTimeout()");
            unixTimestampNow = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            int timeout = Convert.ToInt32(service.cfg.getApplicationSetting("ConnectionTimeout"));                       

            if (unixTimestampNow - unixTimestampStart >= timeout)
            {
                disconnect();
                service.CONNECTION_STATUS = "";
                service.CONNECTION_REASON = "TIMEOUT";
                startConnecting = true;
                return;
            }

            connectingManual();
            if (service.CONNECTION_STATUS == "CONNECTED")
            {
                startConnecting = false;
            }
        }

        public void connectingManual()
        {
            CMSLogger.debug("Process: connectingManual()");
            if (service.network_module.status() == "Up")
            {
                CMSLogger.debug("TAP-interface: Up");
                if (!service.vnc_module.isStarted())
                {
                    service.vnc_module.start();
                }
                service.CONNECTION_STATUS = "CONNECTED";
                service.CONNECTION_REASON = "";
            }
            else
            {
                CMSLogger.debug("TAP-interface: Down");
            }
        }

        private bool isVncConfigChanged()
        {
            System.IO.FileInfo file = new System.IO.FileInfo(service.vnc_module.VNC_CONFIG_FILE);
            return (file.LastWriteTime == service.lastWriteTime) ? false : true;
        }
        
        public void processManual()
        {
            if (service.vncConfigChanged)
            {
                updateVncConfig();
                service.vnc_module.restart();
                service.vncConfigChanged = false;
                
            }
            if (isVncConfigChanged())
            {
                updateVncConfig();
                System.IO.FileInfo file = new System.IO.FileInfo(service.vnc_module.VNC_CONFIG_FILE);
                service.lastWriteTime = file.LastWriteTime;
            }


            send_xml_data();

            if (service.network_module.status() == "Down")
            {
                CMSLogger.debug("TAP-interface Down. Restart VPN");
                disconnect();
                if (service.cfg.getApplicationSetting("AutoStart") == "true")
                {
                    service.CONNECTION_STATUS = "START_CONNECTING";
                }
                else
                {
                    service.CONNECTION_STATUS = "START_CONNECTING_WITH_TIMEOUT";
                }
                service.CONNECTION_REASON = "NETWORKDOWN";
            }


        }

        public void waitingRemote()
        {
            string connect = "";
            if (get_xml_data(ref connect))
            {
                if (connect == "Yes")
                {
                    service.CONNECTION_STATUS = "START_CONNECTING";
                }
            }
        }

        public void waitingRemoteWithTimeout()
        {
            string connect = "";
            if (get_xml_data(ref connect))
            {
                if (connect == "Yes")
                {
                    service.CONNECTION_STATUS = "START_CONNECTING_WITH_TIMEOUT";
                }
            }
        }
          
        public void connectingRemoteWithTimeout()
        {
           connectingManualWithTimeout();
        }

        public void connectingRemote()
        {
            CMSLogger.debug("Process: connectingRemote()");
            connectingManual();
        }

        public void processRemote()
        {
            CMSLogger.debug("Process: processRemote()");
            processManual();

            string connect = "";
            if (get_xml_data(ref connect))
            {
                if (connect == "No")
                {
                    disconnect();
                    if (service.cfg.getApplicationSetting("AutoStart") == "true")
                    {
                        service.CONNECTION_STATUS = "WAITING_REQUEST";
                    }
                    else
                    {
                        service.CONNECTION_STATUS = "WAITING_REQUEST_WITH_TIMEOUT";
                    }
                    
                }
            }
        }
    
        public void disconnect()
        {
            CMSLogger.debug("Process: disconnect()");
            service.CONNECTION_REASON = "";
            if (service.vpn_module.isRunning())
            {
                service.vpn_module.stop();
            }
            if (service.vnc_module.isRunning())
            {
                service.vnc_module.stop();
            }
            publish_first_run = false;
            service.stopConnectionTimer();
        }

        public void disconnecting()
        {
            CMSLogger.debug("Process: disconnecting()");
            service.CONNECTION_STATUS = "";
            service.CONNECTION_CONFIG = "";
            service.CONNECTION_REASON = "";
            disconnect();
            //TODO: сбросить конфиги VPN и VNC в федолтное состояние
        }

        public void shutdown()
        {
            if (service.vpn_module.isRunning())
            {
                service.vpn_module.stop();
            }

            if (service.vnc_module.isRunning())
            {
                service.vnc_module.stop();
            }

            service.threadProcessFlag = false;
            //TODO: сбросить конфиги VPN и VNC в федолтное состояние
        }
    }
}
