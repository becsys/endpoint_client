﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Text;
using System.Web.Script.Serialization;
using cms_transport;

namespace cms_client_service
{
    namespace CMSJsonData
    {
        class Transport
        {
            private static string PIPE_NAME = "be_cms_client";
            private static int BUFFER_SIZE = 4096;

            protected NamedPipeServerStream m_ui_pipe;
            public delegate void Callfunc(object msg);
            public SortedList<string, Callfunc> func_list = new SortedList<string, Callfunc>();
            public PipeSecurity sec;

            public Transport()
            {
                try
                {
                    setPipeSecurity();
                    initPipe();
                }
                catch
                {
                    CMSLogger.debug("Transport: initPipe() ERROR");
                }
            }

			//Создадим правила безопасности бля пользования пайпом
            public void setPipeSecurity()
            {
                System.Security.Principal.SecurityIdentifier sid = new System.Security.Principal.SecurityIdentifier(System.Security.Principal.WellKnownSidType.BuiltinUsersSid, null);
                PipeAccessRule rule = new PipeAccessRule(sid, PipeAccessRights.ReadWrite, System.Security.AccessControl.AccessControlType.Allow);
                sec = new PipeSecurity();
                sec.AddAccessRule(rule);
            }

            public void initPipe()
            {
                if (Program.DEBUG)
                    CMSLogger.debug("Transport: initPipe()");

                //проверим и закроем pipe
                if (m_ui_pipe != null)
                {
                    CMSLogger.debug("pipe Disconnect()");

                    //kill original sever
                    m_ui_pipe.Disconnect();
                    m_ui_pipe.Close();
                }

                // создадим новый pipe
                m_ui_pipe = new NamedPipeServerStream(PIPE_NAME, PipeDirection.InOut, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous, 0, 0, sec);
            }

            //Регистрируем пользовательскую функцию и событие
            public void subscribe(string msgType, Callfunc myfunc)
            {
                func_list.Add(msgType, myfunc);
            }

            //вызовем связанную с событием key зарегистрированную функцию, в качестве параметра функции передасться msg
            public void process(string msg)
            {
                CMSLogger.debug("================== MESSAGE =================="); 
                CMSLogger.debug("RECEIVE message: " + msg); 
                var serializer = new JavaScriptSerializer(new CustomResolver());
                dynamic value = null;
                string clientMsgType = null;
                foreach (string message in func_list.Keys)
                {
                    try
                    {
                        value = serializer.Deserialize(msg, Type.GetType(message));
                        clientMsgType = message;
                        break;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.ToString());
                    }
                }
                if (clientMsgType != null)
                {
                    CMSLogger.debug("Process Message: " + clientMsgType); 
                    func_list[clientMsgType](value);
                }
                else
                {
                    CMSLogger.debug("ERROR: получено неизвестное сообщение: " + msg);
                    throw new Exception("получено неизвестное сообщение");
                }
                CMSLogger.debug("=============================================");
                // dynamic value = serializer.Deserialize(msg, message.BaseType)

                //CMSRegisterRequest value = serializer.Deserialize<CMSRegisterRequest>(msg);
                //msg = serializer.Serialize(value);
                //SendMessage(m_ui_pipe, msg);   
            }

            //Асинхронно запустим пайп на ожидание данных. Обработка будет вестись в отдельном потоке.
            public void start()
            {
                CMSLogger.debug("Transport: start() New Pipe Created");

				//Асинхронный эквисалент WaitForConnection
                m_ui_pipe.BeginWaitForConnection(new AsyncCallback(WaitForConnectionCallBack), m_ui_pipe);
            }

            public static string ReceiveMessage(NamedPipeServerStream pipeClient)
            {
                int received_butes = 0;

                byte[] messageFromServer = new byte[BUFFER_SIZE];
                received_butes = pipeClient.Read(messageFromServer, 0, BUFFER_SIZE);
                return  System.Text.Encoding.UTF8.GetString(messageFromServer).TrimEnd('\0');
            }

            public void send(string message)
            {
                SendMessage(m_ui_pipe, message);
            }

            public static void SendMessage(NamedPipeServerStream pipeClient, string server_msg)
            {
                //serialize message
                byte[] message = Encoding.UTF8.GetBytes(server_msg);

                //send message to client
                pipeClient.Write(message, 0, message.Length);
            }

            //обработка данных пришедшых в пайп
            private void WaitForConnectionCallBack(IAsyncResult iar)
            {
                CMSLogger.debug("Transport: WaitForConnectionCallBack()");

                NamedPipeServerStream m_ui_pipe = (NamedPipeServerStream)iar.AsyncState;
                m_ui_pipe.EndWaitForConnection(iar);

                try
                {
                    string client_msg;
                    while (true)
                    {
                        client_msg = ReceiveMessage(m_ui_pipe);
                        process(client_msg);
                    }
                }
                catch (Exception ex)
                {
                    CMSLogger.debug("WaitConnectionCallBack() Process data ERROR");
                    CMSLogger.debug(ex.ToString());
                }

                try
                {
                    initPipe();
                }
                catch
                {
                    CMSLogger.debug("WaitConnectionCallBack() initPipe() ERROR");
                }

				start();
                CMSLogger.debug("WaitConnectionCallBack() Exit");
            }
        }
    }
}
