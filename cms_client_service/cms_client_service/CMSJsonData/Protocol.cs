﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceProcess;
using cms_transport;
using System.Web.Script.Serialization;
using System.Configuration;

namespace cms_client_service
{
    namespace CMSJsonData
    {
        class Protocol
        {
            CMSService service;
            Transport transport;

            public Protocol(CMSService iservice)
            {
                service = iservice;
            }

            public void start()
            {
                transport = new Transport();
                transport.subscribe("cms_transport.CMSRegisterRequest", Register);
                transport.subscribe("cms_transport.CMSConnectRequest", Connect);
                transport.subscribe("cms_transport.CMSDisconnectRequest", Disconnect);
                transport.subscribe("cms_transport.CMSStatusRequest", Status);
                transport.subscribe("cms_transport.CMSGetGlobalSettingsRequest", GetSettings);
                transport.subscribe("cms_transport.CMSGetProfilesRequest", GetProfiles);
                transport.subscribe("cms_transport.CMSSetGlobalSettingsRequest", SetSettings);
                transport.subscribe("cms_transport.CMSSetProfileRequest", SetProfiles);

                transport.start();
            }
            
            public void Register(object client_msg)
            {
                CMSRegisterRequest request = (CMSRegisterRequest)client_msg;
                CMSRegisterResponse response = new CMSRegisterResponse();
                Profile[] profiles;
                Settings settings;

                profiles = new Profile[service.cfg.dbSettings.Count];
                string key;
                for (int i = 0; i < service.cfg.dbSettings.Count; i++ )
                {
                    key = service.cfg.dbSettings.Keys[i];
                    profiles[i] = new Profile(key,
                                              service.cfg.getDatabaseSetting(key, "IsConfigured"),
                                              service.cfg.getDatabaseSetting(key, "RequestEnabled"),
                                              service.cfg.getDatabaseSetting(key, "RequestURL"),
                                              service.cfg.getDatabaseSetting(key, "RequestFrequency"),
                                              service.cfg.getDatabaseSetting(key, "ManagerAddress"),
                                              service.cfg.getDatabaseSetting(key, "ManagerPort"),
                                              service.cfg.getDatabaseSetting(key, "ManagerVPNAddress"),
                                              service.cfg.getDatabaseSetting(key, "ManagerSOAPPort"));
                }

                settings = new Settings(service.cfg.getApplicationSetting("AutoStart"), 
                                        service.cfg.getApplicationSetting("Lockdown"),
                                        service.cfg.getApplicationSetting("ConnectionTimeout"), 
                                        service.cfg.getApplicationSetting("QuerySetting"), 
                                        service.cfg.getApplicationSetting("QueryTimeout"),
                                        service.cfg.getApplicationSetting("accept_reject_mesg"),
                                        service.cfg.getApplicationSetting("LockSetting"),
                                        service.cfg.getApplicationSetting("ConnectPriority"),
                                        service.cfg.getApplicationSetting("FileTransferEnabled"),
                                        service.cfg.getApplicationSetting("QueryIfNoLogon"));

                response.connectionName = service.CONNECTION_NAME;
                response.connectionStatus = service.CONNECTION_STATUS;
                response.profiles = profiles;
                response.settings = settings;
                response.version = Program.SOFTWARE_NUMBER;
                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

            public void Connect(object client_msg)
            {
                CMSConnectRequest request = (CMSConnectRequest)client_msg;
                CMSConnectResponse response = new CMSConnectResponse();
                string returnStatus;

                service.CONNECTION_NAME = request.connectionName;
                setSetting("DefaultConnection", request.connectionName);
                service.cfg.saveApplicationSettings();

                if (service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "RequestEnabled") == "false")
                {
                    service.CONNECTION_CONFIG = "IMMEDIATE";
                    service.CONNECTION_STATUS = "START_CONNECTING_WITH_TIMEOUT";
                    returnStatus = "CONNECTING";
                }
                else
                {
                    service.CONNECTION_CONFIG = "BY_REQUEST";
                    service.CONNECTION_STATUS = "START_WAITING_REQUEST_WITH_TIMEOUT";
                    returnStatus = "WAITING_REQUEST";
                }

                response.result = "OK";
                response.connectionName = service.CONNECTION_NAME;
                response.connectionStatus = returnStatus;

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

			/*
            public void Connect(object client_msg)
            {
                lock (service.lockThis)
                {
                    Connect_lock(client_msg);
                }
            }
			*/
            
            public void Disconnect(object client_msg)
            {
                CMSLogger.log(Program.programName + "::DISCONNECT" + Environment.NewLine);
                CMSDisconnectRequest request = (CMSDisconnectRequest)client_msg;
                CMSDisconnectResponse response = new CMSDisconnectResponse();

                CMSLogger.debug("Message: DISCONNECT");

                lock (service.lockThis)
                {
                    service.CONNECTION_STATUS = "DISCONNECTING";
                }
                    
                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

			/*
            public void Status(object client_msg)
            {
                lock (service.lockThis)
                {
                    Status_lock(client_msg);
                }
            }
			*/

            public void Status(object client_msg)
            {
                CMSLogger.debug("Message: STATUS");
                CMSStatusRequest request = (CMSStatusRequest)client_msg;
                CMSStatusResponse response = new CMSStatusResponse();
                Vpn vpn = null;
                Network network = null;
                //VPN
                string returnStatus = "";
                
                network = new Network(service.information_module.CLIENT_SYSTEM_NAME,
                                      service.information_module.CLIENT_CUSTOM_NAME,
                                      service.information_module.CLIENT_OS_VERSION,
                                      service.information_module.CLIENT_SERIAL_NUMBER_WITH_BE,
                                      "N/A",
                                      "N/A",
                                      "N/A");

                returnStatus = service.CONNECTION_STATUS;
                if (((service.CONNECTION_STATUS == "START_CONNECTING") ||
                     (service.CONNECTION_STATUS == "START_CONNECTING_WITH_TIMEOUT") ||
                     (service.CONNECTION_STATUS == "CONNECTING_WITH_TIMEOUT")) &&
                     (service.CONNECTION_CONFIG == "IMMEDIATE"))
                {
                    returnStatus = "CONNECTING";
                }

                if (((service.CONNECTION_STATUS == "START_CONNECTING") ||
                     (service.CONNECTION_STATUS == "START_CONNECTING_WITH_TIMEOUT") ||
                     (service.CONNECTION_STATUS == "CONNECTING_WITH_TIMEOUT") ||
                     (service.CONNECTION_STATUS == "START_WAITING_REQUEST") ||
                     (service.CONNECTION_STATUS == "WAITING_REQUEST_WITH_TIMEOUT") ||
                     (service.CONNECTION_STATUS == "START_WAITING_REQUEST_WITH_TIMEOUT")) &&
                     (service.CONNECTION_CONFIG == "BY_REQUEST"))
                {
                    returnStatus = "WAITING_REQUEST";
                }

                if (service.CONNECTION_REASON != "")
                {
                    returnStatus = service.CONNECTION_REASON;
                }
                if (service.CONNECTION_STATUS == "CONNECTED")
                {
                    TimeSpan ts = service.getConnectionTimerTime();
                    vpn = new Vpn(String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds));
                    CMSLogger.debug("VPN timer: " + vpn.lifetime);

                    if (service.network_module.status() == "Up")
                    {
                        CMSLogger.debug("TAP-interface: Up");

                        network.receivedBytes = String.Format("{0}", service.network_module.receivedInBytes);
                        network.sendtBytes = String.Format("{0}", service.network_module.sentInBytes);
                        network.ip = service.network_module.ipAddress;
                    }
                    else
                    {
                        CMSLogger.debug("TAP-interface: Down");
                    }
                }

                response.result = "OK";
                response.connectionStatus = returnStatus;
                response.vpn = vpn;
                response.network = network;

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

            public void GetSettings(object client_msg)
            {
                CMSGetGlobalSettingsRequest request = (CMSGetGlobalSettingsRequest)client_msg;
                CMSGetGlobalSettingsResponse response = new CMSGetGlobalSettingsResponse();
                Settings settings;

                settings = new Settings(service.cfg.getApplicationSetting("AutoStart"),
                        service.cfg.getApplicationSetting("Lockdown"),
                        service.cfg.getApplicationSetting("ConnectionTimeout"), 
                        service.cfg.getApplicationSetting("QuerySetting"),
                        service.cfg.getApplicationSetting("QueryTimeout"),
                        service.cfg.getApplicationSetting("accept_reject_mesg"),
                        service.cfg.getApplicationSetting("LockSetting"),
                        service.cfg.getApplicationSetting("ConnectPriority"),
                        service.cfg.getApplicationSetting("FileTransferEnabled"),
                        service.cfg.getApplicationSetting("QueryIfNoLogon"));

                response.settings = settings;
                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

            public void GetProfiles(object client_msg)
            {
                CMSGetProfilesRequest request = (CMSGetProfilesRequest)client_msg;
                CMSGetProfilesResponse response = new CMSGetProfilesResponse();
                Profile[] profiles;
                
                profiles = new Profile[service.cfg.dbSettings.Count];
                string key;
                for (int i = 0; i < service.cfg.dbSettings.Count; i++)
                {
                    key = service.cfg.dbSettings.Keys[i];
                    profiles[i] = new Profile(key,
                                              service.cfg.getDatabaseSetting(key, "IsConfigured"),
                                              service.cfg.getDatabaseSetting(key, "RequestEnabled"),
                                              service.cfg.getDatabaseSetting(key, "RequestURL"),
                                              service.cfg.getDatabaseSetting(key, "RequestFrequency"),
                                              service.cfg.getDatabaseSetting(key, "ManagerAddress"),
                                              service.cfg.getDatabaseSetting(key, "ManagerPort"),
                                              service.cfg.getDatabaseSetting(key, "ManagerVPNAddress"),
                                              service.cfg.getDatabaseSetting(key, "ManagerSOAPPort"));
                }
                response.profiles = profiles;
                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

            public void SetSettings(object client_msg)
            {
                CMSSetGlobalSettingsRequest request = (CMSSetGlobalSettingsRequest)client_msg;
                CMSSetGlobalSettingsResponse response = new CMSSetGlobalSettingsResponse();

                setSetting("AutoStart", request.settings.autostart);
                setSetting("Lockdown", request.settings.lockdown);
                setSetting("ConnectionTimeout", request.settings.connectionTimeout);
                setSetting("QuerySetting", request.settings.querySetting);
                setSetting("QueryTimeout", request.settings.queryTimeout);
                setSetting("accept_reject_mesg", request.settings.accept_reject_mesg);
                setSetting("LockSetting", request.settings.lockSetting);
                setSetting("ConnectPriority", request.settings.connectPriority);
                setSetting("FileTransferEnabled", request.settings.fileTransferEnabled);
                setSetting("QueryIfNoLogon", request.settings.queryIfNoLogon);
                service.cfg.saveApplicationSettings();

                //service.vnc_module.vnc_info["ManagerVPNAddress"] = service.cfg.getDatabaseSetting(service.CONNECTION_NAME, "ManagerVPNAddress");
                service.vnc_module.vnc_info["QuerySetting"] =  request.settings.querySetting.ToLower();
                service.vnc_module.vnc_info["QueryTimeout"] = request.settings.connectionTimeout;
                service.vnc_module.vnc_info["accept_reject_mesg"] = request.settings.accept_reject_mesg;
                service.vnc_module.vnc_info["LockSetting"] = request.settings.lockSetting.ToLower();
                service.vnc_module.vnc_info["ConnectPriority"] = request.settings.connectPriority.ToLower();
                service.vnc_module.vnc_info["FileTransferEnabled"] = request.settings.fileTransferEnabled.ToLower();
                service.vnc_module.vnc_info["QueryIfNoLogon"] = request.settings.queryIfNoLogon.ToLower();
                service.vncConfigChanged = true;

                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }

            private void setSetting(string key, string value)
            {
                if ((value != "") && (value != null))
                {
                    service.cfg.setApplicationSettings(key, value);
                }
            }

            public void SetProfiles(object client_msg)
            {
                CMSSetProfileRequest request = (CMSSetProfileRequest)client_msg;
                CMSSetProfileResponse response = new CMSSetProfileResponse();

                List<string> mylist = new List<string>();
                int count = service.cfg.dbSettings.Count;
                for (int i = 0; i < count; i++)
                {
                    mylist.Add(service.cfg.dbSettings.Keys[i]);
                }
 
                for (int i = 0; i < count; i++)
                {
                    service.cfg.cfg.ConnectionStrings.ConnectionStrings.Remove(mylist[i]);
                }

                string str_con;
                ConnectionStringSettings constr_con;
                foreach (Profile profile in request.profiles)
                {
                    str_con = profile.name + ";" +
                              profile.isConfigured + ";" +
                              profile.requestEnabled + ";" +
                              profile.requestURL + ";" +
                              profile.requestFrequency + ";" +
                              profile.managerAddress + ";" +
                              profile.managerPort + ";" +
                              profile.managerVPNAddress + ";" +
                              profile.managerSOAPPort;

                    constr_con = new ConnectionStringSettings(profile.name, str_con, profile.name);
                    service.cfg.cfg.ConnectionStrings.ConnectionStrings.Add(constr_con);
                }
                service.cfg.cfg.Save();
                service.readConfig();
                response.result = "OK";

                var serializer = new JavaScriptSerializer(new CustomResolver());
                string msg = serializer.Serialize(response);
                CMSLogger.debug("SEND message: " + msg);
                transport.send(msg);
            }
        
        }
    }
}






