﻿using System;
using System.ServiceProcess;
using System.Security.Principal;
using System.Windows.Forms;
using System.Linq;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Management;
using System.Text.RegularExpressions;

namespace cms_client_service
{
    static class Program
    {
        //Настройки приложения

        //название конфига
        public static string CONFIG_NAME = "cms_client.config";
        //включает логгирование в файл
        public static bool DEBUG = false;
        //запускает приложение как службу
        public static bool SERVICE = true;
        //версия ПО
        public static string SOFTWARE_NUMBER = "2.2.0.0";
        //название программы
        public static string programName = "BE CMS Endpoint Service";

        // Главная точка входа для приложения.
        static void Main()
        {
            if (SERVICE)
            {
                ServiceBase.Run(new CMSService());
            }
            else
            {
                if (IsUserAdministrator())
                {
                    CMSService myServ = new CMSService();
                    myServ.Process();
                }
                else
                {
                    MessageBox.Show("Программа должна быть запущена с правами администратора !",
                                    "Некорректный запуск",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation,
                                    MessageBoxDefaultButton.Button1);
                }
            }
        }

        public static bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            WindowsIdentity user = null;
            try
            {
                //get the currently logged in user
                user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            finally
            {
                if (user != null)
                    user.Dispose();
            }
            return isAdmin;
        }
    }
}
