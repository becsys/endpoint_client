﻿using System;
using System.Linq;
using Microsoft.Win32;
using System.Management;
using System.Text;
using System.Threading;

namespace cms_client_service
{
    namespace CMSModule
    {
        public class Information
        {
            public string CLIENT_SYSTEM_NAME;
            public string CLIENT_CUSTOM_NAME;
            public string CLIENT_OS_VERSION;
            public string CLIENT_SERIAL_NUMBER;
            public string CLIENT_SERIAL_NUMBER_WITH_BE;

            public Information()
            {
                decorate(getSystemName);
                decorate(getComputerName);
                decorate(getSerialNumber);
                decorate(getOsVersion);
            }

            public delegate void infoFunc();
            public void decorate(infoFunc func)
            {
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        func();
                        break;
                    }
                    catch (Exception ex)
                    {
                        CMSLogger.debug("Обнаружена ошибка при получении инфомрации о ПК: " + ex.Message);
                        Thread.Sleep(500);
                        if (i == 2)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
            }

            //Получить ComputerName (должно быть задано в реестре, при корректной настройке софта в Почте России)
            // CLIENT_CUSTOM_NAME
            public void getComputerName()
            {
                using (RegistryKey Key = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\RussianPost\\Inventory"))
                {
                    if (Key != null)
                    {
                        if ((string)Key.GetValue("ComputerName") != null)
                        {
                            CLIENT_CUSTOM_NAME = (string)Key.GetValue("ComputerName");
                        }
                        Key.Close();
                    }
                    else {
                        CLIENT_CUSTOM_NAME = "N/A";
                    }
                }
            }

            //CLIENT_SERIAL_NUMBER
            //CLIENT_SERIAL_NUMBER_WITH_BE
            public void getSerialNumber()
            {
                //Получить SerialNumber 
                string drive = "C";
                ManagementObject dsk = new ManagementObject(@"win32_logicaldisk.deviceid=""" + drive + @":""");
                dsk.Get();
                string volumeSerial = dsk["VolumeSerialNumber"].ToString();

                if (Program.DEBUG)
                    CMSLogger.debug("VolumeSerialNumber: " + volumeSerial);

                //string str = "A" + (volumeSerial + "BE CMS Endpoint Client").GetHashCode().ToString("X");
                
                string str = volumeSerial + CLIENT_SYSTEM_NAME;
                byte[] bytes = Encoding.ASCII.GetBytes(str);

                Common.Crc32 crc32 = new Common.Crc32();
                String hash = String.Empty;

                //bytes = new byte[str_base.Length * sizeof(char)];
                str = BitConverter.ToString(crc32.ComputeHash(bytes));
                CLIENT_SERIAL_NUMBER = "A" + str.Replace("-", "");

                CMSLogger.debug("CLIENT_SERIAL_NUMBER: " + CLIENT_SERIAL_NUMBER);

                CLIENT_SERIAL_NUMBER_WITH_BE = "BE-" + CLIENT_SERIAL_NUMBER;
            }

            //CLIENT_OS_VERSION
            public void getOsVersion()
            {
                //Используем WMI(Windows Management Instrumentation)
                var name = (from x in new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem").Get().OfType<ManagementObject>()
                            select x.GetPropertyValue("Caption")).FirstOrDefault();

                //CLIENT_OS_VERSION = name != null ? name.ToString() : "Unknown";
                if (name != null)
                {
                    CLIENT_OS_VERSION = name.ToString();
                }
                else
                {
                    //Используем реестр (Если на машине не установлен WMI)
                    CLIENT_OS_VERSION = FriendlyName();
                }
            }

            //CLIENT_SYSTEM_NAME
            public void getSystemName ()
            {
                CLIENT_SYSTEM_NAME = System.Environment.MachineName;
            }

            private string FriendlyName()
            {
                string ProductName = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
                string CSDVersion = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CSDVersion");
                if (ProductName != "")
                {
                    return (ProductName.StartsWith("Microsoft") ? "" : "Microsoft ") + ProductName +
                                (CSDVersion != "" ? " " + CSDVersion : "");
                }
                return "";
            }

            private string HKLM_GetString(string path, string key)
            {
                try
                {
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
                    if (rk == null) return "";
                    return (string)rk.GetValue(key);
                }
                catch { return ""; }
            }

        }
    }
}
