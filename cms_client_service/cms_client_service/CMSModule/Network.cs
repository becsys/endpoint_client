﻿using System.Linq;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Management;
using System.Text.RegularExpressions;
using System;
using System.Threading;

namespace cms_client_service
{
    namespace CMSModule
    {
        public class Network
        {
            public long receivedInBytes;
            public long sentInBytes;
            public string ipAddress;

            private struct tap_interface_info
            {
                public string Name;
                public string Id;
                public string OperationalStatus;
                public string IpAddress;
                public string PhysicalAddress;
                public string NetworkInterfaceType;
                public string DnsAddresses;
                public string DnsSuffix;
                public string GatewayAddresses;
                public string IsDnsEnabled;
                public string WinsServersAddresses;
                public string speed;
                public long ReceivedInBytes;
                public long SentInBytes;
            };

            //получить статус сетевого интерфейса TAP-Windows Adapter V9
            //если интерфейс активен, то заполнить поляi receivedInBytes, sentInBytes, ipAddress;
            public string status()
            {
                //interface struct
                tap_interface_info interface_info;
                //GetInterfaceInfo
                interface_info = new tap_interface_info();
                for (int i = 0; i < 3; i++ )
                {
                    try
                    {
                        interface_info = GetNetworkInterfaceStats(interface_info);
                        break;
                    }
                    catch (Exception ex)
                    {
                        CMSLogger.debug("Обнаружена ошибка при работе с TAP-интерфейсом: " + ex.Message);
                        Thread.Sleep(500);
                        if (i==2)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }

                if (interface_info.OperationalStatus == "Up")
                {
                    receivedInBytes = interface_info.ReceivedInBytes;
                    sentInBytes = interface_info.SentInBytes;
                    ipAddress = interface_info.IpAddress;
                }
                else
                {
                    receivedInBytes = 0;
                    sentInBytes = 0;
                    ipAddress = "";
                }

                return interface_info.OperationalStatus;
            }

            public bool isEnabled()
            {
                NetworkInterface[] adapters = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                foreach (System.Net.NetworkInformation.NetworkInterface adapter in adapters)
                {
                    if (Regex.IsMatch(adapter.Description, @"^TAP-Windows Adapter V9"))
                    {
                        return true;
                    }
                }
                return false;
            }

            //Получить информацию о сетевом интерфейсе TAP-Windows Adapter V9
            private static tap_interface_info GetNetworkInterfaceStats(tap_interface_info interface_info)
            {
                CMSLogger.debug("Network: check TAP status()");
                //set default interface OperationalStatus
                interface_info.OperationalStatus = "Down";

                //Get all network interfaces
                NetworkInterface[] adapters = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                
                /*
                foreach (System.Net.NetworkInformation.NetworkInterface adapter in adapters)
                {
                    Console.WriteLine(adapter.Description);
                    Console.WriteLine(adapter.OperationalStatus);
                }
                */
                
                foreach (System.Net.NetworkInformation.NetworkInterface adapter in adapters.Where(a => a.OperationalStatus == OperationalStatus.Up))
                {
                    //if (adapter.Description == "TAP-Windows Adapter V9 - Минипорт планировщика пакетов")
                    if (Regex.IsMatch(adapter.Description, @"^TAP-Windows Adapter V9"))
                    {
                        //set interface OperationalStatus
                        interface_info.OperationalStatus = "Up";

                        //InterfaceStatistics
                        System.Net.NetworkInformation.IPv4InterfaceStatistics ipStats = adapter.GetIPv4Statistics();

                        //long bytesReceivedInKB = ipStats.UnicastPacketsReceived / 1024;
                        //long bytesSentInKB = ipStats.UnicastPacketsSent / 1024;
                        //interface_info.ReceivedInKB = String.Format("{0} ({1} byte)", bytesReceivedInKB, ipStats.BytesReceived);
                        //interface_info.SentInKB = String.Format("{0} ({1} byte)", bytesSentInKB, ipStats.BytesSent);

                        //Received/Transmitted In Bytes
                        interface_info.ReceivedInBytes = ipStats.BytesReceived;
                        interface_info.SentInBytes = ipStats.BytesSent;

                        //Получение параметров интерфейса
                        System.Net.NetworkInformation.IPInterfaceProperties properties = adapter.GetIPProperties();

                        //адреса dns серверов
                        string dnscollection = string.Empty;
                        System.Net.NetworkInformation.IPAddressCollection dnsServers = properties.DnsAddresses;
                        if (dnsServers.Count > 0)
                        {
                            foreach (System.Net.IPAddress dns in dnsServers)
                            {
                                interface_info.DnsAddresses += dns + "; ";
                            }
                        }

                        //DNS суффикс
                        interface_info.DnsSuffix = properties.DnsSuffix;

                        //Возвращает значение true, если на этом интерфейсе настроен NetBIOS
                        //через TCP/IP для использования разрешения DNS-имен;
                        //в противном случае — false.
                        interface_info.IsDnsEnabled = properties.IsDnsEnabled.ToString();

                        //Получение IP адреса версии 4
                        foreach (System.Net.NetworkInformation.IPAddressInformation unicast
                                            in properties.UnicastAddresses)
                        {
                            //Так как UnicastAddresses содержит две версии(4,6) Ip адреса
                            //Выполняем проверку, что это  Ip адрес 4 версии
                            interface_info.IpAddress = "";
                            if (unicast.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                interface_info.IpAddress += System.Net.IPAddress.Parse(unicast.Address.ToString());
                        }

                        //адреса сетевых шлюзов
                        string GatewayAddresses = string.Empty;
                        foreach (System.Net.NetworkInformation.GatewayIPAddressInformation address
                                                        in properties.GatewayAddresses)
                        {
                            interface_info.GatewayAddresses += address.Address + "; ";
                        }

                        //адреса WINS-серверов
                        string WinsServersAddresses = string.Empty;
                        foreach (System.Net.IPAddress address in properties.WinsServersAddresses)
                        {
                            interface_info.WinsServersAddresses += address + "; ";
                        }
                        break; // get out of the loop
                    }
                }

                return interface_info;
            }
			
			public string getPhysicalAddress()
			{
				Process p = new Process();
				p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				p.StartInfo.UseShellExecute = false;
				p.StartInfo.FileName = "route";
				p.StartInfo.Arguments = "PRINT";
				p.StartInfo.RedirectStandardOutput = true;
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.StandardOutputEncoding = Encoding.Default;
				p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				p.Start();
				string Text = p.StandardOutput.ReadToEnd();
                string input;
                try
                {
                    input = Regex.Split(Text, "IPv4")[1];
                }
                catch
                {
                    input = Text;
                }
				
				string pattern = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
				string physIP = Regex.Matches(input, pattern)[3].Value;
				return physIP;
			}

        }
    }
}
