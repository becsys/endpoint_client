﻿using System.Collections.Specialized;
using System.Collections.Generic;
using System;
using System.IO;

namespace cms_client_service
{
    namespace CMSModule
    {
        public class VNC
        {
            public string VNC_SERVICE;
            public string VNC_CONFIG_FILE = "";
            public int SERVICE_TIMEOUT;
            private SortedList<string, string> mapping;
            public SortedList<string, string> vnc_info;

            public string AutoHost
            {
                get { return vnc_info["ManagerVPNAddress"]; }
                set { vnc_info["ManagerVPNAddress"] = value; }
            }


            public VNC(string name, string config, string timeout)
            {
                VNC_SERVICE = name;
                VNC_CONFIG_FILE = config;
                SERVICE_TIMEOUT = Convert.ToInt32(timeout);

                vnc_info = new SortedList<string, string>();
                vnc_info.Add("ManagerVPNAddress", "10.10.10.1");
                vnc_info.Add("QuerySetting", "false");
                vnc_info.Add("QueryTimeout", "0");
                vnc_info.Add("accept_reject_mesg", "");
                vnc_info.Add("LockSetting", "false");
                vnc_info.Add("ConnectPriority", "false");
                vnc_info.Add("FileTransferEnabled", "false");
                vnc_info.Add("QueryIfNoLogon", "false");

                mapping = new SortedList<string, string>();
                mapping.Add("QuerySetting", "4,2"); //true, false
                mapping.Add("LockSetting", "1,0");
                mapping.Add("ConnectPriority", "1,3");
                mapping.Add("FileTransferEnabled", "1,0");
                mapping.Add("QueryIfNoLogon", "1,0");

                updateConfig();               
                //restart();

            }

            public void updateConfig()
            {
                set_default_config();
                update_ini(vnc_info);
            }

            public void set_default_config()
            {
                if (Program.DEBUG)
                    CMSLogger.debug("VNC: set_default_config()");

                using (var writer = new StreamWriter(VNC_CONFIG_FILE, false))
                {
                    // write a bunch of stuff here
                    writer.WriteLine(default_ini);
                }
            }

            private bool vnc_service_started = false;
            
            private string ini_mapping(string key, string value)
            {
                if (mapping.ContainsKey(key))
                {
                    if (value == "true")
                    {
                        return mapping[key].Split(',')[0];
                    }
                    else
                    {
                        return mapping[key].Split(',')[1];
                    }
                    
                }
                return value;
            }

            public void update_ini(SortedList<string, string> info)
            {
                
                Common.IniFile ini = new Common.IniFile(VNC_CONFIG_FILE);
              
                //IList<string> i_keys = info.Keys;

                IList<string> cc = info.Keys;
                string[] keys = new string[10];
                cc.CopyTo(keys, 0);
                foreach (string key in keys)
                {
                    if (key == null)
                        break;

                    //string section = key.Split(',')[0];
                    //string param = key.Split(',')[1];
                    string section ="admin";
                    string param = key;
                    string value = ini_mapping(key, info[key]);

                    if (key == "ManagerVPNAddress")
                    {
                        ini.IniWriteValue(section, "AuthHosts", "-:+" + value + ":+172.19.4");
                    }
                    else
                    {
                        ini.IniWriteValue(section, param, value);
                    }
                    
                }
                 
                //string username = ini.IniReadValue("sftp", "Username");
                //string password = ini.IniReadValue("sftp", "Password");
                //string remotehost = ini.IniReadValue("sftp", "Remote Host");
                //ini.IniWriteValue("sftp", "Username", "test");

            }

            public bool restart()
            {
                if (isRunning())
                {
                    stop();
                }
                start();

                return isRunning();
            }

            public string getStatus()
            {
                return Common.WindowsService.ServiceStatus(VNC_SERVICE);
            }

            public bool isRunning()
            {
                /*
                if (Common.WindowsService.ServiceStatus(VNC_SERVICE) == "Running")
                {
                    return true;
                }
                else
                {
                    return false;
                }
                */

                return Common.WindowsService.ServiceStatus(VNC_SERVICE) == "Running" ? true : false;
            }

            public void start()
            {
                Common.WindowsService.StartService(VNC_SERVICE, SERVICE_TIMEOUT);
                vnc_service_started = true;
            }

            public void stop()
            {
                Common.WindowsService.StopService(VNC_SERVICE, SERVICE_TIMEOUT);
                vnc_service_started = false;
            }

            public bool isStarted()
            {
                return vnc_service_started;
            }

            private string default_ini = "[ultravnc]" + Environment.NewLine +
"passwd=28AD591A62B4AD949F" + Environment.NewLine +
"passwd2=8C46F601FBBA0067E5" + Environment.NewLine +
"[admin]"+ Environment.NewLine +
"UseRegistry=0"+ Environment.NewLine +
"MSLogonRequired=0" + Environment.NewLine +
"NewMSLogon=0" + Environment.NewLine +
"DebugMode=0" + Environment.NewLine +
"Avilog=0" + Environment.NewLine +
"path=C:\\Program Files\\Ultra VNC" + Environment.NewLine +
"accept_reject_mesg=Вы разрешаете подключение специалиста службы поддержки?" + Environment.NewLine +
"DebugLevel=8" + Environment.NewLine +
"DisableTrayIcon=1" + Environment.NewLine +
"LoopbackOnly=0" + Environment.NewLine +
"UseDSMPlugin=0" + Environment.NewLine +
"AllowLoopback=0" + Environment.NewLine +
"AuthRequired=1" + Environment.NewLine +
"ConnectPriority=1" + Environment.NewLine +
"DSMPlugin=No Plugin Detected" + Environment.NewLine +
"AuthHosts=-:+10.10.10.1:+172.19.4" + Environment.NewLine +
"DSMPluginConfig=" + Environment.NewLine +
"AllowShutdown=1" + Environment.NewLine +
"AllowProperties=0" + Environment.NewLine +
"AllowEditClients=0" + Environment.NewLine +
"FileTransferEnabled=1" + Environment.NewLine +
"FTUserImpersonation=1" + Environment.NewLine +
"BlankMonitorEnabled=1" + Environment.NewLine +
"BlankInputsOnly=0" + Environment.NewLine +
"DefaultScale=1" + Environment.NewLine +
"primary=1" + Environment.NewLine +
"secondary=0" + Environment.NewLine +
"SocketConnect=1" + Environment.NewLine +
"HTTPConnect=0" + Environment.NewLine +
"AutoPortSelect=0" + Environment.NewLine +
"PortNumber=8080" + Environment.NewLine +
"HTTPPortNumber=2030" + Environment.NewLine +
"IdleTimeout=900" + Environment.NewLine +
"IdleInputTimeout=0" + Environment.NewLine +
"RemoveWallpaper=1" + Environment.NewLine +
"RemoveAero=1" + Environment.NewLine +
"QuerySetting=2" + Environment.NewLine +
"QueryTimeout=15" + Environment.NewLine +
"QueryAccept=0" + Environment.NewLine +
"QueryIfNoLogon=1" + Environment.NewLine +
"InputsEnabled=1" + Environment.NewLine +
"LockSetting=0" + Environment.NewLine +
"LocalInputsDisabled=0" + Environment.NewLine +
"EnableJapInput=0" + Environment.NewLine +
"kickrdp=0" + Environment.NewLine +
"clearconsole=0" + Environment.NewLine +
"service_commandline=" + Environment.NewLine +
"FileTransferTimeout=1" + Environment.NewLine +
"KeepAliveInterval=5" + Environment.NewLine +
"[admin_auth]" + Environment.NewLine +
"group1=" + Environment.NewLine +
"group2=" + Environment.NewLine +
"group3=" + Environment.NewLine +
"locdom1=0" + Environment.NewLine +
"locdom2=0" + Environment.NewLine +
"locdom3=0" + Environment.NewLine +
"[poll]" + Environment.NewLine +
"TurboMode=1" + Environment.NewLine +
"PollUnderCursor=0" + Environment.NewLine +
"PollForeground=0" + Environment.NewLine +
"PollFullScreen=1" + Environment.NewLine +
"OnlyPollConsole=0" + Environment.NewLine +
"OnlyPollOnEvent=0" + Environment.NewLine +
"MaxCpu=40" + Environment.NewLine +
"EnableDriver=1" + Environment.NewLine +
"EnableHook=1" + Environment.NewLine +
"EnableVirtual=0" + Environment.NewLine +
"SingleWindow=0" + Environment.NewLine +
"SingleWindowName=";
        }
    }
}


/*
      catch (Exception e)
      {
          if (Program.DEBUG)
              CMSLogger.debug("Update VNC ini file: ERROR " + e.ToString());
      }
 */