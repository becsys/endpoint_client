﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;

namespace cms_client_service
{
    namespace CMSModule
    {
        public class PublishWeb
        {
            public int SOAP_TIMEOUT;
            public string SOAP_USERNAME;
            public string SOAP_PASSWORD;
            public string SOAP_URL = "";

            public int interval;
            public int delay;

            public PublishWeb(string delay, string timeout, string interval, string username, string password)
            {
                setDelay(delay);
                SOAP_TIMEOUT = Convert.ToInt32(timeout);
                setInterval(interval);
                SOAP_USERNAME = username;
                SOAP_PASSWORD = password;

            }
            public void setInterval(string frequency)
            {
                Int32.TryParse(frequency, out interval);
                if (interval < 10)
                {
                    interval = 10;
                }
            }

            public int getInterval()
            {
                return interval;
            }

            public void setDelay(string in_delay)
            {
                Int32.TryParse(in_delay, out delay);
            }

            public int getDelay()
            {
                if (delay > interval)
                {
                    delay = interval;
                }

                return delay;
            }

            public string postXMLData(SortedList<string, string> info)
            {
                string destinationUrl = SOAP_URL;
                string username = SOAP_USERNAME;
                string password = SOAP_PASSWORD;

                if (Program.DEBUG)
                    CMSLogger.debug("PublishWeb: prepare xml");

                string XmlData = "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>" +
                      "<s11:Body>" +
                        "<ns1:REGISTERInput xmlns:ns1='http://xmlns.oracle.com/orawsv/CMS/REGISTER'>" +
                          "<ns1:P255_PDU6_ALLOWED_IP-VARCHAR2-IN></ns1:P255_PDU6_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P254_PDU6_SNMP_COMM_RW-VARCHAR2-IN></ns1:P254_PDU6_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P253_PDU6_SNMP_COMM_RO-VARCHAR2-IN></ns1:P253_PDU6_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P252_PDU6_SNMP_VERSION-VARCHAR2-IN></ns1:P252_PDU6_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P251_PDU6_GATEWAY-VARCHAR2-IN></ns1:P251_PDU6_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P250_PDU6_NETMASK-VARCHAR2-IN></ns1:P250_PDU6_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P249_PDU6_IP_ADDRESS-VARCHAR2-IN></ns1:P249_PDU6_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P248_PDU6_INPUT_METER-VARCHAR2-IN></ns1:P248_PDU6_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P247_PDU6_INPUT_STATUS-VARCHAR2-IN></ns1:P247_PDU6_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P246_PDU6_OUTLET_METER-VARCHAR2-IN></ns1:P246_PDU6_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P245_PDU6_OUTLET_STATUS-VARCHAR2-IN></ns1:P245_PDU6_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P244_PDU6_CONN_TYPE-VARCHAR2-IN></ns1:P244_PDU6_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P243_PDU6_FEATURES-VARCHAR2-IN></ns1:P243_PDU6_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P242_PDU6_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P242_PDU6_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P241_PDU6_MODEL-VARCHAR2-IN></ns1:P241_PDU6_MODEL-VARCHAR2-IN>" +
                          "<ns1:P240_PDU6_SN-VARCHAR2-IN></ns1:P240_PDU6_SN-VARCHAR2-IN>" +
                          "<ns1:P239_PDU5_ALLOWED_IP-VARCHAR2-IN></ns1:P239_PDU5_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P238_PDU5_SNMP_COMM_RW-VARCHAR2-IN></ns1:P238_PDU5_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P237_PDU5_SNMP_COMM_RO-VARCHAR2-IN></ns1:P237_PDU5_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P236_PDU5_SNMP_VERSION-VARCHAR2-IN></ns1:P236_PDU5_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P235_PDU5_GATEWAY-VARCHAR2-IN></ns1:P235_PDU5_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P234_PDU5_NETMASK-VARCHAR2-IN></ns1:P234_PDU5_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P233_PDU5_IP_ADDRESS-VARCHAR2-IN></ns1:P233_PDU5_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P232_PDU5_INPUT_METER-VARCHAR2-IN></ns1:P232_PDU5_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P231_PDU5_INPUT_STATUS-VARCHAR2-IN></ns1:P231_PDU5_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P230_PDU5_OUTLET_METER-VARCHAR2-IN></ns1:P230_PDU5_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P229_PDU5_OUTLET_STATUS-VARCHAR2-IN></ns1:P229_PDU5_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P228_PDU5_CONN_TYPE-VARCHAR2-IN></ns1:P228_PDU5_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P227_PDU5_FEATURES-VARCHAR2-IN></ns1:P227_PDU5_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P226_PDU5_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P226_PDU5_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P225_PDU5_MODEL-VARCHAR2-IN></ns1:P225_PDU5_MODEL-VARCHAR2-IN>" +
                          "<ns1:P224_PDU5_SN-VARCHAR2-IN></ns1:P224_PDU5_SN-VARCHAR2-IN>" +
                          "<ns1:P223_PDU4_ALLOWED_IP-VARCHAR2-IN></ns1:P223_PDU4_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P222_PDU4_SNMP_COMM_RW-VARCHAR2-IN></ns1:P222_PDU4_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P221_PDU4_SNMP_COMM_RO-VARCHAR2-IN></ns1:P221_PDU4_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P220_PDU4_SNMP_VERSION-VARCHAR2-IN></ns1:P220_PDU4_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P219_PDU4_GATEWAY-VARCHAR2-IN></ns1:P219_PDU4_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P218_PDU4_NETMASK-VARCHAR2-IN></ns1:P218_PDU4_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P217_PDU4_IP_ADDRESS-VARCHAR2-IN></ns1:P217_PDU4_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P216_PDU4_INPUT_METER-VARCHAR2-IN></ns1:P216_PDU4_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P215_PDU4_INPUT_STATUS-VARCHAR2-IN></ns1:P215_PDU4_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P214_PDU4_OUTLET_METER-VARCHAR2-IN></ns1:P214_PDU4_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P213_PDU4_OUTLET_STATUS-VARCHAR2-IN></ns1:P213_PDU4_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P212_PDU4_CONN_TYPE-VARCHAR2-IN></ns1:P212_PDU4_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P211_PDU4_FEATURES-VARCHAR2-IN></ns1:P211_PDU4_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P210_PDU4_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P210_PDU4_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P209_PDU4_MODEL-VARCHAR2-IN></ns1:P209_PDU4_MODEL-VARCHAR2-IN>" +
                          "<ns1:P208_PDU4_SN-VARCHAR2-IN></ns1:P208_PDU4_SN-VARCHAR2-IN>" +
                          "<ns1:P207_PDU3_ALLOWED_IP-VARCHAR2-IN></ns1:P207_PDU3_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P206_PDU3_SNMP_COMM_RW-VARCHAR2-IN></ns1:P206_PDU3_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P205_PDU3_SNMP_COMM_RO-VARCHAR2-IN></ns1:P205_PDU3_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P204_PDU3_SNMP_VERSION-VARCHAR2-IN></ns1:P204_PDU3_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P203_PDU3_GATEWAY-VARCHAR2-IN></ns1:P203_PDU3_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P202_PDU3_NETMASK-VARCHAR2-IN></ns1:P202_PDU3_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P201_PDU3_IP_ADDRESS-VARCHAR2-IN></ns1:P201_PDU3_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P200_PDU3_INPUT_METER-VARCHAR2-IN></ns1:P200_PDU3_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P199_PDU3_INPUT_STATUS-VARCHAR2-IN></ns1:P199_PDU3_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P198_PDU3_OUTLET_METER-VARCHAR2-IN></ns1:P198_PDU3_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P197_PDU3_OUTLET_STATUS-VARCHAR2-IN></ns1:P197_PDU3_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P196_PDU3_CONN_TYPE-VARCHAR2-IN></ns1:P196_PDU3_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P195_PDU3_FEATURES-VARCHAR2-IN></ns1:P195_PDU3_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P194_PDU3_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P194_PDU3_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P193_PDU3_MODEL-VARCHAR2-IN></ns1:P193_PDU3_MODEL-VARCHAR2-IN>" +
                          "<ns1:P192_PDU3_SN-VARCHAR2-IN></ns1:P192_PDU3_SN-VARCHAR2-IN>" +
                          "<ns1:P191_PDU2_ALLOWED_IP-VARCHAR2-IN></ns1:P191_PDU2_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P190_PDU2_SNMP_COMM_RW-VARCHAR2-IN></ns1:P190_PDU2_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P189_PDU2_SNMP_COMM_RO-VARCHAR2-IN></ns1:P189_PDU2_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P188_PDU2_SNMP_VERSION-VARCHAR2-IN></ns1:P188_PDU2_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P187_PDU2_GATEWAY-VARCHAR2-IN></ns1:P187_PDU2_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P186_PDU2_NETMASK-VARCHAR2-IN></ns1:P186_PDU2_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P185_PDU2_IP_ADDRESS-VARCHAR2-IN></ns1:P185_PDU2_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P184_PDU2_INPUT_METER-VARCHAR2-IN></ns1:P184_PDU2_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P183_PDU2_INPUT_STATUS-VARCHAR2-IN></ns1:P183_PDU2_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P182_PDU2_OUTLET_METER-VARCHAR2-IN></ns1:P182_PDU2_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P181_PDU2_OUTLET_STATUS-VARCHAR2-IN></ns1:P181_PDU2_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P180_PDU2_CONN_TYPE-VARCHAR2-IN></ns1:P180_PDU2_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P179_PDU2_FEATURES-VARCHAR2-IN></ns1:P179_PDU2_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P178_PDU2_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P178_PDU2_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P177_PDU2_MODEL-VARCHAR2-IN></ns1:P177_PDU2_MODEL-VARCHAR2-IN>" +
                          "<ns1:P176_PDU2_SN-VARCHAR2-IN></ns1:P176_PDU2_SN-VARCHAR2-IN>" +
                          "<ns1:P175_PDU1_ALLOWED_IP-VARCHAR2-IN></ns1:P175_PDU1_ALLOWED_IP-VARCHAR2-IN>" +
                          "<ns1:P174_PDU1_SNMP_COMM_RW-VARCHAR2-IN></ns1:P174_PDU1_SNMP_COMM_RW-VARCHAR2-IN>" +
                          "<ns1:P173_PDU1_SNMP_COMM_RO-VARCHAR2-IN></ns1:P173_PDU1_SNMP_COMM_RO-VARCHAR2-IN>" +
                          "<ns1:P172_PDU1_SNMP_VERSION-VARCHAR2-IN></ns1:P172_PDU1_SNMP_VERSION-VARCHAR2-IN>" +
                          "<ns1:P171_PDU1_GATEWAY-VARCHAR2-IN></ns1:P171_PDU1_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P170_PDU1_NETMASK-VARCHAR2-IN></ns1:P170_PDU1_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P169_PDU1_IP_ADDRESS-VARCHAR2-IN></ns1:P169_PDU1_IP_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P168_PDU1_INPUT_METER-VARCHAR2-IN></ns1:P168_PDU1_INPUT_METER-VARCHAR2-IN>" +
                          "<ns1:P167_PDU1_INPUT_STATUS-VARCHAR2-IN></ns1:P167_PDU1_INPUT_STATUS-VARCHAR2-IN>" +
                          "<ns1:P166_PDU1_OUTLET_METER-VARCHAR2-IN></ns1:P166_PDU1_OUTLET_METER-VARCHAR2-IN>" +
                          "<ns1:P165_PDU1_OUTLET_STATUS-VARCHAR2-IN></ns1:P165_PDU1_OUTLET_STATUS-VARCHAR2-IN>" +
                          "<ns1:P164_PDU1_CONN_TYPE-VARCHAR2-IN></ns1:P164_PDU1_CONN_TYPE-VARCHAR2-IN>" +
                          "<ns1:P163_PDU1_FEATURES-VARCHAR2-IN></ns1:P163_PDU1_FEATURES-VARCHAR2-IN>" +
                          "<ns1:P162_PDU1_FIRMWARE_VERSION-VARCHAR2-IN></ns1:P162_PDU1_FIRMWARE_VERSION-VARCHAR2-IN>" +
                          "<ns1:P161_PDU1_MODEL-VARCHAR2-IN></ns1:P161_PDU1_MODEL-VARCHAR2-IN>" +
                          "<ns1:P160_PDU1_SN-VARCHAR2-IN></ns1:P160_PDU1_SN-VARCHAR2-IN>" +
                          "<ns1:P159_COM24_PHY-VARCHAR2-IN></ns1:P159_COM24_PHY-VARCHAR2-IN>" +
                          "<ns1:P158_COM24_NAME-VARCHAR2-IN></ns1:P158_COM24_NAME-VARCHAR2-IN>" +
                          "<ns1:P157_COM23_PHY-VARCHAR2-IN></ns1:P157_COM23_PHY-VARCHAR2-IN>" +
                          "<ns1:P156_COM23_NAME-VARCHAR2-IN></ns1:P156_COM23_NAME-VARCHAR2-IN>" +
                          "<ns1:P155_COM22_PHY-VARCHAR2-IN></ns1:P155_COM22_PHY-VARCHAR2-IN>" +
                          "<ns1:P154_COM22_NAME-VARCHAR2-IN></ns1:P154_COM22_NAME-VARCHAR2-IN>" +
                          "<ns1:P153_COM21_PHY-VARCHAR2-IN></ns1:P153_COM21_PHY-VARCHAR2-IN>" +
                          "<ns1:P152_COM21_NAME-VARCHAR2-IN></ns1:P152_COM21_NAME-VARCHAR2-IN>" +
                          "<ns1:P151_COM20_PHY-VARCHAR2-IN></ns1:P151_COM20_PHY-VARCHAR2-IN>" +
                          "<ns1:P150_COM20_NAME-VARCHAR2-IN></ns1:P150_COM20_NAME-VARCHAR2-IN>" +
                          "<ns1:P149_COM19_PHY-VARCHAR2-IN></ns1:P149_COM19_PHY-VARCHAR2-IN>" +
                          "<ns1:P148_COM19_NAME-VARCHAR2-IN></ns1:P148_COM19_NAME-VARCHAR2-IN>" +
                          "<ns1:P147_COM18_PHY-VARCHAR2-IN></ns1:P147_COM18_PHY-VARCHAR2-IN>" +
                          "<ns1:P146_COM18_NAME-VARCHAR2-IN></ns1:P146_COM18_NAME-VARCHAR2-IN>" +
                          "<ns1:P145_COM17_PHY-VARCHAR2-IN></ns1:P145_COM17_PHY-VARCHAR2-IN>" +
                          "<ns1:P144_COM17_NAME-VARCHAR2-IN></ns1:P144_COM17_NAME-VARCHAR2-IN>" +
                          "<ns1:P143_COM16_PHY-VARCHAR2-IN></ns1:P143_COM16_PHY-VARCHAR2-IN>" +
                          "<ns1:P142_COM16_NAME-VARCHAR2-IN></ns1:P142_COM16_NAME-VARCHAR2-IN>" +
                          "<ns1:P141_COM15_PHY-VARCHAR2-IN></ns1:P141_COM15_PHY-VARCHAR2-IN>" +
                          "<ns1:P140_COM15_NAME-VARCHAR2-IN></ns1:P140_COM15_NAME-VARCHAR2-IN>" +
                          "<ns1:P139_COM14_PHY-VARCHAR2-IN></ns1:P139_COM14_PHY-VARCHAR2-IN>" +
                          "<ns1:P138_COM14_NAME-VARCHAR2-IN></ns1:P138_COM14_NAME-VARCHAR2-IN>" +
                          "<ns1:P137_COM13_PHY-VARCHAR2-IN></ns1:P137_COM13_PHY-VARCHAR2-IN>" +
                          "<ns1:P136_COM13_NAME-VARCHAR2-IN></ns1:P136_COM13_NAME-VARCHAR2-IN>" +
                          "<ns1:P135_COM12_PHY-VARCHAR2-IN></ns1:P135_COM12_PHY-VARCHAR2-IN>" +
                          "<ns1:P134_COM12_NAME-VARCHAR2-IN></ns1:P134_COM12_NAME-VARCHAR2-IN>" +
                          "<ns1:P133_COM11_PHY-VARCHAR2-IN></ns1:P133_COM11_PHY-VARCHAR2-IN>" +
                          "<ns1:P132_COM11_NAME-VARCHAR2-IN></ns1:P132_COM11_NAME-VARCHAR2-IN>" +
                          "<ns1:P131_COM10_PHY-VARCHAR2-IN></ns1:P131_COM10_PHY-VARCHAR2-IN>" +
                          "<ns1:P130_COM10_NAME-VARCHAR2-IN></ns1:P130_COM10_NAME-VARCHAR2-IN>" +
                          "<ns1:P129_COM9_PHY-VARCHAR2-IN></ns1:P129_COM9_PHY-VARCHAR2-IN>" +
                          "<ns1:P128_COM9_NAME-VARCHAR2-IN></ns1:P128_COM9_NAME-VARCHAR2-IN>" +
                          "<ns1:P127_COM8_PHY-VARCHAR2-IN></ns1:P127_COM8_PHY-VARCHAR2-IN>" +
                          "<ns1:P126_COM8_NAME-VARCHAR2-IN></ns1:P126_COM8_NAME-VARCHAR2-IN>" +
                          "<ns1:P125_COM7_PHY-VARCHAR2-IN></ns1:P125_COM7_PHY-VARCHAR2-IN>" +
                          "<ns1:P124_COM7_NAME-VARCHAR2-IN></ns1:P124_COM7_NAME-VARCHAR2-IN>" +
                          "<ns1:P123_COM6_PHY-VARCHAR2-IN></ns1:P123_COM6_PHY-VARCHAR2-IN>" +
                          "<ns1:P122_COM6_NAME-VARCHAR2-IN></ns1:P122_COM6_NAME-VARCHAR2-IN>" +
                          "<ns1:P121_COM5_PHY-VARCHAR2-IN></ns1:P121_COM5_PHY-VARCHAR2-IN>" +
                          "<ns1:P120_COM5_NAME-VARCHAR2-IN></ns1:P120_COM5_NAME-VARCHAR2-IN>" +
                          "<ns1:P119_COM4_PHY-VARCHAR2-IN></ns1:P119_COM4_PHY-VARCHAR2-IN>" +
                          "<ns1:P118_COM4_NAME-VARCHAR2-IN></ns1:P118_COM4_NAME-VARCHAR2-IN>" +
                          "<ns1:P117_COM3_PHY-VARCHAR2-IN></ns1:P117_COM3_PHY-VARCHAR2-IN>" +
                          "<ns1:P116_COM3_NAME-VARCHAR2-IN></ns1:P116_COM3_NAME-VARCHAR2-IN>" +
                          "<ns1:P115_COM2_PHY-VARCHAR2-IN></ns1:P115_COM2_PHY-VARCHAR2-IN>" +
                          "<ns1:P114_COM2_NAME-VARCHAR2-IN></ns1:P114_COM2_NAME-VARCHAR2-IN>" +
                          "<ns1:P113_COM1_PHY-VARCHAR2-IN></ns1:P113_COM1_PHY-VARCHAR2-IN>" +
                          "<ns1:P112_COM1_NAME-VARCHAR2-IN></ns1:P112_COM1_NAME-VARCHAR2-IN>" +
                          "<ns1:P111_WEB_CAM_STATUS-VARCHAR2-IN></ns1:P111_WEB_CAM_STATUS-VARCHAR2-IN>" +
                          "<ns1:P110_WEB_CAM_TYPE-VARCHAR2-IN></ns1:P110_WEB_CAM_TYPE-VARCHAR2-IN>" +
                          "<ns1:P109_MODEM_STATUS-VARCHAR2-IN></ns1:P109_MODEM_STATUS-VARCHAR2-IN>" +
                          "<ns1:P108_MODEM_TYPE-VARCHAR2-IN></ns1:P108_MODEM_TYPE-VARCHAR2-IN>" +
                          "<ns1:P107_NTP4-VARCHAR2-IN></ns1:P107_NTP4-VARCHAR2-IN>" +
                          "<ns1:P106_NTP3-VARCHAR2-IN></ns1:P106_NTP3-VARCHAR2-IN>" +
                          "<ns1:P105_NTP2-VARCHAR2-IN></ns1:P105_NTP2-VARCHAR2-IN>" +
                          "<ns1:P104_NTP1-VARCHAR2-IN></ns1:P104_NTP1-VARCHAR2-IN>" +
                          "<ns1:P103_DNS4-VARCHAR2-IN></ns1:P103_DNS4-VARCHAR2-IN>" +
                          "<ns1:P102_DNS3-VARCHAR2-IN></ns1:P102_DNS3-VARCHAR2-IN>" +
                          "<ns1:P101_DNS2-VARCHAR2-IN></ns1:P101_DNS2-VARCHAR2-IN>" +
                          "<ns1:P100_DNS1-VARCHAR2-IN></ns1:P100_DNS1-VARCHAR2-IN>" +
                          "<ns1:P099_NETMON_TIMEOUT-VARCHAR2-IN></ns1:P099_NETMON_TIMEOUT-VARCHAR2-IN>" +
                          "<ns1:P098_NETMON_HOSTS-VARCHAR2-IN></ns1:P098_NETMON_HOSTS-VARCHAR2-IN>" +
                          "<ns1:P097_NETMON_INTERVAL-VARCHAR2-IN></ns1:P097_NETMON_INTERVAL-VARCHAR2-IN>" +
                          "<ns1:P096_NETFW_INTERFACE_PRI-VARCHAR2-IN></ns1:P096_NETFW_INTERFACE_PRI-VARCHAR2-IN>" +
                          "<ns1:P095_NETMON_PACKETS-VARCHAR2-IN></ns1:P095_NETMON_PACKETS-VARCHAR2-IN>" +
                          "<ns1:P094_NETFW_HOSTS-VARCHAR2-IN></ns1:P094_NETFW_HOSTS-VARCHAR2-IN>" +
                          "<ns1:P093_INT6_DHCP_DNS-VARCHAR2-IN></ns1:P093_INT6_DHCP_DNS-VARCHAR2-IN>" +
                          "<ns1:P092_INT6_DHCP_GATEWAY-VARCHAR2-IN></ns1:P092_INT6_DHCP_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P091_INT6_DHCP_NETMASK-VARCHAR2-IN></ns1:P091_INT6_DHCP_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P090_INT6_DHCP_NETWORK-VARCHAR2-IN></ns1:P090_INT6_DHCP_NETWORK-VARCHAR2-IN>" +
                          "<ns1:P089_INT6_DHCP_SERVER-VARCHAR2-IN></ns1:P089_INT6_DHCP_SERVER-VARCHAR2-IN>" +
                          "<ns1:P088_INT6_KEY-VARCHAR2-IN></ns1:P088_INT6_KEY-VARCHAR2-IN>" +
                          "<ns1:P087_INT6_SSID-VARCHAR2-IN></ns1:P087_INT6_SSID-VARCHAR2-IN>" +
                          "<ns1:P086_INT6_SIGNAL_LEVEL-VARCHAR2-IN></ns1:P086_INT6_SIGNAL_LEVEL-VARCHAR2-IN>" +
                          "<ns1:P085_INT6_MODE-VARCHAR2-IN></ns1:P085_INT6_MODE-VARCHAR2-IN>" +
                          "<ns1:P084_INT6_ENCRYPTION-VARCHAR2-IN></ns1:P084_INT6_ENCRYPTION-VARCHAR2-IN>" +
                          "<ns1:P083_INT6_GATEWAY-VARCHAR2-IN></ns1:P083_INT6_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P082_INT6_NETMASK-VARCHAR2-IN></ns1:P082_INT6_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P081_INT6_ADDRESS-VARCHAR2-IN></ns1:P081_INT6_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P080_INT6_PROTO-VARCHAR2-IN></ns1:P080_INT6_PROTO-VARCHAR2-IN>" +
                          "<ns1:P079_INT6_AVG_DELAY-VARCHAR2-IN></ns1:P079_INT6_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P078_INT6_PACKET_LOSS-VARCHAR2-IN></ns1:P078_INT6_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P077_INT6_CURR_STATE-VARCHAR2-IN></ns1:P077_INT6_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P076_INT6_ADMIN_STATUS-VARCHAR2-IN></ns1:P076_INT6_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P075_INT6_NAME-VARCHAR2-IN></ns1:P075_INT6_NAME-VARCHAR2-IN>" +
                          "<ns1:P074_INT5_GATEWAY-VARCHAR2-IN></ns1:P074_INT5_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P073_INT5_NETMASK-VARCHAR2-IN></ns1:P073_INT5_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P072_INT5_ADDRESS-VARCHAR2-IN></ns1:P072_INT5_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P071_INT5_BALANCE-VARCHAR2-IN></ns1:P071_INT5_BALANCE-VARCHAR2-IN>" +
                          "<ns1:P070_INT5_SIGNAL_LEVEL-VARCHAR2-IN></ns1:P070_INT5_SIGNAL_LEVEL-VARCHAR2-IN>" +
                          "<ns1:P069_INT5_OPERATOR-VARCHAR2-IN></ns1:P069_INT5_OPERATOR-VARCHAR2-IN>" +
                          "<ns1:P068_INT5_PROTO-VARCHAR2-IN></ns1:P068_INT5_PROTO-VARCHAR2-IN>" +
                          "<ns1:P067_INT5_AVG_DELAY-VARCHAR2-IN></ns1:P067_INT5_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P066_INT5_PACKET_LOSS-VARCHAR2-IN></ns1:P066_INT5_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P065_INT5_CURR_STATE-VARCHAR2-IN></ns1:P065_INT5_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P064_INT5_ADMIN_STATUS-VARCHAR2-IN></ns1:P064_INT5_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P063_INT5_NAME-VARCHAR2-IN></ns1:P063_INT5_NAME-VARCHAR2-IN>" +
                          "<ns1:P062_INT4_GATEWAY-VARCHAR2-IN></ns1:P062_INT4_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P061_INT4_NETMASK-VARCHAR2-IN></ns1:P061_INT4_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P060_INT4_ADDRESS-VARCHAR2-IN></ns1:P060_INT4_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P059_INT4_PROTO-VARCHAR2-IN></ns1:P059_INT4_PROTO-VARCHAR2-IN>" +
                          "<ns1:P058_INT4_AVG_DELAY-VARCHAR2-IN></ns1:P058_INT4_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P057_INT4_PACKET_LOSS-VARCHAR2-IN></ns1:P057_INT4_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P056_INT4_CURR_STATE-VARCHAR2-IN></ns1:P056_INT4_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P055_INT4_ADMIN_STATUS-VARCHAR2-IN></ns1:P055_INT4_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P054_INT4_NAME-VARCHAR2-IN></ns1:P054_INT4_NAME-VARCHAR2-IN>" +
                          "<ns1:P053_INT3_DHCP_DNS-VARCHAR2-IN></ns1:P053_INT3_DHCP_DNS-VARCHAR2-IN>" +
                          "<ns1:P052_INT3_DHCP_GATEWAY-VARCHAR2-IN></ns1:P052_INT3_DHCP_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P051_INT3_DHCP_NETMASK-VARCHAR2-IN></ns1:P051_INT3_DHCP_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P050_INT3_DHCP_NETWORK-VARCHAR2-IN></ns1:P050_INT3_DHCP_NETWORK-VARCHAR2-IN>" +
                          "<ns1:P049_INT3_DHCP_SERVER-VARCHAR2-IN></ns1:P049_INT3_DHCP_SERVER-VARCHAR2-IN>" +
                          "<ns1:P048_INT3_KEY-VARCHAR2-IN></ns1:P048_INT3_KEY-VARCHAR2-IN>" +
                          "<ns1:P047_INT3_SSID-VARCHAR2-IN></ns1:P047_INT3_SSID-VARCHAR2-IN>" +
                          "<ns1:P046_INT3_SIGNAL_LEVEL-VARCHAR2-IN></ns1:P046_INT3_SIGNAL_LEVEL-VARCHAR2-IN>" +
                          "<ns1:P045_INT3_MODE-VARCHAR2-IN></ns1:P045_INT3_MODE-VARCHAR2-IN>" +
                          "<ns1:P044_INT3_ENCRYPTION-VARCHAR2-IN></ns1:P044_INT3_ENCRYPTION-VARCHAR2-IN>" +
                          "<ns1:P043_INT3_GATEWAY-VARCHAR2-IN></ns1:P043_INT3_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P042_INT3_NETMASK-VARCHAR2-IN></ns1:P042_INT3_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P041_INT3_ADDRESS-VARCHAR2-IN></ns1:P041_INT3_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P040_INT3_PROTO-VARCHAR2-IN></ns1:P040_INT3_PROTO-VARCHAR2-IN>" +
                          "<ns1:P039_INT3_AVG_DELAY-VARCHAR2-IN></ns1:P039_INT3_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P038_INT3_PACKET_LOSS-VARCHAR2-IN></ns1:P038_INT3_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P037_INT3_CURR_STATE-VARCHAR2-IN></ns1:P037_INT3_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P036_INT3_ADMIN_STATUS-VARCHAR2-IN></ns1:P036_INT3_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P035_INT3_NAME-VARCHAR2-IN></ns1:P035_INT3_NAME-VARCHAR2-IN>" +
                          "<ns1:P034_INT2_GATEWAY-VARCHAR2-IN></ns1:P034_INT2_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P033_INT2_NETMASK-VARCHAR2-IN></ns1:P033_INT2_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P032_INT2_ADDRESS-VARCHAR2-IN></ns1:P032_INT2_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P031_INT2_BALANCE-VARCHAR2-IN></ns1:P031_INT2_BALANCE-VARCHAR2-IN>" +
                          "<ns1:P030_INT2_SIGNAL_LEVEL-VARCHAR2-IN>" + info["RequestEnabled"] + "</ns1:P030_INT2_SIGNAL_LEVEL-VARCHAR2-IN>" +
                          "<ns1:P029_INT2_OPERATOR-VARCHAR2-IN>" + info["FileTransferEnabled"] + "</ns1:P029_INT2_OPERATOR-VARCHAR2-IN>" +
                          "<ns1:P028_INT2_PROTO-VARCHAR2-IN>" + info["Allow_multiple_connections"] + "</ns1:P028_INT2_PROTO-VARCHAR2-IN>" +
                          "<ns1:P027_INT2_AVG_DELAY-VARCHAR2-IN>" + info["Allow_multiple_connections"] + "</ns1:P027_INT2_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P026_INT2_PACKET_LOSS-VARCHAR2-IN>" + info["Enable_file_transfer"] + "</ns1:P026_INT2_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P025_INT2_CURR_STATE-VARCHAR2-IN>" + info["Query timeout"] + "</ns1:P025_INT2_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P024_INT2_ADMIN_STATUS-VARCHAR2-IN>" + info["Query_if_no_logon"] + "</ns1:P024_INT2_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P023_INT2_NAME-VARCHAR2-IN>" + info["Display_Query_Window"] + "</ns1:P023_INT2_NAME-VARCHAR2-IN>" +
                          "<ns1:P022_INT1_GATEWAY-VARCHAR2-IN>0.0.0.0</ns1:P022_INT1_GATEWAY-VARCHAR2-IN>" +
                          "<ns1:P021_INT1_NETMASK-VARCHAR2-IN>0.0.0.0</ns1:P021_INT1_NETMASK-VARCHAR2-IN>" +
                          "<ns1:P020_INT1_ADDRESS-VARCHAR2-IN>" + info["ClientPhysicalAddress"] + "</ns1:P020_INT1_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P019_INT1_PROTO-VARCHAR2-IN>STATIC</ns1:P019_INT1_PROTO-VARCHAR2-IN>" +
                          "<ns1:P018_INT1_AVG_DELAY-VARCHAR2-IN></ns1:P018_INT1_AVG_DELAY-VARCHAR2-IN>" +
                          "<ns1:P017_INT1_PACKET_LOSS-VARCHAR2-IN></ns1:P017_INT1_PACKET_LOSS-VARCHAR2-IN>" +
                          "<ns1:P016_INT1_CURR_STATE-VARCHAR2-IN>" + info["RequestInterval"] + "</ns1:P016_INT1_CURR_STATE-VARCHAR2-IN>" +
                          "<ns1:P015_INT1_ADMIN_STATUS-VARCHAR2-IN></ns1:P015_INT1_ADMIN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P014_INT1_NAME-VARCHAR2-IN>" + info["CustomName"] + "</ns1:P014_INT1_NAME-VARCHAR2-IN>" +
                          "<ns1:P013_APP_PORT_COUNT-VARCHAR2-IN>" + info["SystemName"] + "</ns1:P013_APP_PORT_COUNT-VARCHAR2-IN>" +
                          "<ns1:P012_APP_ACTIVE_IF-VARCHAR2-IN>" + info["OSversion"] + "</ns1:P012_APP_ACTIVE_IF-VARCHAR2-IN>" +
                          "<ns1:P011_APP_VPN_ADDRESS-VARCHAR2-IN>" + info["ClientVPNAddress"] + "</ns1:P011_APP_VPN_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P010_APP_VPN_STATUS-VARCHAR2-IN>true</ns1:P010_APP_VPN_STATUS-VARCHAR2-IN>" +
                          "<ns1:P009_REPO_ADDRESS-VARCHAR2-IN>" + info["RequestURL"] + "</ns1:P009_REPO_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P008_MGR_VPN_PORT-VARCHAR2-IN>" + info["ManagerPort"] + "</ns1:P008_MGR_VPN_PORT-VARCHAR2-IN>" +
                          "<ns1:P007_MGR_VPN_ADDRESS-VARCHAR2-IN>" + info["ManagerVPNAddress"] + "</ns1:P007_MGR_VPN_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P006_MGR_ADDRESS-VARCHAR2-IN>" + info["ManagerAddress"] + "</ns1:P006_MGR_ADDRESS-VARCHAR2-IN>" +
                          "<ns1:P005_CONFIG_VER-VARCHAR2-IN>" + info["AutoStart"] + "</ns1:P005_CONFIG_VER-VARCHAR2-IN>" +
                          "<ns1:P004_SYS_LOCK-VARCHAR2-IN>" + info["Lockdown"] + "</ns1:P004_SYS_LOCK-VARCHAR2-IN>" +
                          "<ns1:P003_SYS_VER-VARCHAR2-IN>" + info["SoftwareVersion"] + "</ns1:P003_SYS_VER-VARCHAR2-IN>" +
                          "<ns1:P002_SYS_MODEL-VARCHAR2-IN>Endpoint</ns1:P002_SYS_MODEL-VARCHAR2-IN>" +
                          "<ns1:P001_SYS_SN-VARCHAR2-IN>" + info["Serial Number"] + "</ns1:P001_SYS_SN-VARCHAR2-IN>" +
                        "</ns1:REGISTERInput>" +
                      "</s11:Body>" +
                    "</s11:Envelope>";

                try
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    if (Program.DEBUG)
                        CMSLogger.debug("PublishWeb: create request");
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                    request.Timeout = SOAP_TIMEOUT;
                    byte[] bytes;

                    Encoding win1251 = Encoding.GetEncoding("windows-1251");
                    bytes = win1251.GetBytes(XmlData);

                    //bytes = System.Text.Encoding.ASCII.GetBytes(XmlData);
                    request.Headers.Add("Authorization", "Basic " + encoded);
                    //request.ContentType = "Content-Type: text/xml;charset=UTF-8";
                    request.ContentType = "Content-Type: text/xml;charset=windows-1251";
                    request.ContentLength = bytes.Length;
                    request.Method = "POST";
                    if (Program.DEBUG)
                        CMSLogger.debug("PublishWeb: GetRequestStream()");
                    Stream requestStream = request.GetRequestStream();

                    if (Program.DEBUG)
                        CMSLogger.debug("PublishWeb: send");

                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response;
                    if (Program.DEBUG)
                        CMSLogger.debug("PublishWeb: get response");

                    response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        if (Program.DEBUG)
                            CMSLogger.debug("PublishWeb: response OK");

                        Stream responseStream = response.GetResponseStream();
                        string responseStr = new StreamReader(responseStream).ReadToEnd();
                        return responseStr;
                    }
                }
                catch (Exception ex)
                {
                    if (Program.DEBUG)
                        CMSLogger.debug("PublishWeb: ERROR " + ex.ToString());
                }
                return null;
            }

        }
    }
}
