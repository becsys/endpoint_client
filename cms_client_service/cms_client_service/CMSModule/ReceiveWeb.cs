﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Threading;

namespace cms_client_service.CMSModule
{
    public class ReceiveWeb
    {
        //таймаут ожидание ответа от веб сервиса
        public int WEB_SERVICE_TIMEOUT;
        //урл запроса данных от веб сервиса
        public string REQUEST_URL = "";
        //интервал запросов к вебсервису
        public int request_frequency;

        public ReceiveWeb(string timeout)
        {
            WEB_SERVICE_TIMEOUT = Convert.ToInt32(timeout);
        }

        public void setRequestFrequency(string frequency)
        {
            Int32.TryParse(frequency, out request_frequency);
            if (request_frequency < 1)
            {
                request_frequency = 1;
            }
            request_frequency = request_frequency * 60;
        }

        public int getRequestFrequency()
        {
            return request_frequency;
        }

        public XmlDocument MakeWebServiceRequest(string url, string serial)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url + serial + ".xml") as HttpWebRequest;
                request.Timeout = WEB_SERVICE_TIMEOUT;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(response.GetResponseStream());
                return (xmlDoc);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        static public void ProcessWebServiceResponse(XmlDocument InfoResponse, SortedList<string, string> config)
        {
            foreach (XmlNode node in InfoResponse.SelectNodes("Endpoint"))
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    foreach (XmlNode subchild in child.ChildNodes)
                    {
                        Console.WriteLine(subchild.InnerText);
                        //Обработаем только параметр Connect (задача 1.6)
                        if (subchild.Name == "Connect")
                        {
                            config.Add(subchild.Name, subchild.InnerText);
                        }
                    }
                }

            }
        }

        public SortedList<string, string> getWebServiceConfig(string serial_number)
        {
            //Create the REST Services request
            SortedList<string, string> WebServiceConfig = new SortedList<string, string>();
            for (int i = 0; i < 3; i++ )
            {
                try
                {
                    XmlDocument InfoResponse = MakeWebServiceRequest(REQUEST_URL, serial_number);
                    ProcessWebServiceResponse(InfoResponse, WebServiceConfig);
                    Console.WriteLine("RECEIVED");
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Thread.Sleep(2000);
                }
            }
                

            return WebServiceConfig;
        }
    }
}
