﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace cms_client_service
{
    namespace CMSModule
    {
        public class VPN
        {
            public string VPN_SERVICE;
            public string VPN_CONFIG_FILE = "";
            public int SERVICE_TIMEOUT;

            public VPN(string name, string config, string timeout)
            {
                VPN_SERVICE = name;
                VPN_CONFIG_FILE = config;
                SERVICE_TIMEOUT = Convert.ToInt32(timeout);
            }
            public string getStatus()
            {
                return Common.WindowsService.ServiceStatus(VPN_SERVICE);
            }

            public bool isRunning()
            {
                return Common.WindowsService.ServiceStatus(VPN_SERVICE) == "Running" ? true : false;
            }

            public void start()
            {
                Common.WindowsService.StartService(VPN_SERVICE, SERVICE_TIMEOUT);
            }

            public bool restart()
            {
                if (isRunning())
                {
                    stop();
                }
                start();

                return isRunning();
            }

            public void stop()
            {
                Common.WindowsService.StopService(VPN_SERVICE, SERVICE_TIMEOUT);
            }

            public void update_config(string ManagerAddress, string ManagerPort)
            {
                using (var writer = new StreamWriter(VPN_CONFIG_FILE, false))
                {
                    // write a bunch of stuff here
                    writer.WriteLine(default_config);
                }
                //update client.ovpn config file
                File.WriteAllText(VPN_CONFIG_FILE, Regex.Replace(File.ReadAllText(VPN_CONFIG_FILE), "^remote .* .*$", "remote " +
                ManagerAddress + " " + ManagerPort, RegexOptions.Multiline));
            }

            private string default_config = "proto tcp" + Environment.NewLine +
                                            "remote vpn.becsys.ru 80" + Environment.NewLine +
                                            "client" + Environment.NewLine +
                                            "dev tun" + Environment.NewLine +
                                            "resolv-retry infinite" + Environment.NewLine +
                                            "nobind" + Environment.NewLine +
                                            "persist-key" + Environment.NewLine +
                                            "ca ca.crt" + Environment.NewLine +
                                            "cert client.crt" + Environment.NewLine +
                                            "key client.key" + Environment.NewLine +
                                            "keepalive 5 10";
        }
    }
}
