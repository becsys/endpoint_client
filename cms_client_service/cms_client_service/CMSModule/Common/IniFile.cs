﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;

//IniFile ini = new IniFile("C:\\Users\\Julia\\Documents\\config.ini");
//string username = ini.IniReadValue("sftp", "Username");
//string password = ini.IniReadValue("sftp", "Password");
//string remotehost = ini.IniReadValue("sftp", "Remote Host");
//ini.IniWriteValue("sftp", "Username", "test");


namespace cms_client_service.CMSModule.Common
{
    class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// INIFile Constructor.
        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        /// Write Data to the INI File
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// Read Data Value From the Ini File
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();
        }
    }
}
