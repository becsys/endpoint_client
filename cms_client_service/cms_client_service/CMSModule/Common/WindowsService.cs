﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Threading;

namespace cms_client_service
{
    namespace CMSModule
    {
        namespace Common
        {
            class WindowsService
            {
                public delegate void infoFunc(string name, int timeout);

                public static string ServiceStatus(string serviceName)
                {
                    ServiceController service = new ServiceController(serviceName);
                    try
                    {
                        if (Program.DEBUG)
                        {
                            CMSLogger.debug("get service status");
                            CMSLogger.debug(serviceName + ": " + service.Status);
                        }

                        switch (service.Status)
                        {
                            case ServiceControllerStatus.Running:
                                return "Running";
                            case ServiceControllerStatus.Stopped:
                                return "Stopped";
                            case ServiceControllerStatus.Paused:
                                return "Paused";
                            case ServiceControllerStatus.StopPending:
                                return "Stopping";
                            case ServiceControllerStatus.StartPending:
                                return "Starting";
                            default:
                                return "Status Changing";
                        }
                    }
                    catch
                    {
                        if (Program.DEBUG)
                            CMSLogger.debug("Get Service STATUS: ERROR");
                        return "error";
                    }
                }

                public static void decorate(infoFunc func, string serviceName, int timeoutMilliseconds)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        try
                        {
                            func(serviceName, timeoutMilliseconds);
                            break;
                        }
                        catch (Exception ex)
                        {
                            CMSLogger.debug("Обнаружена ошибка при работе со службой: " + ex.Message);
                            Thread.Sleep(500);
                            if (i == 2)
                            {
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }

                public static void StartService(string serviceName, int timeoutMilliseconds)
                {
                    decorate(_StartService, serviceName, timeoutMilliseconds);
                }

                public static void StopService(string serviceName, int timeoutMilliseconds)
                {
                    decorate(_StopService, serviceName, timeoutMilliseconds);
                }

                public static void _StartService(string serviceName, int timeoutMilliseconds)
                {
                    CMSLogger.debug("Starting service: " + serviceName + ". Time: " + DateTime.Now.ToString("h:mm:ss tt"));
                    ServiceController service = new ServiceController(serviceName);
                    
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);

                    CMSLogger.debug("Startied service: " + serviceName + ". Time: " + DateTime.Now.ToString("h:mm:ss tt"));
                }

                public static void _StopService(string serviceName, int timeoutMilliseconds)
                {
                    CMSLogger.debug("Stopping service: " + serviceName + ". Time: " + DateTime.Now.ToString("h:mm:ss tt"));
                    ServiceController service = new ServiceController(serviceName);
                    
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                    CMSLogger.debug("Stopped service: " + serviceName + ". Time: " + DateTime.Now.ToString("h:mm:ss tt"));
                }

                //timeoutMilliseconds - общий таймаут на остановку и запуск
                /*
                public static void RestartService(string serviceName, int timeoutMilliseconds)
                {
                    ServiceController service = new ServiceController(serviceName);
                    try
                    {
                        //Получим время, истекшее с момента загрущки системы (в милисекундах)
                        int millisec1 = Environment.TickCount;
                        TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                        // count the rest of the timeout
                        int millisec2 = Environment.TickCount;
                        timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                        service.Start();
                        service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                    }
                    catch { }
                }
                */
            }
        }
    }
}
