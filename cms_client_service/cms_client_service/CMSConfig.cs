﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System;

namespace cms_client_service
{
    //Класс отвечает за загрузку конфигурации из xml-файла и выдачу настроек по запросу
    public class CMSConfig
    {
            public Configuration cfg;
            private string[] db_config_template = { "Name", "IsConfigured", "RequestEnabled", "RequestURL", "RequestFrequency", "ManagerAddress", "ManagerPort", "ManagerVPNAddress", "ManagerSOAPPort" };
            public SortedList<string, SortedList<string, string>> dbSettings;
            private string default_config = "C:\\Program Files\\BE CMS Client\\cms_client.config";

            public CMSConfig(string cfg_path)
            {
                if (!File.Exists(cfg_path))
                {
                    cfg_path = default_config;
                }
                cfg = LoadConfiguration(cfg_path);
                parseDatabaseSettings();
            }

            //Создадим объект конфигурации <Configuration> на основе команд из xml-файла
            //Объект конфигурации хранит информацию как о настройках прилодения, так и о параметрах доступа к БД
            private Configuration LoadConfiguration(string config_path)
            {
                ExeConfigurationFileMap ConfigFile = new ExeConfigurationFileMap();
                ConfigFile.ExeConfigFilename = config_path;
                Configuration Config;
                Config = ConfigurationManager.OpenMappedExeConfiguration(ConfigFile, ConfigurationUserLevel.None);
                return Config;
            }

            public void refresh()
            {
                ConfigurationManager.RefreshSection("appSettings");
            }

            //Парсит данные по коннектам в БД и заносит в переменную dbSettings
            private void parseDatabaseSettings()
            {
                dbSettings = new SortedList<string, SortedList<string, string>>();
                for (int i = 1; i < cfg.ConnectionStrings.ConnectionStrings.Count; i++)
                {
                    string[] ConnString = cfg.ConnectionStrings.ConnectionStrings[i].ConnectionString.Split(';');
                    dbSettings.Add(ConnString[0], parseConnectionString(ConnString));
                }
            }
       
            public SortedList<string, string> parseConnectionString(string[] ConnString)
            {
                //StringDictionary dbSettings = new StringDictionary();
                SortedList<string, string> settings = new SortedList<string, string>();
                int i = 0;
                string value;
                foreach (string s in db_config_template)
                {
                    value = ConnString[i];
                    if ((value == "true") || (value == "True"))
                    {
                        value = "true";
                    }
                   if ((value == "false") || (value == "False"))
                    {
                        value = "false";
                    }
                    settings.Add(s, value);
                    //settings.Add(s, ConnString[i]);
                    i++;
                }

                return settings;
            }

            public bool hasDBKey(string name, string key)
            {
                Console.WriteLine(">!>");
                if (key != "ManagerVPNAddress")
                    return false;
                try
                {
                    string res = dbSettings[name][key];
                    Console.WriteLine("true");
                    return true;
                }
                catch
                {
                    return false;
                }
          
            }
            public bool hasKey(string key)
            {
                //(ConfigurationManager.AppSettings.AllKeys.Contains("myKey"))
                return cfg.AppSettings.Settings[key] != null;
            }

            //Получим настройки приложения
            public string getApplicationSetting(string key)
            {
                string value;
                value = cfg.AppSettings.Settings[key].Value;
                if (value == "True")
                {
                    return "true";
                }
                if (value == "False")
                {
                    return "false";
                }
                return value;
            }

            public void setApplicationSettings(string key, string value)
            {
                cfg.AppSettings.Settings[key].Value = value;
            }

            public void saveApplicationSettings()
            {
                cfg.Save(ConfigurationSaveMode.Modified);
            }
            
            public string getApplicationSettingAsBool(string key)
            {
                if (cfg.AppSettings.Settings[key].Value == "No"
                    || cfg.AppSettings.Settings[key].Value == "0"
                    || cfg.AppSettings.Settings[key].Value == "false"
                    || cfg.AppSettings.Settings[key].Value == "False")
                {
                    return "false";
                }
                else
                {
                    return "true";
                }
            }

            //Получим настройки БД
            public string getDatabaseSetting(string name, string setting)
            {
                return dbSettings[name][setting];
            }
        }
}
