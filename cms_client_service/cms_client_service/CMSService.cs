﻿using System;
using System.ServiceProcess;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO.Pipes;
using Microsoft.Win32;
using System.Management;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace cms_client_service
{
    public class CMSService : ServiceBase
    {
        //
        public System.Object lockThis = new System.Object();
        //настройки параметров сервиса
        public string CONNECTION_NAME = "";   //ConnectionString (Immediate, Waiting)
        public string _CONNECTION_STATUS = "";
        public string CONNECTION_REASON = "";
        //public string CONNECTION_STATUS = "";
        public string CONNECTION_STATUS
        {
            get { return _CONNECTION_STATUS; }
            set { _CONNECTION_STATUS = value; CONNECTION_REASON = ""; CMSLogger.debug("SET STATUS:" + value);  m_shutdownEvent.Set(); }
        }

        public string CONNECTION_CONFIG = ""; //BY_REQUEST, IMMEDIATE, ""
        CMSServiceState serviceState;
        public bool vncConfigChanged = false;
        public DateTime lastWriteTime;

        public CMSConfig cfg;
        
        public Stopwatch stopWatch = new Stopwatch();

        public bool threadProcessFlag = true;  
        protected Thread                m_thread;
        protected ManualResetEvent      m_shutdownEvent;
        public TimeSpan              m_delay;

        public CMSModule.Information information_module;
        public CMSModule.Network network_module;
        public CMSModule.PublishWeb publishWeb_module;
        public CMSModule.ReceiveWeb receiveWeb_module;
        public CMSModule.VNC vnc_module;
        public CMSModule.VPN vpn_module;

        //вычисление абсолютного пути конфига и его загрузка
        public void readConfig()
        {
            string fullAppName = Application.ExecutablePath;
            string fullAppPath = System.IO.Path.GetDirectoryName(fullAppName);

            string absolute_config_name = fullAppPath + "\\" + Program.CONFIG_NAME;
            cfg = new CMSConfig(absolute_config_name);
        }

        public void stopConnectionTimer()
        {
            stopWatch.Stop();
            stopWatch.Reset();
        }

        public void startConnectionTimer()
        {
            stopWatch.Start();
        }

        public TimeSpan getConnectionTimerTime()
        {
            return stopWatch.Elapsed;
        }

        //Обновление настроек (происходит при запуске/перезапуске службы)
        public void initService()
        {
            CMSLogger.debug("CMSService: initialize modules");

            // создадим manual reset event и выставим в состояние unsignaled
            m_shutdownEvent = new ManualResetEvent(false);

            //таймер, отвечающий за период работы главного цикла сервиса
            m_delay = new TimeSpan(0, 0, 0, 5, 0 );

            readConfig();

            CONNECTION_NAME = cfg.getApplicationSetting("DefaultConnection");

            //создание модулей
            information_module = new CMSModule.Information();

            network_module = new CMSModule.Network();

            publishWeb_module = new CMSModule.PublishWeb(cfg.getApplicationSetting("SOAPDelay"),
                                                         cfg.getApplicationSetting("SOAPTimeout"),
                                                         cfg.getApplicationSetting("SOAPInterval"),
                                                         cfg.getApplicationSetting("SOAPUsername"),
                                                         cfg.getApplicationSetting("SOAPPassword"));

            receiveWeb_module = new CMSModule.ReceiveWeb(cfg.getApplicationSetting("WebServiceTimeout"));

            vpn_module = new CMSModule.VPN(cfg.getApplicationSetting("VPNServiceName"),
                                           cfg.getApplicationSetting("VPNServiceConfigFile"),
                                           cfg.getApplicationSetting("ServiceTimeout"));

            vnc_module = new CMSModule.VNC(cfg.getApplicationSetting("VNCServiceName"),
                                           cfg.getApplicationSetting("VNCServiceConfigFile"),
                                           cfg.getApplicationSetting("ServiceTimeout"));  
        }

        public void Process()
        {
            if (File.Exists(CMSLogger.logfile))
            {
                File.Delete(CMSLogger.logfile);
            }

            try
            {
                initService();

                string autostart = cfg.getApplicationSetting("AutoStart");
                if (autostart == "true")
                {
                    CMSLogger.log(Program.programName + "::AUTO_CONNECT" + Environment.NewLine);
                    CONNECTION_NAME = cfg.getApplicationSetting("DefaultConnection");
                    if (cfg.getDatabaseSetting(CONNECTION_NAME, "RequestEnabled") == "false")
                    {
                        CONNECTION_CONFIG = "IMMEDIATE";
                        CONNECTION_STATUS = "START_CONNECTING";
                    }
                    else
                    {
                        CONNECTION_CONFIG = "BY_REQUEST";
                        CONNECTION_STATUS = "START_WAITING_REQUEST";
                    }
                }
            }
            catch (Exception ex)
            {
                CMSLogger.debug("Ошибка при инициализации службы: " + ex.ToString());
                return;
            }

             // create our threadstart object to wrap our delegate method
            ThreadStart ts = new ThreadStart(this.ServiceMain);

            // create the worker thread
            m_thread = new Thread(ts);

            // go ahead and start the worker thread
            m_thread.Start();
        }

        // Основная функция рабочего потока
        public void ServiceMain()
        {
            CMSLogger.debug("CMSService: ServiceMain()");

            //CMSData.Protocol dataExchange = new CMSJsonData.Protocol(this);
            CMSJsonData.Protocol dataExchange = new CMSJsonData.Protocol(this);
            dataExchange.start();
            serviceState = new CMSServiceState(this);

            while (threadProcessFlag)
            {
                CMSLogger.debug("MAIN LOOP: " + DateTime.Now.ToString("h:mm:ss tt"));

                //Ставим lock на обработку обработку состояния сервиса.
                //Необходимо для того, чтобы в процессе обработки состояния его нельзя было изменить из вне,
                //что могло бы привести к непредсказуемым последствиям.

                //Обрабатываем состояние сервиса либо по таймеру, либо по событию.
                //Событие наступает, когда сервис переходит из одного состояния в другое.
                if (m_shutdownEvent.WaitOne(m_delay, true))
                {
                    m_shutdownEvent.Reset();
                    CMSLogger.debug("ServiceState Changed. New CONNECTION_STATUS = " + CONNECTION_STATUS);
                }

                lock (lockThis)
                {
                    processState();
                }
            }
        }

        public void processState()
        {
            CMSLogger.debug("---------------------- START PROCESS STATE ----------------------");
            CMSLogger.debug("CONNECTION_NAME = " + CONNECTION_NAME);
            CMSLogger.debug("CONNECTION_CONFIG = " + CONNECTION_CONFIG);
            CMSLogger.debug("CONNECTION_STATUS = " + CONNECTION_STATUS);
            CMSLogger.debug("CONNECTION_REASON = " + CONNECTION_REASON);

            if ((CONNECTION_STATUS == "") || (CONNECTION_STATUS == "TIMEOUT"))
            {
                serviceState.state = "idle";
            }

            if ((CONNECTION_CONFIG == "IMMEDIATE") &&
                (CONNECTION_STATUS == "START_CONNECTING"))
            {
                serviceState.state = "start manual";
            }

            if ((CONNECTION_CONFIG == "IMMEDIATE") &&
                (CONNECTION_STATUS == "START_CONNECTING_WITH_TIMEOUT"))
            {
                serviceState.state = "start manual with timeout";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "START_WAITING_REQUEST"))
            {
                serviceState.state = "init remote";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "START_WAITING_REQUEST_WITH_TIMEOUT"))
            {
                serviceState.state = "init remote with timeout";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "START_CONNECTING"))
            {
                serviceState.state = "start remote";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "START_CONNECTING_WITH_TIMEOUT"))
            {
                serviceState.state = "start remote with timeout";
            }

            if ((CONNECTION_CONFIG == "IMMEDIATE") &&
                (CONNECTION_STATUS == "CONNECTING"))
            {
                serviceState.state = "connecting manual";
            }

            if ((CONNECTION_CONFIG == "IMMEDIATE") &&
                (CONNECTION_STATUS == "CONNECTING_WITH_TIMEOUT"))
            {
                serviceState.state = "connecting manual with timeout";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "WAITING_REQUEST"))
            {
                serviceState.state = "waiting remote";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "WAITING_REQUEST_WITH_TIMEOUT"))
            {
                serviceState.state = "waiting remote with timeout";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "CONNECTING"))
            {
                serviceState.state = "connecting remote";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "CONNECTING_WITH_TIMEOUT"))
            {
                serviceState.state = "connecting remote with timeout";
            }

            if ((CONNECTION_CONFIG == "BY_REQUEST") &&
                (CONNECTION_STATUS == "CONNECTED"))
            {
                serviceState.state = "process remote";
            }

            if ((CONNECTION_CONFIG == "IMMEDIATE") &&
                (CONNECTION_STATUS == "CONNECTED"))
            {
                serviceState.state = "process manual";
            }

            if (CONNECTION_STATUS == "DISCONNECTING")
            {
                serviceState.state = "disconnecting";
            }

            if (CONNECTION_STATUS == "SHUTDOWN")
            {
                serviceState.state = "shutdown";
            }

            CMSLogger.debug("serviceState.state = " + serviceState.state);
            try
            {
                serviceState.process();
            }
            catch (Exception ex)
            {
                CMSLogger.debug("Ошибка при обработке состояния <" + serviceState.state + "> :" + ex.Message);
            }

            CMSLogger.debug("----------------------- END PROCESS STATE -----------------------");
        }
        
        protected override void OnStart(string[] args)
        {
            Process();
            base.OnStart(args);
        }

        //выполняет остановку сервисов OpenVPN/UltraVNC (в текущей версии только OpenVPN)
        protected override void OnStop()
        {
            CONNECTION_STATUS = "SHUTDOWN";

            // wait for the thread to stop giving it 10 seconds
            if (!m_thread.Join(10000))
            {
                CMSLogger.log(Program.programName + "::OnStop() поток не завершился за отведенные 10 сек." + Environment.NewLine);
                CMSLogger.debug("OnStop() поток не завершился за отведенные 10 сек.");
            }
            base.OnStop();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
        }
    }
}
