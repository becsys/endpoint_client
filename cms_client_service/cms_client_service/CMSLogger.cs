﻿using System;
using System.IO;

namespace cms_client_service
{
    class CMSLogger
    {
        private static string log_name = "BE CMS Endpoint Client";
        public static System.Object lockThis2 = new System.Object();
        public static string logfile = "debug.log";
        //public static string logfile = "C:\\Program Files\\BE CMS Client\\debug.log";
        public static void log(string text)
        {
            System.Diagnostics.EventLog.WriteEntry(log_name, text);
        }
        
        public static void debug(string text)
        {
            if (!Program.DEBUG)
                return;

            lock (lockThis2)
            {
                using (var writer = new StreamWriter(logfile, true))
                {
                    // write a bunch of stuff here
                    writer.WriteLine(text);
                }
            }
           
        }
    }
}
