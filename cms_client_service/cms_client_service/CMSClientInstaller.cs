﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using Microsoft.Win32;
using cms_client_service;

[RunInstaller(true)]
public class CMSClientServiceInstaller : Installer
{
    private ServiceProcessInstaller processInstaller;
    private ServiceInstaller serviceInstaller;

    public CMSClientServiceInstaller()
    {
        processInstaller = new ServiceProcessInstaller();
        serviceInstaller = new ServiceInstaller();

        processInstaller.Account = ServiceAccount.LocalSystem;
        //serviceInstaller.ServicesDependedOn = new string[] { "uvnc_service", "OpenVPN Service" };
        serviceInstaller.StartType = ServiceStartMode.Automatic;
        serviceInstaller.DelayedAutoStart = false;
        serviceInstaller.ServiceName = "BE CMS Endpoint Service"; //must match CMSClientService.ServiceName
        this.serviceInstaller.Description = "Business Ecosystems Console Management System Endpoint Service v" + cms_client_service.Program.SOFTWARE_NUMBER;
        Installers.Add(serviceInstaller);
        Installers.Add(processInstaller);
    }

}