1. Install OpenVPN
2. Install UltraVNC
3. Copy \Demo\Program Files\BE CMS Client to C:Program Files
4. Install "BE CMS Client service", Run \Program Files\BE CMS Client\_installer.bat
5. Run "BE CMS Client" tray application, Run \Program Files\BE CMS Client\cms_client_tray.exe
6. Check "Application" log in Event Viewer
7. Check "OpenVPN Service" and "uvnc_service" service status (should be Not Started)
8. Push "Connect" in BE CMS Client
9. Check "OpenVPN Service" and "uvnc_service" service status (should be Started)
10. Check "Application" log in Event Viewer
