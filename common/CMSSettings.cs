﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using cms_transport;


namespace cms_client_tray
{
    partial class CMSSettings
    {
        public static bool isAutoStart
        {
            get { return s_instance.isAutoStart; }
            set { s_instance.isAutoStart = value; }
        }
        public static bool isLockdown
        {
            get { return s_instance.isLockdown; }
            set { s_instance.isLockdown = value; }
        }
        public static string defaultConnection
        {
            get { return s_instance.defaultConnection; }
            set { s_instance.defaultConnection = value; }
        }
        public static int serviceTimeout
        {
            get { return s_instance.serviceTimeout; }
            set { s_instance.serviceTimeout = value; }
        }
        public static int webServiceTimeout
        {
            get { return s_instance.webServiceTimeout; }
            set { s_instance.webServiceTimeout = value; }
        }
        public static string vpnServiceName
        {
            get { return s_instance.vpnServiceName; }
            set { s_instance.vpnServiceName = value; }
        }
        public static string vpnServiceConfigFile
        {
            get { return s_instance.vpnServiceConfigFile; }
            set { s_instance.vpnServiceConfigFile = value; }
        }
        public static string vpnServiceLogFile
        {
            get { return s_instance.vpnServiceLogFile; }
            set { s_instance.vpnServiceLogFile = value; }
        }
        public static string vncServiceName
        {
            get { return s_instance.vncServiceName; }
            set { s_instance.vncServiceName = value; }
        }
        public static int soapPort
        {
            get { return s_instance.soapPort; }
            set { s_instance.soapPort = value; }
        }
        public static int soapDelay
        {
            get { return s_instance.soapDelay; }
            set { s_instance.soapDelay = value; }
        }
        public static int soapTimeout
        {
            get { return s_instance.soapTimeout; }
            set { s_instance.soapTimeout = value; }
        }
        public static int soapInterval
        {
            get { return s_instance.soapInterval; }
            set { s_instance.soapInterval = value; }
        }
        public static string soapUsername
        {
            get { return s_instance.soapUsername; }
            set { s_instance.soapUsername = value; }
        }
        public static string soapPassword
        {
            get { return s_instance.soapPassword; }
            set { s_instance.soapPassword = value; }
        }
        public static string soapResources
        {
            get { return s_instance.soapResources; }
            set { s_instance.soapResources = value; }
        }
        public static ConnectionStringsSection ConnectionStrings
        {
            get { return s_instance.ConnectionStrings; }
        }
        public static string version
        {
            get { return s_instance.version; }
            set { s_instance.version = value; }
        }

        // Global tab
        public static bool isDisplayQueryWindow
        {
            get { return s_instance.isDisplayQueryWindow; }
            set { s_instance.isDisplayQueryWindow = value; }
        }
        public static bool isQueryIfNoLogon
        {
            get { return s_instance.isQueryIfNoLogon; }
            set { s_instance.isQueryIfNoLogon = value; }
        }
        public static int queryTimeout
        {
            get 
            { 
                const int DEFAULT_TIMEOUT = 1;
                return s_instance.queryTimeout == 0 ? DEFAULT_TIMEOUT : s_instance.queryTimeout;
            }
            set { s_instance.queryTimeout = value; }
        }
        public static bool isEnableFileTransfer
        {
            get { return s_instance.isEnableFileTransfer; }
            set { s_instance.isEnableFileTransfer = value; }
        }
        public static bool isAllowMultipleConnections
        {
            get { return s_instance.isAllowMultipleConnections; }
            set { s_instance.isAllowMultipleConnections = value; }
        }
        public static bool isLockOnDisconnect
        {
            get { return s_instance.isLockOnDisconnect; }
            set { s_instance.isLockOnDisconnect = value; }
        }
        public static string queryMessage
        {
            get { return s_instance.queryMessage; }
            set { s_instance.queryMessage = value; }
        }
        public static int connectionTimeout
        {
            get { return s_instance.connectionTimeout; }
            set { s_instance.connectionTimeout = value; }
        }
        
        // connection data
        public static SortedList<string, Connection> connections = new SortedList<string, Connection>();

        public static string connectionName { get; set; }

        public static bool isConnected { get; set; }

        public static void addConnection(Connection connection)
        {
            connections.Add(connection.name, connection);
            s_instance.saveProfiles();
        }

        public static void removeConnection(string name)
        {
            connections.Remove(name);
            s_instance.saveProfiles();
        }

        public static void updateConnection(Connection connection)
        {
            connections[connection.name] = connection;
            s_instance.saveProfiles();
        }

        public static void saveGlobal()
        {
            s_instance.saveGlobal();
        }       
    }
}
