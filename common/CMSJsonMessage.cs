﻿using System.Web.Script.Serialization;
using System;

namespace cms_transport
{

    //Messages

    public class CMSRegisterRequest { }
    
    public class CMSRegisterResponse
    {
        public string result { get;  set; }
        public string connectionName { get;  set; }
        public string connectionStatus { get;  set; }
        public string version { get; set; }
        public Profile[] profiles {get;  set; }
        public Settings settings { get; set; }
        public CMSRegisterResponse() { }
        public CMSRegisterResponse(string iresult, string iname, string istatus, string iversion, Profile[] iprofiles, Settings isettings)
        {
            result = iresult;
            connectionName = iname;
            connectionStatus = istatus;
            version = iversion;
            profiles = iprofiles;
            settings = isettings;
        }
    }

    public class Settings
    {
        public string autostart { get; set; }
        public string lockdown { get; set; }
        public string connectionTimeout { get; set; }
        public string querySetting { get; set; }
        public string queryTimeout { get; set; }
        public string accept_reject_mesg { get; set; }
        public string lockSetting { get; set; }
        public string connectPriority { get; set; }
        public string fileTransferEnabled { get; set; }
        public string queryIfNoLogon { get; set; }
        public Settings() { }
        public Settings(string iautostart, string ilockdown, string iconnectionTimeout, string iquerySetting, string iqueryTimeout, string iaccept_reject_mesg, string ilockSetting, string iconnectPriority, string ifileTransferEnabled, string iqueryIfNoLogon)
        {
            autostart = iautostart;
            lockdown = ilockdown;
            connectionTimeout = iconnectionTimeout;
            querySetting = iquerySetting;
            queryTimeout = iqueryTimeout;
            accept_reject_mesg = iaccept_reject_mesg;
            lockSetting = ilockSetting;
            connectPriority = iconnectPriority;
            fileTransferEnabled = ifileTransferEnabled;
            queryIfNoLogon = iqueryIfNoLogon;
        }
    }

    public class Profile 
    {
        public string name { get;  set; }
        public string isConfigured { get;  set; }
        public string requestEnabled { get;  set; }
        public string requestURL { get;  set; }
        public string requestFrequency { get;  set; }
        public string managerAddress { get;  set; }
        public string managerPort { get;  set; }
        public string managerVPNAddress { get; set; }
        public string managerSOAPPort { get;  set; }
        public Profile() { }
        public Profile(string _name, string _isConfigured, string _requestEnabled, string _requestURL, string _requestFrequency, string _managerAddress, string _managerPort, string _managerVPNAddress, string _managerSOAPPort)
        {
            name = _name;
            isConfigured = _isConfigured;
            requestEnabled = _requestEnabled;
            requestURL = _requestURL;
            requestFrequency = _requestFrequency;
            managerAddress = _managerAddress;
            managerPort = _managerPort;
            managerVPNAddress = _managerVPNAddress;
            managerSOAPPort = _managerSOAPPort;
        }

    }

    public class CMSGetGlobalSettingsRequest { }

    public class CMSGetGlobalSettingsResponse
    {
        public string result { get; set; }
        public Settings settings { get; set;}
        public CMSGetGlobalSettingsResponse() { }
        public CMSGetGlobalSettingsResponse(string iresult, Settings isettings)
        {
            result = iresult;
            settings = isettings;
        }
    }
    
    public class CMSGetProfilesRequest { }

    public class CMSGetProfilesResponse
    {
        public string result { get; set; }
        public Profile[] profiles { get; set; }
        public CMSGetProfilesResponse() { }
        CMSGetProfilesResponse (string iresult, Profile[] iprofiles)
        {
            result = iresult;
            profiles = iprofiles;
        }
    }
    
    public class CMSConnectRequest
    {
        public string connectionName { get; set; }
        public string connectionStatus { get; set; }
        public CMSConnectRequest() { }
        public CMSConnectRequest(string iname, string istatus)
        {
            connectionName = iname;
            connectionStatus = istatus;
        }
    }

    public class CMSConnectResponse
    {
        public string result { get; set; }
        public string connectionName { get; set; }
        public string connectionStatus { get;  set; }
        public CMSConnectResponse() { }
        public CMSConnectResponse(string iresult, string iname, string istatus)
        {
            result = iresult;
            connectionName = iname;
            connectionStatus = istatus;
        }
    }

    public class CMSDisconnectRequest { }

    public class CMSDisconnectResponse
    {
        public string result { get; set; }
        public CMSDisconnectResponse() { }
        public CMSDisconnectResponse(string iresult)
        {
            result = iresult;
        }
    }

    public class CMSStatusRequest { }

    public class CMSStatusResponse
    {
        public string result { get; set; }
        public string connectionStatus { get; set; }
        public string isGlobalSettingsChanged { get; set; }
        public string isProfilesChanged { get; set; }
        public Network network{ get; set; }
        public Vpn vpn { get; set; }
        public CMSStatusResponse() { }
        public CMSStatusResponse(string iresult, string istatus, Network inetwork, Vpn ivpn, string iisGlobalSettingsChanged, string iisProfilesChanged)
        {
            result = iresult;
            connectionStatus = istatus;
            network = inetwork;
            vpn = ivpn;
            isGlobalSettingsChanged = iisGlobalSettingsChanged;
            isProfilesChanged = iisProfilesChanged;
        }
    }

    public class Network 
    {
        public string systemName { get;  set; }
        public string customName { get;  set; }
        public string osVersion { get;  set; }
        public string serialNumber { get;  set; }
        public string receivedBytes { get;  set; }
        public string sendtBytes { get;  set; }
        public string ip { get;  set; }
        public Network() { }
        public Network(string isystemName, string icustomName, string iosVersion,  string iserialNumber, string ireceivedB, string isendedB, string iip)
        {
            systemName = isystemName;
            customName = icustomName;
            osVersion = iosVersion;
            serialNumber = iserialNumber;
            receivedBytes = ireceivedB;
            sendtBytes = isendedB;
            ip = iip;
        }
    }	

    public class Vpn
    {
        public string lifetime { get;  set; }
        public Vpn() { }
        public Vpn(string ilifetime)
        {
            lifetime = ilifetime;
        }
    }

    public class CMSSetGlobalSettingsRequest
    {
        public Settings settings { get; set;}
        public CMSSetGlobalSettingsRequest() { }
        public CMSSetGlobalSettingsRequest(Settings isettings)
        {
            settings = isettings;
        }
    }

    public class CMSSetGlobalSettingsResponse
    {
        public string result { get; set; }
        public CMSSetGlobalSettingsResponse () {}
        public CMSSetGlobalSettingsResponse (string iresut)
        {
            result = iresut;
        }
    }
 
    public class CMSSetProfileRequest
    {
        public Profile[] profiles { get; set; }
        public CMSSetProfileRequest() { }
        public CMSSetProfileRequest(Profile[] iprofiles)
        {
            profiles = iprofiles;
        }
    }

    public class CMSSetProfileResponse
    {
        public string result;
        public CMSSetProfileResponse() { }
        public CMSSetProfileResponse(string iresult)
        {
            result = iresult;
        }
    }
    
    //Serialization

    public class CustomResolver : JavaScriptTypeResolver
    {
        public override Type ResolveType(string id)
        {
            return Type.GetType(id);
        }

        public override string ResolveTypeId(Type type)
        {
            return type.ToString();
        }
    }

}
