﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace cms_client_tray
{
    public struct Connection
    {
        public string name;
        public bool isConfigured;
        public bool requestEnabled;
        public string requestURL;
        public int requestFrequency;
        public string managerAddress;
        public int managerPort;
        public string managerVPNAddress;
        public int managerSOAPPort;

        public Connection(string _name,
            bool _isConfigured,
            bool _requestEnabled,
            string _requestURL,
            int _requestFrequency,
            string _managerAddress,
            int _managerPort,
            string _managerVPNAddress,
            int _managerSOAPPort)
        {
            name = _name;
            isConfigured = _isConfigured;
            requestEnabled = _requestEnabled;
            requestURL = _requestURL;
            requestFrequency = _requestFrequency;
            managerAddress = _managerAddress;
            managerPort =_managerPort;
            managerVPNAddress = _managerVPNAddress;
            managerSOAPPort = _managerSOAPPort;
        }

        public Connection(string connectionString)
        {
            string[] fieldsString = connectionString.Split(';');
            name = fieldsString[0];
            isConfigured = Convert.ToBoolean(fieldsString[1]);
            requestEnabled = Convert.ToBoolean(fieldsString[2]);
            requestURL = fieldsString[3];
            requestFrequency = Convert.ToInt32(fieldsString[4]);
            managerAddress = fieldsString[5];
            managerPort = Convert.ToInt32(fieldsString[6]);
            managerVPNAddress = fieldsString[7];
            managerSOAPPort = Convert.ToInt32(fieldsString[8]);
        }

        public ConnectionStringSettings CreateConnectionString(string connName, string providerName)
        {
            return new ConnectionStringSettings(connName,
                GetConnectionString(),
                providerName);
        }

        public string GetConnectionString()
        {
            return name + ";" +
                Convert.ToString(isConfigured) + ";" +
                Convert.ToString(requestEnabled) + ";" +
                requestURL + ";" +
                Convert.ToString(requestFrequency) + ";" +
                managerAddress + ";" +
                Convert.ToString(managerPort) + ";" +
                managerVPNAddress + ";" +
                Convert.ToString(managerSOAPPort);
        }
    };
}
